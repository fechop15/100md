import { Component } from '@angular/core';
import {ViewController} from "ionic-angular";

/**
 * Generated class for the PopoverComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'popover',
  templateUrl: 'popover.html'
})
export class PopoverComponent {

  items:any;
  constructor(public viewCtrl: ViewController) {
    console.log('Hello PopoverComponent Component');
    //this.items=[{texto:"Mi Cuenta",id:"1"},{texto:"Salir",id:"4"}];
    //si es admin
    this.items=[{texto:"Informacion",id:"1"},{texto:"Salir",id:"4"}];
  }

  itemClick(item){
    this.viewCtrl.dismiss(item);
  }

}
