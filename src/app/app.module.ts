import {NgModule, ErrorHandler} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {MyApp} from './app.component';
import {HttpModule} from "@angular/http";

import {ArticuloPage} from '../pages/articulo/articulo';
import {InformacionPage} from '../pages/informacion/informacion';
import {AboutPage} from '../pages/about/about';
import {ContactPage} from '../pages/contact/contact';
import {HomePage} from '../pages/home/home';
import {TabsPage} from '../pages/tabs/tabs';
import {ModulosPage} from '../pages/modulos/modulos';
import {EscrituraClavePage} from '../pages/temas/escritura-clave/escritura-clave';
import {MiCompromisoPage} from '../pages/temas/mi-compromiso/mi-compromiso';
import {AplicacionPage} from '../pages/temas/aplicacion/aplicacion';


import {VisionPage} from '../pages/clases/vision/vision';
import {AdoracionPage} from '../pages/clases/adoracion/adoracion';
import {DiscipuladoPage} from '../pages/clases/discipulado/discipulado';
import {RelacionesPage} from '../pages/clases/relaciones/relaciones';
import {EymPage} from '../pages/clases/eym/eym';
import {MisionPage} from '../pages/clases/mision/mision';

import {StreamingMedia} from '@ionic-native/streaming-media';

//cuestionarios
import {Cuestionario1Page} from '../pages/temas/cuestionarios/cuestionario1/cuestionario1';
import {Cuestionario2Page} from '../pages/temas/cuestionarios/cuestionario2/cuestionario2';
import {Cuestionario3Page} from '../pages/temas/cuestionarios/cuestionario3/cuestionario3';
import {Cuestionario4Page} from '../pages/temas/cuestionarios/cuestionario4/cuestionario4';
import {Cuestionario5Page} from '../pages/temas/cuestionarios/cuestionario5/cuestionario5';
import {Cuestionario6Page} from '../pages/temas/cuestionarios/cuestionario6/cuestionario6';
import {Cuestionario7Page} from '../pages/temas/cuestionarios/cuestionario7/cuestionario7';
import {Cuestionario8Page} from '../pages/temas/cuestionarios/cuestionario8/cuestionario8';
import {Cuestionario9Page} from '../pages/temas/cuestionarios/cuestionario9/cuestionario9';
import {Cuestionario10Page} from '../pages/temas/cuestionarios/cuestionario10/cuestionario10';
import {Cuestionario11Page} from '../pages/temas/cuestionarios/cuestionario11/cuestionario11';
import {Cuestionario12Page} from '../pages/temas/cuestionarios/cuestionario12/cuestionario12';
import {Cuestionario13Page} from '../pages/temas/cuestionarios/cuestionario13/cuestionario13';
import {Cuestionario14Page} from '../pages/temas/cuestionarios/cuestionario14/cuestionario14';
import {Cuestionario15Page} from '../pages/temas/cuestionarios/cuestionario15/cuestionario15';
import {Cuestionario16Page} from '../pages/temas/cuestionarios/cuestionario16/cuestionario16';
import {Cuestionario17Page} from '../pages/temas/cuestionarios/cuestionario17/cuestionario17';
import {Cuestionario18Page} from '../pages/temas/cuestionarios/cuestionario18/cuestionario18';
import {Cuestionario19Page} from '../pages/temas/cuestionarios/cuestionario19/cuestionario19';
import {Cuestionario20Page} from '../pages/temas/cuestionarios/cuestionario20/cuestionario20';
import {Cuestionario21Page} from '../pages/temas/cuestionarios/cuestionario21/cuestionario21';
import {Cuestionario22Page} from '../pages/temas/cuestionarios/cuestionario22/cuestionario22';
import {Cuestionario23Page} from '../pages/temas/cuestionarios/cuestionario23/cuestionario23';
import {Cuestionario24Page} from '../pages/temas/cuestionarios/cuestionario24/cuestionario24';
import {Cuestionario25Page} from '../pages/temas/cuestionarios/cuestionario25/cuestionario25';
import {Cuestionario26Page} from '../pages/temas/cuestionarios/cuestionario26/cuestionario26';
import {Cuestionario27Page} from '../pages/temas/cuestionarios/cuestionario27/cuestionario27';
import {Cuestionario28Page} from '../pages/temas/cuestionarios/cuestionario28/cuestionario28';
import {Cuestionario29Page} from '../pages/temas/cuestionarios/cuestionario29/cuestionario29';
import {Cuestionario30Page} from '../pages/temas/cuestionarios/cuestionario30/cuestionario30';
import {Cuestionario31Page} from '../pages/temas/cuestionarios/cuestionario31/cuestionario31';
import {Cuestionario32Page} from '../pages/temas/cuestionarios/cuestionario32/cuestionario32';
import {Cuestionario33Page} from '../pages/temas/cuestionarios/cuestionario33/cuestionario33';
import {Cuestionario34Page} from '../pages/temas/cuestionarios/cuestionario34/cuestionario34';
import {Cuestionario35Page} from '../pages/temas/cuestionarios/cuestionario35/cuestionario35';
import {Cuestionario36Page} from '../pages/temas/cuestionarios/cuestionario36/cuestionario36';
import {Cuestionario37Page} from '../pages/temas/cuestionarios/cuestionario37/cuestionario37';
import {Cuestionario38Page} from '../pages/temas/cuestionarios/cuestionario38/cuestionario38';
import {Cuestionario39Page} from '../pages/temas/cuestionarios/cuestionario39/cuestionario39';
import {Cuestionario40Page} from '../pages/temas/cuestionarios/cuestionario40/cuestionario40';
import {Cuestionario41Page} from '../pages/temas/cuestionarios/cuestionario41/cuestionario41';
import {Cuestionario42Page} from '../pages/temas/cuestionarios/cuestionario42/cuestionario42';
import {Cuestionario43Page} from '../pages/temas/cuestionarios/cuestionario43/cuestionario43';
import {Cuestionario44Page} from '../pages/temas/cuestionarios/cuestionario44/cuestionario44';
import {Cuestionario45Page} from '../pages/temas/cuestionarios/cuestionario45/cuestionario45';
import {Cuestionario46Page} from '../pages/temas/cuestionarios/cuestionario46/cuestionario46';
import {Cuestionario47Page} from '../pages/temas/cuestionarios/cuestionario47/cuestionario47';
import {Cuestionario48Page} from '../pages/temas/cuestionarios/cuestionario48/cuestionario48';
import {Cuestionario49Page} from '../pages/temas/cuestionarios/cuestionario49/cuestionario49';
import {Cuestionario50Page} from '../pages/temas/cuestionarios/cuestionario50/cuestionario50';
import {Cuestionario51Page} from '../pages/temas/cuestionarios/cuestionario51/cuestionario51';
import {Cuestionario52Page} from '../pages/temas/cuestionarios/cuestionario52/cuestionario52';

import {WelcomePage} from '../pages/welcome/welcome';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {RegisterPage} from '../pages/register/register';
import {AuthServiceProvider} from '../providers/auth-service/auth-service';
import {PopoverComponent} from "../components/popover/popover";

import {InAppBrowser} from '@ionic-native/in-app-browser';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    WelcomePage,
    RegisterPage,
    ContactPage,
    HomePage,
    ArticuloPage,
    InformacionPage,
    TabsPage,
    VisionPage,
    AdoracionPage,
    ModulosPage,
    DiscipuladoPage,
    RelacionesPage,
    EymPage,
    MisionPage,
    EscrituraClavePage,
    MiCompromisoPage,
    AplicacionPage,
    Cuestionario1Page,
    Cuestionario2Page,
    Cuestionario3Page,
    Cuestionario4Page,
    Cuestionario5Page,
    Cuestionario6Page,
    Cuestionario7Page,
    Cuestionario8Page,
    Cuestionario9Page,
    Cuestionario10Page,
    Cuestionario11Page,
    Cuestionario12Page,
    Cuestionario13Page,
    Cuestionario14Page,
    Cuestionario15Page,
    Cuestionario16Page,
    Cuestionario17Page,
    Cuestionario18Page,
    Cuestionario19Page,
    Cuestionario20Page,
    Cuestionario21Page,
    Cuestionario22Page,
    Cuestionario23Page,
    Cuestionario24Page,
    Cuestionario25Page,
    Cuestionario26Page,
    Cuestionario27Page,
    Cuestionario28Page,
    Cuestionario29Page,
    Cuestionario30Page,
    Cuestionario31Page,
    Cuestionario32Page,
    Cuestionario33Page,
    Cuestionario34Page,
    Cuestionario35Page,
    Cuestionario36Page,
    Cuestionario37Page,
    Cuestionario38Page,
    Cuestionario39Page,
    Cuestionario40Page,
    Cuestionario41Page,
    Cuestionario42Page,
    Cuestionario43Page,
    Cuestionario44Page,
    Cuestionario45Page,
    Cuestionario46Page,
    Cuestionario47Page,
    Cuestionario48Page,
    Cuestionario49Page,
    Cuestionario50Page,
    Cuestionario51Page,
    Cuestionario52Page,
    PopoverComponent
  ],
  imports: [
    BrowserModule, HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    WelcomePage,
    RegisterPage,
    ContactPage,
    HomePage,
    ArticuloPage,
    InformacionPage,
    TabsPage,
    VisionPage,
    AdoracionPage,
    ModulosPage,
    DiscipuladoPage,
    RelacionesPage,
    EymPage,
    MisionPage,
    EscrituraClavePage,
    MiCompromisoPage,
    AplicacionPage,
    Cuestionario1Page,
    Cuestionario1Page,
    Cuestionario2Page,
    Cuestionario3Page,
    Cuestionario4Page,
    Cuestionario5Page,
    Cuestionario6Page,
    Cuestionario7Page,
    Cuestionario8Page,
    Cuestionario9Page,
    Cuestionario10Page,
    Cuestionario11Page,
    Cuestionario12Page,
    Cuestionario13Page,
    Cuestionario14Page,
    Cuestionario15Page,
    Cuestionario16Page,
    Cuestionario17Page,
    Cuestionario18Page,
    Cuestionario19Page,
    Cuestionario20Page,
    Cuestionario21Page,
    Cuestionario22Page,
    Cuestionario23Page,
    Cuestionario24Page,
    Cuestionario25Page,
    Cuestionario26Page,
    Cuestionario27Page,
    Cuestionario28Page,
    Cuestionario29Page,
    Cuestionario30Page,
    Cuestionario31Page,
    Cuestionario32Page,
    Cuestionario33Page,
    Cuestionario34Page,
    Cuestionario35Page,
    Cuestionario36Page,
    Cuestionario37Page,
    Cuestionario38Page,
    Cuestionario39Page,
    Cuestionario40Page,
    Cuestionario41Page,
    Cuestionario42Page,
    Cuestionario43Page,
    Cuestionario44Page,
    Cuestionario45Page,
    Cuestionario46Page,
    Cuestionario47Page,
    Cuestionario48Page,
    Cuestionario49Page,
    Cuestionario50Page,
    Cuestionario51Page,
    Cuestionario52Page,
    PopoverComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServiceProvider, StreamingMedia, InAppBrowser
  ]
})
export class AppModule {
}
