import {Component} from '@angular/core';
import {IonicPage, NavController, ToastController} from 'ionic-angular';
import {RegisterPage} from '../register/register';
import {TabsPage} from '../tabs/tabs';
import {LoadingController} from 'ionic-angular';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';

import {InAppBrowser} from '@ionic-native/in-app-browser';


/**
 * Generated class for the WelcomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {

  responseData: any;
  userData = {"username": "", "password": ""};

  constructor(public navCtrl: NavController, public authServiceProvider: AuthServiceProvider, public loadingCtrl: LoadingController, public toastCtrl: ToastController, private iab: InAppBrowser) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WelcomePage');
    if (localStorage.getItem('userData')) {
      this.authServiceProvider.actualizarLogin();
      this.navCtrl.setRoot(TabsPage);
    } else {
      let elements = document.querySelectorAll(".tabbar");
      if (elements != null) {
        Object.keys(elements).map((key) => {
          elements[key].style.display = 'none';
        });
      }
    }

  }

  registro() {
    this.navCtrl.push(RegisterPage);
  }

  ingreso() {

    if (this.userData.username && this.userData.password) {

      const loader = this.loadingCtrl.create({
        content: "Iniciando..."
      });
      loader.present();
      this.authServiceProvider.postData(this.userData, "login").then((result) => {
        this.responseData = result;
        console.log(this.responseData);
        if (this.responseData.userData) {
          localStorage.setItem("userData", JSON.stringify(this.responseData));
          localStorage.setItem("json", JSON.stringify(this.responseData.userData.json));
          //this.authServiceProvider.consultarTemasRealizados();
          loader.dismiss();
          this.navCtrl.setRoot(TabsPage);
        } else if (this.responseData.error) {
          loader.dismiss();
          this.presentToast(this.responseData.error.text);
        }
        else {
          loader.dismiss();
          this.presentToast("Ingrese un usuario y una contraseña valida");
        }
      });
    } else {
      this.presentToast("ingrese un usuario y una contraseña");
    }
  }

  presentToast(texto) {
    let toast = this.toastCtrl.create({
      message: texto,
      duration: 2000,
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  recuperar() {
    let url = "http://www.plan100mildiscipulos.com/app100/recovery";
    this.iab.create(url, '_system');
  }

}
