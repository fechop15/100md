import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, Platform, ViewController} from 'ionic-angular';

/**
 * Generated class for the AplicacionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-aplicacion',
  templateUrl: 'aplicacion.html',
})
export class AplicacionPage {
  contenido:any;
  captura:any;
  aplicaciones=[
    {
      aplicacion:[
        {
          texto:"Toma tiempo para leer y estudiar esta lección cuidadosamente. Estudia los 5 componentes diferentes de la visión CMF: adoración, evangelismo, discipulado, relación y misión. Mira a ver si estás viviendo tu vida en relación a estos componentes. Si no es así, esta es una buena oportunidad para conocerlos mejor y aplicarlos en tu vida cotidiana. "
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Debo regresar al lugar de adoración."
        },     {
          texto:"Debo rendir mi espíritu, alma y cuerpo al señorío de Jesucristo. "
        },     {
          texto:"Debo permitir que Él me use para Su gloria. "
        }
      ]
    },{
      aplicacion:[
        {
          texto:"¿Cómo puedo hoy empezar a ser agradecido y alabar a Dios?"
        },     {
          texto:"¿Cómo puedo mantener un cielo abierto del Todopoderoso si no soy agradecido en relación al Salmo 100-4?"
        },     {
          texto:"Necesito recordarme constantemente de ser agradecido, a llenar mi boca con alabanza incluso en situaciones que no lo justifican."
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Una mente preocupada (con las preocupaciones de este mundo) no te permitirá adorar a Dios por completo, por eso es importante que te presentes con anhelo y expectativa. "
        },     {
          texto:"Aprende a humillarte ante Su presencia y adorarle. "
        },     {
          texto:"La calidad de tu vida devocional y tu vida de adoración determinan la calidad de tu vida ministerial. "
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Si te disciplinas a orar consistentemente en el mismo lugar, a la misma hora, la oración se convertirá en un hábito. "}
      ]
    },{
      aplicacion:[
        {
          texto:"Cuando decidimos honrar a Dios con todo lo que tenemos, y a dar generosamente con lo que Él provee, nos alinearemos para las maravillosas bendiciones de Dios. El prometió que el abriría los cielos y derramaría sus bendiciones hasta no tener suficiente espacio para contenerlas. (Mal 3:10). Dios nos bendecirá para que podamos ser bendición. "}
      ]
    },{
      aplicacion:[
        {
          texto:"En el tiempo de Jesús muchas personas le siguieron por muchas razones distintas. ¿Qué tipo de personas le siguieron y por qué? (Matero 4:23,25; 4:18,22; Lucas 18:41,43)"
        },{
          texto:"¿Crees que en la actualidad vemos estas 4 categorías de personas siguiendo a Jesús? ¿Cuál es la marca en sus vidas?"
        },{
          texto:"Jesús sólo reconoció a uno de estos 4 grupos como verdaderos discípulos. ¿Cuál y por qué? (Mateo 7:21)"
        },{
          texto:"¿Qué faltaba en la vida de estos otros 3 grupos? ¿Qué nos enseña esto acerca de los requisitos como puntos de inicio para un verdadero discipulado?  (Mateo 4:17,19,22; Juan 1:12; 3:5,7)"
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Un verdadero discípulo inicia por escuchar y obedecer el llamado de Cristo. Pero ¿cómo puede una persona escuchar el llamado de Cristo para seguirlo en la actualidad? Sugiera formas ( Romanos 10:14-17, Juan 16:8-11)"
        },{
          texto:"¿Qué quiso dar a entender Jesús cuando dijo “toma mi yugo y aprende de mí” (vs 29)?. ¿Cuáles son dos requisitos del discipulado que hay en estas palabras? ¿Cómo aplican a tu propia vida?"
        },{
          texto:"¿Cuál es el mayor propósito del discipulado? (vs29, Marcos 1:17; Romanos 8:29, 2 Cor 3:18?"
        },{
          texto:"Alguien ha dicho “la conversión es sólo el 5 % de la vida cristiana 95% es andar con Cristo”.  A partir de lo que has aprendido en este estudio ¿estás de acuerdo no? ¿Por qué?"
        }
      ]
    },{
      aplicacion:[
        {
          texto:"¿Cómo puede una persona tener este entendimiento profundo de Jesús? ¿Qué se requiere? (Mateo 16:7; 1Corintios2 :11-13)"
        },{
          texto:"Un resultado importante de seguir a Cristo es el crecimiento el conocimiento sobre uno mismo. ¿Cuál fue la primera lección que Pedro aprendió acerca de sí mismo en el bote? (Lucas 5:1,7) ¿Qué cosas acera de ti mismo aún no entiendes?"
        },{
          texto:"En la actualidad hay muchos caminos abiertos para que nosotros sigamos. ¿Cuáles son algunos de estos caminos? ¿Crees que tienen la respuesta ala vida? (Mateo 6:24, 1 Timoteo 6: 6,10) "
        }
      ]
    },{
      aplicacion:[
        {
          texto:"¿Cuáles son algunas características de un verdadero discípulo cristiano que notas en tu propia vida?"
        },{
          texto:"¿Cuáles son algunas características de un verdadero discípulo cristiano con las que luchas? ¿Le has entregado eso al Señor?"
        },{
          texto:"Empieza hoy a identificar y a discipular a una que conozcas que pueda eventualmente discipular a otras personas."
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Revisa las habilidades de liderazgo en esta lección e intenta y aplica al menos un fielemente en una semana y después de un par de semana, deberías de manejar estas habilidades tan bien que podrás crecer como líder. "
        }
      ]
    },{
      aplicacion:[
        {
          texto:""
        },{
          texto:""
        },{
          texto:""
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Empieza con decisiones pequeñas que sólo afecten tu vida y ve los resultados. "
        },{
          texto:"Aprende a tomar decisiones juntos, como pareja. "
        },{
          texto:"Vas a construir tu nivel de confianza para tomar decisiones más grandes cuando veas que las pequeñas están dando fruto."
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Identifica áreas dentro de la iglesia en la que necesitas ayuda y encuentra a las personas correctas que ya están ahí y que lo pueden hacer."
        },{
          texto:"Utiliza al personal dentro de la iglesia para estas áreas de responsabilidad."
        },{
          texto:"Cuando delegues responsabilidades, delega también autoridad."
        },{
          texto:"Empieza con tareas fáciles al inicio."
        },{
          texto:"Recuerda que como líder no puedes hacer todo, por eso es que Dios ha traído personas a la iglesia para que te ayuden en tu liderazgo."
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Trata de utilizar tu tiempo sabiamente escribiendo que es lo que necesitas hacer durante el día y haciendo un esfuerzo por cumplirlo."
        },{
          texto:"Determina en tu corazón estar a tiempo en todo lugar que seas requerido."
        },{
          texto:"Trata de llegar a la iglesia a tiempo. Honras a Dios al hacerlo. "
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Lee Gálatas 5:22 y medita en esa escritura tomando uno de los frutos del Espíritu cada día y haciendo un estudio más profundo al respecto hasta que entiendas qué es y cómo lo puedes vivir todos los días de maneras prácticas. "
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Trata de hacer de la lectura de la palabra Dios un hábito diario."
        },{
          texto:"Pídele a Dios que te revele lo que Él tiene para ti para ese día. Dios siempre tendrá una palabra para ti pero tienes que pedírsela. "
        },{
          texto:"La lectura de la Biblia tiene que ser acompañada por la oración, Dios te habla a través de la Palabra, y tu le hablas a Dios mediante la oración. "
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Busca el rostro de Dios diariamente para que sepas quien es Él. "
        },{
          texto:"¡Cuando lo conoces puedes reconocer su voz!"
        },{
          texto:"Cuando te hable es importante que obedezcas su voz, aunque no entiendas que es lo que te está diciendo. El entendimiento puede esperar, la obediencia no. "
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Toma tiempo para volver a leer esta lección y empezar a confesar la palabra de Dios diariamente. Cuando lo haces estás entronando a Dios sobre tu situación, llenando tu tanque de fe y empezando a caminar en victoria. "
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Identifica una situación que estés enfrentando justo donde estás a punto de darte por vencido y aplica los principios de caminar por fe de esta lección. Observa como Dios trabaja y como tu fe será levantada a otro nivel. "
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Como un creyente nacido de nuevo del espíritu o misionero, yo estoy equipado espiritualmente para vivir victoriosamente y para ser fructífero en el ministerio. Filipenses 4:13 dice “todo lo puedo en Cristo que me fortalece”. "
        },{
          texto:"Para ser victorioso y fructífero en el ministerio viviré y caminaré en amor y humildad como Jesucristo quien es mi modelo."
        },{
          texto:"Para tener la impartición de Su poder divino, hay que estar equipado ampliamente para cada buena obra y ser maduro y completo, sin falta de nada."
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Orar y ayunar por libertad en mi vida."
        },{
          texto:"Fidelidad en los periodos de ayuno de la iglesia."
        },{
          texto:"Hacer todo en oración."
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Conoces las áreas débiles de tu vida. Pídele a Dios que te ayude a lidiar con ellas y también a compartir las luchas que tienes en estas áreas con hermanos y hermanas cristianos maduros que te puedan ayudar. "
        },{
          texto:"Comprométete a observar y orar por estas áreas débiles."
        },{
          texto:"Lidia con ellas individualmente hasta que seas victorioso en cada área. "
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Aplica los principios que has aprendido primero en casa."
        },{
          texto:"Asegúrate de ser fiel con las cosas pequeñas primero. Si puedes, Dios te confiará con las grandes.  "
        },{
          texto:"Aprende a ser responsable y a rendir cuentas con todo lo que haces. Cuando algo ale mal toma la responsabilidad, admítelo, aprende de ello y continua hacia delante. "
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Reconoce tu autoridad y ora por ellos como oras por ti mismo, sométete a ellos como Cristo se sometió al Padre y a la Iglesia. "
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Vive hoy para amar a Dios con todo tu corazón, alma, mente y fuerza."
        },{
          texto:"Vive hoy para amar a tu vecino, especialmente con quienes te odian, te han lastimado, maldecido y a quienes identificas como tus enemigos. "
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Si sabes que tu relación con alguien no está bien, no tienes otra opción que asegurarte que sea reparada. Se practico con esta lección y toma la iniciativa de hacerlo, esa es la mejor manera de aplicar esta lección en tu vida. "
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Discute las siguientes preguntes y aplica las respuestas en tu matrimonio:<br>" +
            "1.\t¿Por qué Dios estableció el matrimonio como pilar de la sociedad?<br>" +
            "2.\tUna cosa es estar casado, otra es permanecer casado. ¿Cómo puedes asegurarte que tu matrimonio durará hasta la muerte?<br>" +
            "3.\tUna relación duradera “empieza conmigo”. ¿Qué piensas que necesitas hacer como esposa o esposo para que esto suceda?<br>"
        }
      ]
    },{
      aplicacion:[
        {
          texto:"“No te puedes acercar a Dios si estás distante de tu hermano” - santo anónimo. Pon en práctica las guías anteriores para que nos ayuden como creyentes a tener una comunión los unos con los otros, a pesar de nuestras diferencias."
        }
      ]
    },{
      aplicacion:[
        {
          texto:"No hay ningún cristiano que individualmente pueda funcionar efectivamente."
        },{
          texto:"Ningún miembro de la comunidad supernatural es más o menos importante que el otro. "
        },{
          texto:"Debemos “conectarnos” en la vida relacional real si queremos experimentar la realidad de ser miembros los unos de los otros. "
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Se una persona que ora y lee la Biblia, y que desarrolla su relación con Dios."
        },{
          texto:"Lucha por ser un buen jugador en equipo, sométete y apoya a tu líder. Relaciónate bien con tus compañeros y con las personas en general."
        },{
          texto:"Si no estás en un equipo únete a uno para que puedas crecer. "
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Juan 13:34-35 “34 Un mandamiento nuevo os doy: Que os améis unos a otros; como yo os he amado, que también os améis unos a otros. 35 En esto conocerán todos que sois mis discípulos, si tenéis amor los unos por los otros”."
        },{
          texto:"1 Juan 3:16 “16 En esto hemos conocido el amor, en que él puso su vida por nosotros; también nosotros debemos poner nuestras vidas por los hermanos”."
        },{
          texto:"La esencia en la membresía de la iglesia está contenida en la voluntad de comprometerse el uno con el otro en un pacto de membresía. Nuestra relación del uno con el otro está basada en un pacto. "
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Reflexiona en esta lección y pregúntate como puedes cultivar un estilo de vida que presente a Cristo a otros más a través de las acciones que de las palabras."
        },{
          texto:"¿Cómo sabes que tipo de enfoque de evangelismo es mejor usar en una situación particular?"
        },{
          texto:"Si Jesús estuviera ahora mismo en la tierra, ¿cuál crees que sería el método de evangelismo que él usaría?"
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Puedo empezar cada día orando al Señor para que me direccione a una persona que pueda tener una necesidad y a quien eventualmente pueda guiar al Señor. "
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Cultiva y nutre una relación diaria con el Espíritu Santo a través de la oración y de la Palabra. "
        },{
          texto:"Aprende a escuchar incluso a la voz más baja y corta del Espíritu Santo hablándote a través de la palabra de Dios, las experiencias, las impresiones o las circunstancias. "
        },{
          texto:"Proponte conocerlo mejor y también Su ministerio para ti y para otros. "
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Dedícate a la oración, al ayuno y al estudio de la Palabra, rinde tu cuerpo a Dios como un instrumento de justicia, y aléjate del pecado. "
        },{
          texto:"Dedícate al llamado de Dios para tu vida y al ministerio que te ha dado. "
        }
      ]
    },{
      aplicacion:[
        {
          texto:"¿Estás actualmente lleno con el Espíritu Santo o estás tratando de aguantar con la unción del año pasado?"
        },{
          texto:"Escribe las 6 profecías del Espíritu Santo al final de los tiempos en Hechos 2:17-19, y mira a ver como eso aplica en tu vida y ministerio. "
        },{
          texto:"¿Cuántas de las 5 señales sobrenaturales de Marcos 16, 17 y18 operan en tu vida?"
        },{
          texto:"¿Has experimentado el toque de Dios sobre tu vida? Si no es así, ¿estás hambriento y sediento por Dios y su unción?"
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Ora, arrepiéntete de tus pecados delante de Dios y pide que te perdone."
        },{
          texto:"Reconoce a Jesucristo como tu Señor y Salvador."
        },{
          texto:"Pídele a Dios que te llene con Su espíritu."
        },{
          texto:"Por fe recibe al Espíritu Santo en tu vida."
        }
      ]
    },{
      aplicacion:[
        {
          texto:"A la luz de este estudio, ¿qué tan a menudo intentas, cultivas y alimentas tus sentidos de escucha espiritual para que puedas conocer la voz del Espíritu Santo?"
        },{
          texto:"¿Cuáles son algunas de las cosas en tu vida que necesitas hacer para ser consciente del espíritu?"
        },{
          texto:"¿Crees que renovar la mente con la palabra de Dios es importante? Si es así, ¿por qué es importante y que vas a hacer al respecto? ¿Cómo se relaciona esto con la idea de ser guiado por el Espíritu?"
        },{
          texto:"¿Por qué crees que amar la verdad es significativo para ti y tu crecimiento espiritual?"
        },{
          texto:"¿Hay alguna relación entre ser guiado por el Espíritu y la madurez espiritual? Si es así, ¿cuál es y cómo lo puedes cultivar?"
        }
      ]
    },{
      aplicacion:[
        {
          texto:"¿Qué tan abierta y rendida está tu vida al Espíritu Santo para que Sus dones fluyan a través de ti?"
        },{
          texto:"¿Estás orando y creyéndole al Espíritu Santo para que libere los dones de profecía para edificar a la iglesia a través de ti? Si no es así, ¿por qué no?"
        },{
          texto:"Hechos 4:29-31 es una oración del Nuevo Testamento para el coraje. Escríbela en una ficha y ora esta oración con regularidad."
        },{
          texto:"¿Vienes el domingo a la iglesia esperando que el Señor te unja y te use en la operación de los dones espirituales? Si no es así toma la decisión hoy en acuerdo con 1 Corintios 14:26"
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Pídele a Dios que te bautice con el Espíritu Santo, y haz de orar en lenguas una prioridad diaria en tu relación con Dios."
        }
      ]
    },{
      aplicacion:[
        {
          texto:"¿Cómo han afectado tu vida estas verdades dinámicas sobre la sanidad?"
        },{
          texto:"¿Has ministrado recientemente a alguien enfermo y Dios los sanó? ¿Qué fue lo que pasó?"
        },{
          texto:"¿Has orado pidiéndole al Señor que te use en el ministerio de sanidad?"
        },{
          texto:"Toma acción contra las obras del diablo, comprométete en la oración por las personas enfermas de forma regular. "
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Ora por libertad<br>" +
            "Señor Jesucristo creo que eres el hijo del Dios viviente, que moriste en la cruz y que resucitaste triunfalmente al tercer día, venciendo la muerte, el infierno, a Satanás y a todos los espíritus malos. Reconozco hoy mi necesidad de liberación de las influencias demoniacas sobre mi vida. Admito que a través de estilos de vida que no son tuyos he abierto las puertas para que ellos traigan acoso, tormentos y esclavitudes sobre mi vida. Me arrepiento de todos estos caminos y pido tu perdón sobre todos mis pecados. En particular confieso… (se específico)<br>"
        },{
          texto:"Renuncia<br>" +
            "Aun más renuncio a todo involucramiento con lo oculto o las sociedades secretas y de ahora en adelante escojo vivir para ti.<br>"
        },{
          texto:"Expulsando demonios<br>" +
            "Señor Jesús tomo autoridad de todas las influencias demoniacas sobre la vida de estas personas y en el nombre de Jesús les ordeno irse y no regresar nunca más. <br>"
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Toma algunos minutos para preguntarle al Espíritu Santo a quien quieres que perdones. Has una lista. "
        },{
          texto:"Deja que el Espíritu Santo te muestre que es lo que hay en tu corazón (amargura, orgullo, miedo, etc)."
        },{
          texto:"Perdona a las personas en la lista. Se específico de lo que han hecho."
        },{
          texto:"Perdónate por cualquier pecado que hayas cometido, especialmente por aquel que te cuesta dejar ir."
        },{
          texto:"Perdona a Dios por cualquier ofensa percibida. (Nota: Dios nunca te ha hecho nada malo)"
        },{
          texto:"Arrepiéntete por cualquier respuesta impía que hayas tenido contra la persona que te ofendió."
        },{
          texto:"Pídele al Espíritu Santo que te llene con su amor y sabiduría para saber como relacionarte con estas personas si aun mantienes relación con ellas."
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Construye tu confianza en la ministración de consejería y de sanidad interior empezando con tus amistades o personas con las que te sientas cómodo."
        },{
          texto:"Desarrollas las habilidades necesarias, has contacto visual, habilidades de escucha y realiza las preguntas correctas. "
        },{
          texto:"Tal vez tu mismo necesitas sanidad interior, así que siéntate con aquellos que tienen dones en esta área y aprende de ellos a medida que ellos te ministran. "
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Asegúrate de que estás listo espiritualmente antes de hacer guerra espiritual."
        },{
          texto:"Recuerda, no te involucres en la guerra espiritual si tu relación no está bien con Dios y con las personas."
        },{
          texto:"Perdona, se lleno del Espíritu, ora y prepárate con la palabra de Dios."
        },{
          texto:"Finalmente, guarda tu vida espiritual en diligencia después de la batalla, y siempre dale la gloria y alabanza a Dios que merece porque la batalla le pertenece al Señor. "
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Reflexiona sobre esta lección y discute como puedes ser un miembro productivo de tu célula."
        },{
          texto:"¿Qué crees que se necesita para asegurar que tienes un grupo celular saludable, en crecimiento y que se multiplica?"
        },{
          texto:"¿Qué estarías dispuesto a hacer para atraer a más personas a la célula?"
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Pregúntate lo siguiente conforme guías tu grupo celular:<br>"+
            "1.\t¿Estoy propiciando el crecimiento de los miembros de mi célula en su relación con Dios y entre ellos?<br>" +
            "2.\t¿Yo estoy creciendo como líder?<br>" +
            "3.\t¿Cuándo quiero ver que mi grupo celular de a luz a uno nuevo?<br>" +
            "4.\t¿Tengo a las personas correctas, en las que me he entregado, para que tomen el liderazgo de la nueva célula?<br>" +
            "5.\t¿Puede mi grupo celular actual sostenerse sin mi?<br>"
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Conoce a las personas bajo tu cuidado."
        },{
          texto:"Aprende a llamarlos por nombre. Eso significa mucho."
        },{
          texto:"Visítalos a menudo y conoce sus familias. "
        },{
          texto:"Celebra días importantes en su vida como cumpleaños, aniversario de bodas, etc. "
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Para posicionarme donde mejor calzo en las misiones<br>" +
            "1.\t¿Eres un socio de los tiempos de la cosecha?<br>" +
            "2.\t¿Con qué frecuencia das al mundo de las misiones?<br>" +
            "3.\t¿Tienes una semilla preciosa cuando se trata del Domingo misionero global?<br>" +
            "4.\t¿Has adoptado una nación y te has comprometido a orar por los grupos de personas?<br>" +
            "5.\t¿Oras por nuestros misioneros y otras organizaciones misioneras que están vitalmente involucradas en la misión?<br>"
        }
      ]
    },{
      aplicacion:[
        {
          texto:"La tarea de completar la Gran Comisión en las zonas de nuestras iglesias locales y en los grupos celulares debe seguir el siguiente proceso.<br>" +
            "1.\tLa predicación del evangelio del reino utilizando metodologías ministeriales de evangelismo y plantación de iglesias ya existentes en la iglesia.<br>" +
            "2.\tDiscipulado a los salvos:<br>" +
            "a.\tFacilitar enseñanzas bíblicas en diferentes niveles, utilizando los manuales de base disponibles en la iglesia.<br>" +
            "b.\tEquipar a los creyentes para que participen en actividades ministeriales de la iglesia local. <br>" +
            "c.\tSeleccionar aquellos que serán entrenados en las universidades bíblicas para desarrollar ministerios de tiempo completo.<br>" +
            "3.\tSer una iglesia local que envía misioneros."
        }
      ]
    },{
      aplicacion:[
        {
          texto:"Empieza con tu vecino y orar por los perdidos en tu propia área de influencia: lugar de trabajo, miembros de tu familia extendida, lugar donde haces deporte y cualquier otro espacio."
        }
      ]
    }
  ];
  public unregisterBackButtonAction: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController,public platform: Platform) {
    this.captura=this.navParams.get('modulo');
    this.contenido=this.aplicaciones[this.captura.modulo[0].idModulo-1];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AplicacionPage');
    console.log(this.captura);
    this.initializeBackButtonCustomHandler(this.captura,this.viewCtrl);
  }
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  dismiss() {
    this.captura.modulo[0].datos[3].color="#e94b67"
    this.viewCtrl.dismiss(this.captura);
  }

  initializeBackButtonCustomHandler(captura,ctr): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(function(event){
      console.log('Prevent Back Button Page Change');
      captura.modulo[0].datos[3].color="#e94b67";
      ctr.dismiss(captura);
    }, 101); // Priority 101 will override back button handling (we set in app.component.ts) as it is bigger then priority 100 configured in app.component.ts file */
  }
}
