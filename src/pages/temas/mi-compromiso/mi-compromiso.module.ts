import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MiCompromisoPage } from './mi-compromiso';

@NgModule({
  declarations: [
    MiCompromisoPage,
  ],
  imports: [
    IonicPageModule.forChild(MiCompromisoPage),
  ],
})
export class MiCompromisoPageModule {}
