import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, Platform, ViewController} from 'ionic-angular';

/**
 * Generated class for the MiCompromisoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mi-compromiso',
  templateUrl: 'mi-compromiso.html',
})
export class MiCompromisoPage {
  contenido:any;
  captura:any
  public unregisterBackButtonAction: any;

  miCompromiso=[
    {texto:"Me comprometo a la visión de CMF y todos sus componentes. Creo que es una visión de Dios para el mundo. Como miembro de la iglesia CMF me comprometo a apropiarme de la visión, y también con aquellos a los que Dios ha ordenado como líderes de este movimiento. "},
    {texto:"El primer propósito del ser humano es adorar a Dios. Esta es la razón principal para su existencia. Este también es mi compromiso. "},
    {texto:"Me comprometo a ser agradecido con Dios en todo y continuamente elevar alabanzas a Dios. Elijo por tanto cumplir con la voluntad de Dios de dar gracias en cualquier circunstancia porque esta es su voluntad en Cristo Jesús para mí. "},
    {texto:"Señor rindo mi vida a Ti y comprometo mi vida a adorarte en espíritu y verdad. Comprometo mi vida a esperar y anhelar Tu presencia así como el ciervo brama por las aguas. Me comprometo a tu dirección y guía. "},
    {texto:"Me comprometo a hacer la oración una prioridad en mi vida. "},
    {texto:"Como hijo de Dios, salvo por gracia, me comprometo a ser un dador generoso. Ya tomé una determinación, honraré al Señor con mis posesiones, y el primer fruto de todos mis ingresos. Me comprometo a ser un diezmador consistente y un dador generoso, así como Él provee y guía. Confieso que soy bendecido, y he sido bendecido en cada área para ser generoso en cada área y con todas las personas. "},
    {texto:"Si Cristo no es el Señor de todo, El no es Señor de nada. El llamado de Cristo es el llamado más alto de todos, y escuchar su llamado es muy importante porque es aquí donde se encuentra nuestro destino. Mateo 22:14  “Porque muchos son llamados, pero pocos son escogidos”. "},
    {texto:"Aparta un tiempo para cada día hablar con El que te ha llamado. Busca escuchar y obedecer la voz del Señor y estudia para ser aprobado por Dios. "},
    {texto:"Entiendo que ser un discípulo es una decisión que debo tomar. Estoy haciendo el compromiso para ser un discípulo de Cristo, para seguirlo a El todo el camino, vivir por el El y ser su testigo al mundo. "},
    {texto:"Como creyente estoy comprometido a la Gran Comisión de ir a todo el mundo y ganar a tantos como pueda en el Reino de Dios, y hacer discípulos de Jesucristo. Escojo ser un creyente que se reproduce comprometiendo mi vida con aquellos que son fieles, que están disponibles y que que pueden enseñar a otros. "},
    {texto:"Me comprometo a ser el líder que Dios quiere que yo sea. El liderazgo es una habilidad que debo aprender y en esta lección determino poner estos principios en práctica para poder ser efectivo en mi liderazgo, en el lugar en el que Dios me ha llamado a servir. "},
    {texto:"Me comprometo a ser un líder decidido. Debo aprender a no tener miedo al fracaso al tomar decisiones, pero si fallo determino aprender de eso, a hacer las correcciones necesarias y a seguir adelante. "},
    {texto:"Ya que delegar es bíblico no tengo otra opción como líder mas que hacerlo. Sé que esto  aligerará mi carga para que yo me pueda enfocar en los asuntos espirituales importantes en el manejo de la iglesia, y también para empoderar a los miembros de la iglesia a hacer cosas en las cuales tienen talento para que también puedan crecer y ser responsables. ¡Me comprometo a hacer esto!"},
    {texto:"Me comprometo a honrar a Dios con mayordomía inteligente de mi tiempo. Trataré de llegar a tiempo donde sea que me necesiten, y animar a otros a hacer lo mismo. "},
    {texto:"Me comprometo a buscar a Dios con mayor profundidad en mi vida de oración, a adorarlo con todo mi corazón y a cavar más profundo en Su Palabra para crecer a la plenitud de Cristo y desarrollar las cualidades necesarias para compartir mi fe efectivamente con todas las personas. "},
    {texto:"Me comprometo a la lectura diaria, al estudio, a la meditación y memorizar la Palabra de Dios. "},
    {texto:"Al pasar por este estudio he aprendido la importancia de escuchar la voz de Dios. Me comprometo a pasar tiempo en Su presencia para poder conocerlo mejor, y también para reconocer su voz a medida que me habla. "},
    {texto:"Me comprometo a creer, obedecer y confesar la Palabra de Dios sobre cualquier situación que enfrente en mi vida. Me rehúso a creer las mentiras del diablo y el testimonio del mundo. Soy un hijo especial y único de Dios, lavado y limpiado por la sangre del cordero, redimido y perdonado por todos mis pecados y estoy sentado con Cristo a la mano derecha del Padre. Este es quien soy y lo que creo y con lo que estoy comprometido por el resto de mi vida. Me comprometo a vivir en justicia, a caminar en la voluntad del Señor, poner mi fe en la fiabilidad de Su palabra y aplicarlo en mi vida diaria. "},
    {texto:"Hoy he decidido vivir por fe y confiar en Dios en todas las áreas de mi vida. Reconozco mi necesidad de total dependencia de Dios, que es fe en acción en la presencia de problemas, desafíos y dificultades. Coloco mi fe en la fidelidad de Dios. Le enseñaré a otros a confiar en la fidelidad de Dios y a caminar en fe con Dios. "},
    {texto:"Me comprometo a estar en el armario de <br>" +
        "•\tMi relación íntima diaria con el Espíritu Santo.<br>" +
        "•\tMi estudio bíblico diario y personal, permitiendo que la Palabra de Dios me enseñe, reprenda y me corrija para estar equipado.<br>" +
        "•\tIr por dificultades con gozo, creyendo que producirá madurez y totalidad en mi vida. <br>"},
    {texto:"Me comprometo a la oración regular y a la vida del ayuno. Animaré a otros a ejercitarse regularmente en el ayuno y en la oración. Seguiré buscando la dirección y revelación de Dios al pasar tiempo con el Señor en ayuno y oración."},
    {texto:"Me comprometo a la oración y a la Palabra de Dios para vencer cualquier tentación que venga a mi camino. Estoy determinado a estar en el lugar correcto en el tiempo correcto y asociarme con personas que me ayuden a huir de la tentación."},
    {texto:"Entiendo que Dios es dueño de todo y que yo simplemente soy un mayordomo de lo que Él ha confiado a mi cuidado. Sabiendo también que tendré que rendir cuentas a Dios por como gestione todo lo que se me ha dado, me comprometo a ser fiel en todas mis responsabilidades y transparente en todas mis negociaciones. Me comprometo a ser cuidadoso con el manejo del dinero y a nunca dejarlo ser mi amo, solo mi sirviente. "},
    {texto:"Muchas epístolas (Efesios, Colosenses, 1 Timoteo, 1 Pedro) hablan sobre estas áreas de sumisión. Fuera de estas áreas están el engaño y el peligro. Nuestra cobertura y protección vienen cuando humildemente nos sometemos a la autoridad señalada por Dios. "},
    {texto:"Comprometo mi vida a priorizar, construir y mantener una relación amorosa y de cuido con los demás, a pesar de su estatus, y a esforzarme por tener una relación íntima y personal con mi Señor Jesucristo. "},
    {texto:"Como cristiano sé que todo en la vida está basado en una buena relación. Primero, debo asegurarme que mi relación con Dios es buena y debo mostrar eso a través de mi relación con otros. El gran mandamiento es amar a Dios primero y después, que es igualmente importante, amar a los demás. Esto habla de desarrollar buenas relaciones."},
    {texto:"Como un líder casado me comprometo a ser fiel en mi matrimonio, amar a mi cónyuge y ser un buen ejemplo para mis hijos en como deben de tratar a su cónyuge cuando se casen. "},
    {texto:"Me comprometo a<br>" +
        "1.\tAmar a los demás con la misma compasión que Cristo mostró con los suyos. (Juan 13:34-35; 15:12)<br>" +
        "2.\tCultivar el espíritu de humildad que busca el honor de la otra persona, no el mío. (Filipenses 2:3-5) <br>" +
        "3.\tAliviar las cargas de compañeros creyentes al compartir las cargas los unos con los otros. (Gálatas 6:2)<br>" +
        "4.\tCompartir cualquier posesión con los hermanos y hermanas en necesidad. (2 Corintios 9:13).<br>" +
        "5.\tCorregir con amabilidad a un pecador mientras que ayudo a buscar soluciones para el problema. (Gálatas 6:1)<br>" +
        "6.\tEscuchar a un compañero creyente en tiempos de sufrimiento. (I Corintios 12:16)<br>" +
        "7.\tOrar por los demás en el espíritu sin cesar. (Efesios 6:18). <br>"},
    {texto:"Como miembro del mismo cuerpo comprometo mi vida a desarrollar una relación de amor y cuido mediante el trabajo en equipo y el entendimiento mutuo de honrar que somos diferentes, pero iguales en el juego de propósitos y roles para completar el propósito mayor."},
    {texto:"•\tEstaré disponible y permitiré que en mi trabaje el propósito de Dios. <br>" +
        "•\tCon el Espíritu Santo lucharé para ser un jugador efectivo en equipo, trabajar duro para que los miembros confíen en mi. <br>" +
        "•\tMe comprometo a amar y a cuidar a mis compañeros de equipo, a ayudarles y asistirles en cualquier manera que pueda. <br>" +
        "•\tPermaneceré en las tareas y en la misión en la que sea ubicado y con el equipo estoy comprometido a trabar en línea con la visión. <br>"},
    {texto:"Pacto de Membresía. Hay dos tipos diferentes de compromiso de acuerdo a 2 corintios 8:5 “Y no como lo esperábamos, sino que a sí mismos se dieron primeramente al Señor y luego a nosotros, por la voluntad de Dios”<br>" +
        " El primero es tu compromiso con Cristo para salvación y el segundo es comprometerte con otros cristianos para la membresía en nuestra iglesia de familia. Tenemos que estar comprometidos el uno con el otro, así como lo estamos con Cristo Jesús. <br>"},
    {texto:"Como cristiano creo que soy un evangelista en como me presento a las personas de manera diaria a través de mis palabras, conducta personal y de mis obras. Me comprometo a ser primeramente esa persona, incluso antes de abrir mi boca para compartir la Palabra de Dios. "},
    {texto:"Entendiendo que el último mandado del Señor es alcanzar a quienes están perdidos, entonces me comprometo a guiar a más personas a un conocimiento salvador de Cristo Jesús. "},
    {texto:"A través de este estudio me doy cuenta que el Espíritu Santo es la persona más importante en el presente y lo necesito a Él para todas las áreas de mi vida. Me comprometo a conocer, entender, apreciar y experimentar la persona y el trabajo del Espíritu Santo en mi vida."},
    {texto:"Tu dedicación al Señor determinará lo que ordenas, te pondrá en un lugar de autoridad, poder y dominio (Moisés sobre el Faraón)."},
    {texto:"Señor sé que no puedo vivir de los avivamientos del pasado y las unciones pasadas que he experimentado. Hoy estoy haciendo un compromiso para ser lleno del Espíritu Santo y caminar en una unción fresca diariamente en el Nombre de Jesús. "},
    {texto:"Una cosa es saber como ser lleno del Espíritu y otra es ser lleno del Espíritu y permanecer lleno del Espíritu de Dios. Me comprometo a recibir y a mantener al Espíritu Santo en mi vida en el nombre de Jesús. "},
    {texto:"Reconozco en este estudio que sin la guía del Espíritu Santo nunca sería capaz de entender el propósito de Dios para mi vida y continuar viviendo en el centro de Su voluntad. Mi deseo es amar la verdad y amar la verdad significa amar al Espíritu Santo y Su Palabra. Me comprometo a someterme completamente a la guía del Espíritu Santo y a hacer cualquier cosa que Él me diga o ir donde quiera que yo vaya sin cuestionarlo. "},
    {texto:"Mi Dios me rindo al Espíritu Santo como una vasija deseosa, y abro mi corazón, mi mente y mi vida a ti para que trabajes a través de mí liberando tus dones espirituales para satisfacer las necesidades de las personas a mi alrededor y ser un siervo fiel para tu Reino y gloria. Amén. "},
    {texto:"Entre más hable en lenguas, entre más ore y adore a Dios en lenguas, más manifestación de los otros donde tendré en mi vida. Y entre menos lenguas hable menos manifestación de los dones sobrenaturales del Espíritu Santo estarán operando en mi vida.  De hoy en adelante me comprometo y me rindo totalmente al Espíritu Santo que sabe que es lo que viene en el futuro, y estoy totalmente convencido de que, si somos sensibles y obedientes, Él nos mostrará las cosas por venir. (Juan 16:13). Él nos equipará, uno a uno, para lo que hay más adelante en la vida."},
    {texto:"Señor Jesús sé que tu voluntad para mi es que fluya en tu poder de sanidad y sea un instrumento de operación de milagros de tus manos sanadoras para los enfermos, los que están en sufrimiento y los afligidos de mi generación. Con esto, ¡Comprometo mi vida a ti y a tu ministerio de sanidad, liberación y libertad en ¡el nombre de Jesús!"},
    {texto:"Proverbios 28:13 “El que oculta sus pecados no prosperará, pero el que los confiesa y se aparta de ellos alcanzará misericordia”. Señor Jesucristo me comprometo a dejar toda mi rebelión y pecado. Me someto a ti como Señor. "},
    {texto:"Ahora que sé que Dios no me perdonará si no perdono a otros, me comprometo a perdonar a todos quienes me han herido de una u otra manera, y los suelto para yo también poder ser libre."},
    {texto:"Como trabajador cristiano me comprometo a estudiar esta lección cuidadosamente para estar totalmente preparado para proveer consejería y sanidad interior cuando así se necesite. Prometo depender solamente del Espíritu Santo, a medida que Él me guía a toda verdad y a revela las áreas en mi propia vida que necesito resolver antes de ministrar a otros. "},
    {texto:"Me comprometo a la guerra espiritual porque es mi responsabilidad atar y echar fuera demonios en el nombre de Jesús con la autoridad que se nos ha encomendado para hacerlo."},
    {texto:"Me comprometo a ser un miembro fiel y funcional en un grupo celular, a dar lo mejor de mi para que crezca espiritual, social, relacional y numéricamente, y asegurarme de que se reproduzca y de a luz a nuevas células. "},
    {texto:"Me comprometo a una relación más profunda con Dios en oración y en la Palabra para ser intencional dirigiendo y desarrollando mis grupos celulares a su máxima capacidad. También me comprometo a la multiplicación del liderazgo para que a medida que el grupo celular se multiplique pueda ser auto sostenible con los lideres correctos."},
    {texto:"Me comprometo a ser un pastor fiel del rebaño bajo mi cuidado sabiendo que Dios me ha confiado con esta responsabilidad sagrada. "},
    {texto:"Me comprometo con <br>" +
        "•\tSembrar en el Mundo de la Misión. <br>" +
        "•\tDarles a otros la oportunidad de sembrar en el Mundo de la Misión. <br>" +
        "•\tOrar por los misioneros.<br>" +
        "•\tOrar por los grupos no alcanzados. <br>"},
    {texto:"Como el cumplimiento de la Gran Comisión es una prioridad de la iglesia CMFI, renuevo mi compromiso a una vida de <br>" +
        "•\tExaltación de Jesucristo, es decir a la adoración.<br>" +
        "•\tProclamar Su evangelio, es decir al evangelismo.<br>"},
    {texto:"Me comprometo a un tiempo todos los días para orar por las naciones alrededor del mundo, dar apoyo al trabajo misionero e ir a una misión de corto o largo plazo. "},
    {texto:"Ahora que entiendo el importante rol que tiene la misión en la vida de la iglesia me comprometo a hacer todo lo que pueda para crear sensibilización y consciencia en la iglesia acerca de nuestro involucramiento en la misión. "},
    ];
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController,public platform: Platform) {
    this.captura=this.navParams.get('modulo');
    this.contenido=this.miCompromiso[this.captura.modulo[0].idModulo-1];


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MiCompromisoPage');
    this.initializeBackButtonCustomHandler(this.captura,this.viewCtrl);

  }
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  dismiss() {
    this.captura.modulo[0].datos[2].color="#e94b67";
    this.viewCtrl.dismiss(this.captura);
  }
  initializeBackButtonCustomHandler(captura,ctr): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(function(event){
      console.log('Prevent Back Button Page Change');
      captura.modulo[0].datos[2].color="#e94b67";
      ctr.dismiss(captura);
    }, 101); // Priority 101 will override back button handling (we set in app.component.ts) as it is bigger then priority 100 configured in app.component.ts file */
  }
}
