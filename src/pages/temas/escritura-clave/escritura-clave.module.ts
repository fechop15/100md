import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EscrituraClavePage } from './escritura-clave';

@NgModule({
  declarations: [
    EscrituraClavePage,
  ],
  imports: [
    IonicPageModule.forChild(EscrituraClavePage),
  ],
})
export class EscrituraClavePageModule {}
