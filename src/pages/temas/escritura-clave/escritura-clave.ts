import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, Platform, ViewController} from 'ionic-angular';

/**
 * Generated class for the EscrituraClavePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-escritura-clave',
  templateUrl: 'escritura-clave.html',
})
export class EscrituraClavePage {

  contenido:any;
  captura:any;
  public unregisterBackButtonAction: any;

  escrituras=[
    {
      citas:[
        {
          versiculo:"Mateo 22:37- 40",
          contenido:"37 “Ama al Señor tu Dios con todo tu corazón, con todo tu ser y con toda tu mente”[c] —le respondió Jesús—. 38 Este es el primero y el más importante de los mandamientos. 39 El segundo se parece a este: “Ama a tu prójimo como a ti mismo” 40 De estos dos mandamientos dependen toda la ley y los profetas."},
        {
          versiculo:"Mateo 28:19-20 ",
          contenido:"19 Por tanto, vayan y hagan discípulos de todas las naciones, bautizándolos en el nombre del Padre y del Hijo y del Espíritu Santo, 20 enseñándoles a obedecer todo lo que les he mandado a ustedes. Y les aseguro que estaré con ustedes siempre, hasta el fin del mundo."
        }
        ]
    },{
      citas:[
        {
          versiculo:"Juan 4: 23-24",
          contenido:"23 Pero se acerca la hora, y ha llegado ya, en que los verdaderos adoradores rendirán culto al Padre en espíritu y en verdad,[d] porque así quiere el Padre que sean los que le adoren. 24 Dios es espíritu, y quienes lo adoran deben hacerlo en espíritu y en verdad."}
      ]
    },{
      citas:[
        {
          versiculo:"Salmo 95:1-4",
          contenido:"1 Vengan, cantemos con júbilo al Señor; aclamemos a la roca de nuestra salvación.  2 Lleguemos ante él con acción de gracias, aclamémoslo con cánticos. 3 Porque el Señor es el gran Dios, el gran Rey sobre todos los dioses 4 En sus manos están los abismos de la tierra; suyas son las cumbres de los montes."}
      ]
    },{
      citas:[
        {
          versiculo:"Juan 4:23",
          contenido:"Pero se acerca la hora, y ha llegado ya, en que los verdaderos adoradores rendirán culto al Padre en espíritu y en verdad, porque así quiere el Padre que sean los que le adoren"}
      ]
    },{
      citas:[
        {
          versiculo:"Mateo 7: 7-8",
          contenido:"7 Pidan, y se les dará; busquen, y encontrarán; llamen, y se les abrirá. 8 Porque todo el que pide, recibe; el que busca, encuentra; y al que llama, se le abre."}
      ]
    },{
      citas:[
        {
          versiculo:"Proverbios 3:9-10",
          contenido:"Honra al Señor con tus riquezas y con los primeros frutos de tus cosechas. 10 Así tus graneros se llenarán a reventa y tus bodegas rebosarán de vino nuevo."}
      ]
    },{
      citas:[
        {
          versiculo:"Mateo 4:19-20",
          contenido:"19 Y les dijo: Venid en pos de mí, y os haré pescadores de hombres. 20 Ellos entonces, dejando al instante las redes, le siguieron."}
      ]
    },
    {
      citas:[
        {
          versiculo:"Mateo 11:28-30",
          contenido:"28 Venid a mí todos los que estáis trabajados y cargados, y yo os haré descansar. 29 Llevad mi yugo sobre vosotros, y aprended de mí, que soy manso y humilde de corazón; y hallaréis descanso para vuestras almas; 30 porque mi yugo es fácil, y ligera mi carga."}
      ]
    },{
      citas:[
        {
          versiculo:"Marcos 3:13-16",
          contenido:"13 después subió al monte, y llamó a sí a los que él quiso; y vinieron a él. 14 Y estableció a doce, para que estuviesen con él, y para enviarlos a predicar, 15 y que tuviesen autoridad para sanar enfermedades y para echar fuera demonios: 16 a Simón, a quien puso por sobrenombre Pedro;"}
      ]
    },{
      citas:[
        {
          versiculo:"Marcos 3:13-16",
          contenido:"13 después subió al monte, y llamó a sí a los que él quiso; y vinieron a él. 14 Y estableció a doce, para que estuviesen con él, y para enviarlos a predicar, 15 y que tuviesen autoridad para sanar enfermedades y para echar fuera demonios: 16 a Simón, a quien puso por sobrenombre Pedro;"}
      ]
    },{
      citas:[
        {
          versiculo:"Salmo 78:72",
          contenido:"72 Y los apacentó conforme a la integridad de su corazón, los pastoreó con la pericia de sus manos"}
      ]
    },{
      citas:[
        {
          versiculo:"Proverbios 19:21",
          contenido:"21 Muchos pensamientos hay en el corazón del hombre, pero el consejo de Jehová es el que permanece."}
      ]
    },{
      citas:[
        {
          versiculo:"Génesis 39:5",
          contenido:"Desde el momento en que le dio el encargo de su casa y de todo lo que tenía, Jehová bendijo la casa del egipcio a causa de José, y la bendición de Jehová estaba sobre todo lo que tenía, tanto en la casa como en el campo."}
      ]
    },{
      citas:[
        {
          versiculo:"Eclesiastés 1:1, 14 ",
          contenido:"1 Todo tiene su tiempo, y todo lo que se quiere debajo del cielo tiene su hora. 14 Sé que todo lo que Dios hace es perpetuo: Nada hay que añadir ni nada que quitar. Dios lo hace para que los hombres teman delante de él."}
      ]
    },{
      citas:[
        {
          versiculo:"1 Samuel 16:7",
          contenido:"Pero Jehová respondió a Samuel:\n—No mires a su parecer, ni a lo grande de su estatura, porque yo lo desecho; porque Jehová no mira lo que mira el hombre, pues el hombre mira lo que está delante de sus ojos, pero Jehová mira el corazón.\n"}
      ]
    },{
      citas:[
        {
          versiculo:"Hechos 20:32",
          contenido:"“Y ahora, hermanos, os encomiendo a Dios y a la palabra de su gracia, que tiene poder para sobreedificaros y daros herencia con todos los santificados”"}
      ]
    },{
      citas:[
        {
          versiculo:"Juan 10: 27",
          contenido:"“27 Mis ovejas oyen mi voz y yo las conozco, y me siguen”"}
      ]
    },{
      citas:[
        {
          versiculo:"Proverbios 18:21",
          contenido:"“La muerte y la vida están en poder de la lengua; el que la ama, comerá de sus frutos”."}
      ]
    },{
      citas:[
        {
          versiculo:"Marcos 9:23",
          contenido:"“Jesús le dijo: —Si puedes creer, al que cree todo le es posible”."}
      ]
    },{
      citas:[
        {
          versiculo:"1 Juan 5:4-5",
          contenido:"“4 porque todo lo que es nacido de Dios vence al mundo; y ésta es la victoria que ha vencido al mundo, nuestra fe. 5 ¿Quién es el que vence al mundo, sino el que cree que Jesús es el Hijo de Dios?”"}
      ]
    },{
      citas:[
        {
          versiculo:"Salmo 35:13-14",
          contenido:"“13 Pero yo, cuando ellos enfermaron, me vestí con ropas ásperas; afligí con ayuno mi alma y mi oración se volvía a mi seno. 14 Como por mi compañero, como por mi hermano andaba; como el que trae luto por madre, enlutado me humillaba”"}
      ]
    },{
      citas:[
        {
          versiculo:"Santiago 1:13-15",
          contenido:"“13 Cuando alguno es tentado no diga que es tentado de parte de Dios, porque Dios no puede ser tentado por el mal ni él tienta a nadie; 14 sino que cada uno es tentado, cuando de su propia pasión es atraído y seducido. 15 Entonces la pasión, después que ha concebido, da a luz el pecado; y el pecado, siendo consumado, da a luz la muerte”"}
      ]
    },{
      citas:[
        {
          versiculo:"Salmo 24:1",
          contenido:"“De Jehová es la tierra y su plenitud, el mundo y los que en él habitan”"}
      ]
    },{
      citas:[
        {
          versiculo:"Efesios 5:21",
          contenido:"“Someteos unos a otros en el temor de Dios”."},
        {
          versiculo:"1 Pedro 5:5",
          contenido:"“…revestíos de humildad…”"}
      ]
    },{
      citas:[
        {
          versiculo:"Mateo 22:37-39",
          contenido:"37 Jesús le dijo: —“Amarás al Señor tu Dios con todo tu corazón, con toda tu alma y con toda tu mente.”38 Éste es el primero y grande mandamiento. 39 Y el segundo es semejante: “Amarás a tu prójimo como a ti mismo.”"}
      ]
    },{
      citas:[
        {
          versiculo:"Mateo 4:19",
          contenido:"“19 Y les dijo: —Venid en pos de mí…”"}
      ]
    },{
      citas:[
        {
          versiculo:"Génesis 2:24",
          contenido:"“24 Por tanto dejará el hombre a su padre y a su madre, se unirá a su mujer y serán una sola carne”. "}
      ]
    },{
      citas:[
        {
          versiculo:"1 Juan 1:3",
          contenido:"“lo que hemos visto y oído, eso os anunciamos, para que también vosotros tengáis comunión con nosotros; y nuestra comunión verdaderamente es con el Padre y con su Hijo Jesucristo”"}
      ]
    },{
      citas:[
        {
          versiculo:"Juan 13:34-35",
          contenido:"“34 Un mandamiento nuevo os doy: Que os améis unos a otros; como yo os he amado, que también os améis unos a otros. 35 En esto conocerán todos que sois mis discípulos, si tenéis amor los unos por los otros”"}
      ]
    },{
      citas:[
        {
          versiculo:"Romanos 15:5-6",
          contenido:"“5 Y el Dios de la paciencia y de la consolación os dé entre vosotros un mismo sentir según Cristo Jesús, 6 para que unánimes, a una voz, glorifiquéis al Dios y Padre de nuestro Señor Jesucristo”."}
      ]
    },{
      citas:[
        {
          versiculo:"Mateo 16:16 -18",
          contenido:"“16 Respondiendo Simón Pedro, dijo: —Tú eres el Cristo, el Hijo del Dios viviente.17 Entonces le respondió Jesús: —Bienaventurado eres, Simón, hijo de Jonás, porque no te lo reveló carne ni sangre, sino mi Padre que está en los cielos. 18 Y yo también te digo que tú eres Pedro, y sobre esta roca edificaré mi iglesia, y las puertas del Hades no la dominarán”."}
      ]
    },{
      citas:[
        {
          versiculo:"Mateo 24:14",
          contenido:"“14 Y será predicado este evangelio del Reino en todo el mundo, para testimonio a todas las naciones, y entonces vendrá el fin”."}
      ]
    },{
      citas:[
        {
          versiculo:"Mateo 28:18-20",
          contenido:"“18 Jesús se acercó y les habló diciendo: «Toda potestad me es dada en el cielo y en la tierra.19 Por tanto, id y haced discípulos a todas las naciones, bautizándolos en el nombre del Padre, del Hijo y del Espíritu Santo, 20 y enseñándoles que guarden todas las cosas que os he mandado. Y yo estoy con vosotros todos los días, hasta el fin del mundo. Amén”."}
      ]
    },{
      citas:[
        {
          versiculo:"Lucas 4:1",
          contenido:"“Jesús, lleno del Espíritu Santo, volvió del Jordán y fue llevado por el Espíritu al desierto”"}
      ]
    },{
      citas:[
        {
          versiculo:"Lucas 4:18",
          contenido:"“«El Espíritu del Señor está sobre mí, por cuanto me ha ungido para dar buenas nuevas a los pobres; me ha enviado a sanar a los quebrantados de corazón, a pregonar libertad a los cautivos y vista a los ciegos, a poner en libertad a los oprimidos”"}
      ]
    },{
      citas:[
        {
          versiculo:"Hechos 1:8",
          contenido:"“8 pero recibiréis poder cuando haya venido sobre vosotros el Espíritu Santo, y me seréis testigos en Jerusalén, en toda Judea, en Samaria y hasta lo último de la tierra”."}
      ]
    },{
      citas:[
        {
          versiculo:"Hechos 2:33",
          contenido:"“Así que, exaltado por la diestra de Dios y habiendo recibido del Padre la promesa del Espíritu Santo, ha derramado esto que vosotros veis y oís”."}
      ]
    },{
      citas:[
        {
          versiculo:"Romanos 8:13-15",
          contenido:"“13 Porque si viviereis conforme a la carne, moriréis; mas si por el espíritu mortificáis las obras de la carne, viviréis.14 Porque todos los que son guiados por el Espíritu de Dios, los tales son hijos de Dios. 15 Porque no habéis recibido el espíritu de servidumbre para estar otra vez en temor; mas habéis recibido el espíritu de adopción, por el cual clamamos, Abba, Padre”."}
      ]
    },{
      citas:[
        {
          versiculo:"Hechos 2:17-18",
          contenido:"“17 En los postreros días —dice Dios—, derramaré de mi Espíritu sobre toda carne, y vuestros hijos y vuestras hijas profetizarán; vuestros jóvenes verán visiones y vuestros ancianos soñarán sueños; 18 y de cierto sobre mis siervos y sobre mis siervas, en aquellos días derramaré de mi Espíritu, y profetizarán”."}
      ]
    },{
      citas:[
        {
          versiculo:"Hechos 2:1-4",
          contenido:"“Cuando llegó el día de Pentecostés estaban todos unánimes juntos. 2 De repente vino del cielo un estruendo como de un viento recio que soplaba, el cual llenó toda la casa donde estaban; 3 y se les aparecieron lenguas repartidas, como de fuego, asentándose sobre cada uno de ellos. 4 Todos fueron llenos del Espíritu Santo y comenzaron a hablar en otras lenguas, según el Espíritu les daba que hablaran”"}
      ]
    },{
      citas:[
        {
          versiculo:"Hechos 10:38",
          contenido:"“cómo Dios ungió con el Espíritu Santo y con poder a Jesús de Nazaret, y cómo éste anduvo haciendo bienes y sanando a todos los oprimidos por el diablo, porque Dios estaba con él”."}
      ]
    },{
      citas:[
        {
          versiculo:"Marcos 16:17",
          contenido:"“Estas señales seguirán a los que creen: En mi nombre echarán fuera demonios, hablarán nuevas lenguas”"}
      ]
    },{
      citas:[
        {
          versiculo:"Mateo 6:14-15",
          contenido:"“Por tanto, si perdonáis a los hombres sus ofensas, os perdonará también a vosotros vuestro Padre celestial; 15 pero si no perdonáis sus ofensas a los hombres, tampoco vuestro Padre os perdonará vuestras ofensas”."}
      ]
    },{
      citas:[
        {
          versiculo:"Éxodo 18:19",
          contenido:"“Oye ahora mi voz: yo te aconsejaré y Dios estará contigo. Preséntate tú por el pueblo delante de Dios, y somete tú los asuntos a Dios”."}
      ]
    },{
      citas:[
        {
          versiculo:"2 Corintios 10:3-5",
          contenido:"“3 Aunque andamos en la carne, no militamos según la carne, 4 porque las armas de nuestra milicia no son carnales, sino poderosas en Dios para la destrucción de fortalezas, 5 derribando argumentos y toda altivez que se levanta contra el conocimiento de Dios, y llevando cautivo todo pensamiento a la obediencia a Cristo”"}
      ]
    },{
      citas:[
        {
          versiculo:"Hebreos 10:25",
          contenido:"“no dejando de congregarnos, como algunos tienen por costumbre, sino exhortándonos; y tanto más, cuanto veis que aquel día se acerca”."}
      ]
    },{
      citas:[
        {
          versiculo:"Hebreos 10:24-25",
          contenido:"“24 Y considerémonos unos a otros para estimularnos al amor y a las buenas obras, 25 no dejando de congregarnos, como algunos tienen por costumbre, sino exhortándonos; y tanto más, cuanto veis que aquel día se acerca”."}
      ]
    },{
      citas:[
        {
          versiculo:"Juan 10:10-14",
          contenido:"“10 El ladrón no viene sino para hurtar, matar y destruir; yo he venido para que tengan vida, y para que la tengan en abundancia. 11 Yo soy el buen pastor; el buen pastor su vida da por las ovejas. 12 Pero el asalariado, que no es el pastor, de quien no son propias las ovejas, ve venir al lobo y deja las ovejas y huye, y el lobo arrebata las ovejas y las dispersa. 13 Así que el asalariado huye porque es asalariado y no le importan las ovejas.14 Yo soy el buen pastor y conozco mis ovejas, y las mías me conocen”"}
      ]
    },{
      citas:[
        {
          versiculo:"Mateo 28:19",
          contenido:"“Por tanto, id y haced discípulos a todas las naciones, bautizándolos en el nombre del Padre, del Hijo y del Espíritu Santo”"}
      ]
    },{
      citas:[
        {
          versiculo:"Mateo 28:18-20",
          contenido:"“Jesús se acercó y les habló diciendo: «Toda potestad me es dada en el cielo y en la tierra. 19 Por tanto, id y haced discípulos a todas las naciones, bautizándolos en el nombre del Padre, del Hijo y del Espíritu Santo, 20 y enseñándoles que guarden todas las cosas que os he mandado. Y yo estoy con vosotros todos los días, hasta el fin del mundo”"}
      ]
    },{
      citas:[
        {
          versiculo:"Marcos 16:15",
          contenido:"“Y les dijo: —Id por todo el mundo y predicad el evangelio a toda criatura”."}
      ]
    },{
      citas:[
        {
          versiculo:"Romanos 10:14-15",
          contenido:"“14 ¿Cómo, pues, invocarán a aquel en el cual no han creído? ¿Y cómo creerán en aquel de quien no han oído? ¿Y cómo oirán sin haber quien les predique? 15 ¿Y cómo predicarán si no son enviados? Como está escrito: «¡Cuán hermosos son los pies de los que anuncian la paz, de los que anuncian buenas nuevas!”"}
      ]
    }
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController,public platform: Platform) {
    this.captura=this.navParams.get('modulo');
    //console.log(this.captura.modulo[0]);
    this.contenido=this.escrituras[this.captura.modulo[0].idModulo-1];

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EscrituraClavePage');
    console.log(this.captura);
    this.initializeBackButtonCustomHandler(this.captura,this.viewCtrl);

  }
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  dismiss() {
    this.captura.modulo[0].datos[0].color="#e94b67";
    this.viewCtrl.dismiss(this.captura);
  }
  initializeBackButtonCustomHandler(captura,ctr): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(function(event){
      console.log('Prevent Back Button Page Change');
      captura.modulo[0].datos[0].color="#e94b67";
      ctr.dismiss(captura);
    }, 101); // Priority 101 will override back button handling (we set in app.component.ts) as it is bigger then priority 100 configured in app.component.ts file */
  }
}
