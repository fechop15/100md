import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario2Page } from './cuestionario2';

@NgModule({
  declarations: [
    Cuestionario2Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario2Page),
  ],
})
export class Cuestionario2PageModule {}
