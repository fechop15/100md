import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario26Page } from './cuestionario26';

@NgModule({
  declarations: [
    Cuestionario26Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario26Page),
  ],
})
export class Cuestionario26PageModule {}
