import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario23Page } from './cuestionario23';

@NgModule({
  declarations: [
    Cuestionario23Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario23Page),
  ],
})
export class Cuestionario23PageModule {}
