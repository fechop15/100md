import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario46Page } from './cuestionario46';

@NgModule({
  declarations: [
    Cuestionario46Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario46Page),
  ],
})
export class Cuestionario46PageModule {}
