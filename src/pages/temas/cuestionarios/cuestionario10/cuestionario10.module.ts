import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario10Page } from './cuestionario10';

@NgModule({
  declarations: [
    Cuestionario10Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario10Page),
  ],
})
export class Cuestionario10PageModule {}
