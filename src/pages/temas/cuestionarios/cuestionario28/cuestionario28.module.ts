import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario28Page } from './cuestionario28';

@NgModule({
  declarations: [
    Cuestionario28Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario28Page),
  ],
})
export class Cuestionario28PageModule {}
