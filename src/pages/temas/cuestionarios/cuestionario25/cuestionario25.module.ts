import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario25Page } from './cuestionario25';

@NgModule({
  declarations: [
    Cuestionario25Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario25Page),
  ],
})
export class Cuestionario25PageModule {}
