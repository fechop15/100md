import {Component, ViewChild} from '@angular/core';
import {
  ActionSheetController,
  AlertController,
  IonicPage,
  NavController,
  NavParams,
  Platform, Slides,
  ViewController
} from 'ionic-angular'; import {AuthServiceProvider} from "../../../../providers/auth-service/auth-service";

/**
 * Generated class for the Cuestionario25Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cuestionario25',
  templateUrl: 'cuestionario25.html',
})
export class Cuestionario25Page {
  @ViewChild(Slides) slides: Slides;
  ocultar1: boolean;
  ocultar2: boolean;
  ocultar3: boolean;
  respuestas=[
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },

  ];
  possibleButtons=[
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Esencia",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Amor",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Sobreabundante",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Abundante",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Conectada",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Conexión ",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Amor sobreabundante",valor: 1 }
    ],
    [
      { text: "Acuerdo vinculante",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Lazo ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Imagen ",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Semejanza",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Expresarse",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Continua",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Diseño",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Nueva creación ",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Parte",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Relación ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Rol vital",valor: 1 }
    ],
    [
      { text: "Desconectado",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Afecta ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Odio",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Mató",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Priorizada",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Vida",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Luz ",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Completa ",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Esperanza",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Vital ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Seria",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Íntima ",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Sabios",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Agradable",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Perdón ",valor: 1 }
    ],
    [
      { text: "Reconciliarnos",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Libera",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Fe",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Falle ",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Durmió ",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Conecta ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Ilumina. ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],

  ];
  captura: any;
  public unregisterBackButtonAction: any;
  constructor(public navCtrl: NavController,public navParams: NavParams,public actionSheetCtrl: ActionSheetController,
              public alertCtrl: AlertController,public authServiceProvider: AuthServiceProvider,public viewCtrl: ViewController,public platform: Platform) {

    this.ocultar1 = false;     this.ocultar2 = true;     this.ocultar3 = false;     this.captura=this.navParams.get('modulo');
    console.log(this.captura);
    for (let index in this.possibleButtons) {
      for (let index2 in this.possibleButtons[index]){
        if (this.possibleButtons[index][index2].text=="mala") {
          this.possibleButtons[index][index2].text=this.authServiceProvider.ramdomRespuestas();
        }
      }
    }
    console.log("ramdom");
    //validar si esta marcada completada
    if (this.captura.modulo[0].datos[1].color=="#e94b67"){
      for (let index in this.respuestas){
        for (let j = 0; j < 4; j++) {
          if (this.possibleButtons[index][j].valor==1) {
            this.respuestas[index].text = this.possibleButtons[index][j].text;
            this.respuestas[index].valor = this.possibleButtons[index][j].valor;
          }
        }
      }
    }else{
      if (localStorage.getItem("progreso25")) {
        const data = JSON.parse(localStorage.getItem("progreso25"));
        console.log(data);
        this.respuestas=data;
      }else{
        localStorage.setItem("progreso25", JSON.stringify(this.respuestas));
        //console.log(this.progreso);
      }
    }

    for (let index in this.respuestas) {
      if (this.respuestas[index].text==" ") {
        this.respuestas[index].text = " ";
      }
    }
    console.log("tamaño "+this.possibleButtons.length);
  }

  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Cuestionario1Page');
    this.initializeBackButtonCustomHandler(this.captura,this.viewCtrl);

  }

  openActionSheet(pos) {
    console.log("opening");
    let actionsheet = this.actionSheetCtrl.create({
      title: "Selecciona una respuesta",
      buttons: this.createButtons(pos)
    });
    actionsheet.present();
  }

  createButtons(pos) {
    let buttons = [];
    for (let j = 0; j < 4; j++) {
      let button = {
        text: this.possibleButtons[pos][j].text,
        icon: "checkmark",
        handler: () => {
          this.respuestas[pos].text = this.possibleButtons[pos][j].text;
          this.respuestas[pos].valor = this.possibleButtons[pos][j].valor;
          localStorage.setItem("progreso25", JSON.stringify(this.respuestas));
          return true;
        }
      };
      buttons.push(button);
    }
    return buttons;
  }

  calificar(){
    let correctas=0;
    let malas=0;
    for (let index in this.respuestas) {
      if (this.respuestas[index].valor==1){
        correctas++;
      }else{
        malas++;
        this.respuestas[index].color="#e94b67";
      }
    }
    if (correctas==this.respuestas.length){
       if(this.captura.modulo[0].datos[1].color!="#e94b67"){         localStorage.removeItem("progreso25");       }
      this.showAlert("Felicidades has respondido todas las preguntas bien");
      this.captura.modulo[0].datos[1].color="#e94b67";
      this.initializeBackButtonCustomHandler(this.captura,this.viewCtrl);


    } else{
      this.showAlert("ups, encontramos "+malas+" respuestas malas, revisa las respuestas" );
    }

  }

  showAlert(texto) {
    const alert = this.alertCtrl.create({
      title: 'Resultados',
      subTitle: texto,
      buttons: ['OK']
    });
    alert.present();
  }

  dismiss() {
    this.viewCtrl.dismiss(this.captura);
  }

  initializeBackButtonCustomHandler(captura,ctr): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(function(event){
      console.log('Prevent Back Button Page Change');
      ctr.dismiss(captura);
    }, 101); // Priority 101 will override back button handling (we set in app.component.ts) as it is bigger then priority 100 configured in app.component.ts file */
  }
  siguiente() {
    let currentIndex = this.slides.getActiveIndex();
    this.slides.slideTo(currentIndex+1, 500);
    this.ocultar1=true;
    if(this.slides.isEnd()){
      this.ocultar2=false;
      this.ocultar3=true;
    }else {
      this.ocultar2=true;
      this.ocultar3=false;
    }

  }

  atras() {
    let currentIndex = this.slides.getActiveIndex();
    this.slides.slideTo(currentIndex-1, 500);
    if(this.slides.isBeginning()){
      this.ocultar1=false;
    }
    if(this.slides.isEnd()){
      this.ocultar2=false;
      this.ocultar3=true;
    }else {
      this.ocultar2=true;
      this.ocultar3=false;
    }
  }
  slideChanged() {
    if (this.slides.isBeginning()) {
      this.ocultar1 = false;
    }else{
      this.ocultar1 = true;
    }
    if (this.slides.isEnd()) {
      this.ocultar2 = false;
      this.ocultar3 = true;
    } else {
      this.ocultar2 = true;
      this.ocultar3 = false;
    }
  }
}
