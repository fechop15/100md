import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario27Page } from './cuestionario27';

@NgModule({
  declarations: [
    Cuestionario27Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario27Page),
  ],
})
export class Cuestionario27PageModule {}
