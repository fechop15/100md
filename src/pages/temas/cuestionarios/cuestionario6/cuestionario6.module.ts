import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario6Page } from './cuestionario6';

@NgModule({
  declarations: [
    Cuestionario6Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario6Page),
  ],
})
export class Cuestionario6PageModule {}
