import {Component, ViewChild} from '@angular/core';
import {
  ActionSheetController,
  AlertController,
  IonicPage,
  NavController,
  NavParams, Platform, Slides,
  ViewController
} from 'ionic-angular';
import {AuthServiceProvider} from "../../../../providers/auth-service/auth-service";

/**
 * Generated class for the Cuestionario6Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cuestionario6',
  templateUrl: 'cuestionario6.html',
})
export class Cuestionario6Page {
  @ViewChild(Slides) slides: Slides;
  ocultar1: boolean;
  ocultar2: boolean;
  ocultar3: boolean;
  respuestas = [
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"}

  ];
  possibleButtons = [
    [
      {text: "Habitual", valor: 0},
      {text: "Diario", valor: 0},
      {text: "Cristiano", valor: 1},
      {text: "Moderno", valor: 0}
    ],
    [
      {text: "Adoracion", valor: 1},
      {text: "Fe", valor: 0},
      {text: "Agradecimiento", valor: 0},
      {text: "Humildad", valor: 0}
    ],
    [
      {text: "Tiempo", valor: 0},
      {text: "Incremento", valor: 1},
      {text: "Dia", valor: 0},
      {text: "Semana", valor: 0}
    ],
    [
      {text: "Alabanza", valor: 0},
      {text: "Oracion", valor: 0},
      {text: "Hablar", valor: 0},
      {text: "Ofrenda", valor: 1}
    ],
    [
      {text: "Salvador", valor: 0},
      {text: "Liberador", valor: 0},
      {text: "Patron", valor: 1},
      {text: "Rey", valor: 0}
    ],

    [
      {text: "Imagen", valor: 1},
      {text: "Barro", valor: 0},
      {text: "Partir", valor: 0},
      {text: "Semejanza", valor: 0}
    ],
    [
      {text: "Imagen", valor: 0},
      {text: "Mano", valor: 0},
      {text: "Palabra", valor: 0},
      {text: "Semejanza", valor: 1}
    ],
    [
      {text: "Segundo", valor: 0},
      {text: "Primero", valor: 1},
      {text: "Tercero", valor: 0},
      {text: "Despues", valor: 0}
    ],
    [
      {text: "Amar", valor: 0},
      {text: "Buscar", valor: 0},
      {text: "Dar", valor: 1},
      {text: "Agradecer", valor: 0}
    ],

    [
      {text: "Armonia", valor: 0},
      {text: "Paz", valor: 0},
      {text: "Amor", valor: 0},
      {text: "Demostracion", valor: 1}
    ],
    [
      {text: "Proporcion", valor: 1},
      {text: "Lugar", valor: 0},
      {text: "Igual", valor: 0},
      {text: "Primer", valor: 0}
    ],
    [
      {text: "Recibido", valor: 1},
      {text: "Dado", valor: 0},
      {text: "Agregado", valor: 0},
      {text: "Amado", valor: 0}
    ],
    [
      {text: "Alegra", valor: 0},
      {text: "Desmotiva", valor: 0},
      {text: "Motiva", valor: 1},
      {text: "Entristece", valor: 0}
    ],
    [
      {text: "Oracion", valor: 0},
      {text: "Poder", valor: 0},
      {text: "Alabanza", valor: 0},
      {text: "Adoracion", valor: 1}
    ],

    [
      {text: "Alegria", valor: 0},
      {text: "Motivacion", valor: 1},
      {text: "Tristeza", valor: 0},
      {text: "Familia", valor: 0}
    ],
    [
      {text: "Mas Pequeño", valor: 0},
      {text: "Mas Grande", valor: 1},
      {text: "Grande", valor: 0},
      {text: "Mediano", valor: 0}
    ],
    [
      {text: "Amor", valor: 0},
      {text: "Tesoro", valor: 1},
      {text: "Familia", valor: 0},
      {text: "Corazon", valor: 0}
    ],
    [
      {text: "Familia", valor: 0},
      {text: "Tesoro", valor: 0},
      {text: "Amigos", valor: 0},
      {text: "Corazon", valor: 1}
    ],
    [
      {text: "Cercanos", valor: 0},
      {text: "Iguales", valor: 0},
      {text: "Inseparables", valor: 1},
      {text: "Lejanos", valor: 0}
    ],

    [
      {text: "Acercate", valor: 1},
      {text: "Alejarte", valor: 0},
      {text: "Dividirte", valor: 0},
      {text: "Quererte", valor: 0}
    ],
    [
      {text: "Minoria", valor: 0},
      {text: "Mayoria", valor: 1},
      {text: "Igual", valor: 0},
      {text: "Diferentes", valor: 0}
    ],
    [
      {text: "Tiempo", valor: 0},
      {text: "Almuerzo", valor: 0},
      {text: "Examen", valor: 1},
      {text: "Puente", valor: 0}
    ],
    [
      {text: "Examen", valor: 0},
      {text: "Ofrenda", valor: 1},
      {text: "Actitud", valor: 0},
      {text: "Bendicion", valor: 0}
    ],
    [
      {text: "Amor", valor: 0},
      {text: "Rapidez", valor: 0},
      {text: "Motivacion", valor: 0},
      {text: "Actitud", valor: 1}
    ],

    [
      {text: "Porcion", valor: 0},
      {text: "Motivacion", valor: 1},
      {text: "Cantidad", valor: 0},
      {text: "Vision", valor: 0}
    ],
    [
      {text: "Palabra", valor: 0},
      {text: "Consuelo", valor: 0},
      {text: "Directrices", valor: 1},
      {text: "Cantidades", valor: 0}
    ],
    [
      {text: "Cantidad", valor: 0},
      {text: "Actitud", valor: 0},
      {text: "Proporcion", valor: 1},
      {text: "Ofrenda", valor: 0}
    ],
    [
      {text: "Decima", valor: 1},
      {text: "Quinta", valor: 0},
      {text: "Octava", valor: 0},
      {text: "Novena", valor: 0}
    ],
    [
      {text: "Mayoria", valor: 0},
      {text: "Mitad", valor: 1},
      {text: "Octava Parte", valor: 0},
      {text: "Decima Parte", valor: 0}
    ],

    [
      {text: "Nada de", valor: 0},
      {text: "Algo de", valor: 0},
      {text: "La mitad", valor: 0},
      {text: "Todo", valor: 1}
    ],
    [
      {text: "Grande", valor: 0},
      {text: "Amoroso", valor: 0},
      {text: "Deseoso", valor: 1},
      {text: "Gozoso", valor: 0}
    ],
    [
      {text: "Que si", valor: 0},
      {text: "Gracia", valor: 0},
      {text: "Obligacion", valor: 1},
      {text: "Cantidad", valor: 0}
    ],
    [
      {text: "Ley", valor: 1},
      {text: "Opcional", valor: 0},
      {text: "Si Quiero", valor: 0},
      {text: "Mal Visto", valor: 0}
    ],
    [
      {text: "Ley", valor: 0},
      {text: "Opcional", valor: 0},
      {text: "Obligacion", valor: 1},
      {text: "Amor", valor: 0}
    ],

    [
      {text: "Amor", valor: 1},
      {text: "Gracia", valor: 0},
      {text: "Ley", valor: 0},
      {text: "Obligacion", valor: 0}
    ],
    [
      {text: "Da", valor: 0},
      {text: "Quita", valor: 0},
      {text: "Pertenece", valor: 1},
      {text: "Agrada", valor: 0}
    ],
    [
      {text: "Nombre", valor: 0},
      {text: "Puesto", valor: 0},
      {text: "Don", valor: 0},
      {text: "Diezmo", valor: 1}
    ],
    [
      {text: "Sueldo", valor: 0},
      {text: "Incremento", valor: 1},
      {text: "Salario", valor: 0},
      {text: "Trabajo", valor: 0}
    ],
    [
      {text: "Calle", valor: 0},
      {text: "Familia", valor: 0},
      {text: "Gracia", valor: 0},
      {text: "Casa", valor: 1}
    ],

    [
      {text: "Agrado", valor: 0},
      {text: "Sustento", valor: 0},
      {text: "Desagrado", valor: 0},
      {text: "Pago", valor: 1}
    ],
    [
      {text: "Encargado", valor: 0},
      {text: "Sacerdote", valor: 1},
      {text: "Familiar", valor: 0},
      {text: "Amigo", valor: 0}
    ],
    [
      {text: "Cuido", valor: 0},
      {text: "Rechazo", valor: 0},
      {text: "Alentó", valor: 1},
      {text: "Obligo", valor: 0}
    ],
    [
      {text: "Agradar", valor: 0},
      {text: "Mantener", valor: 0},
      {text: "Demostrar", valor: 1},
      {text: "Cuidar", valor: 0}
    ],
    [
      {text: "Cuidamos", valor: 0},
      {text: "Honramos", valor: 1},
      {text: "Protegemos", valor: 0},
      {text: "Amamos", valor: 0}
    ],

    [
      {text: "Bendicion", valor: 1},
      {text: "Oracion", valor: 0},
      {text: "Adoracion", valor: 0},
      {text: "Alegria", valor: 0}
    ],
    [
      {text: "Desbalance", valor: 0},
      {text: "Balance", valor: 1},
      {text: "Proporcion", valor: 0},
      {text: "Ayuda", valor: 0}
    ],
    [
      {text: "Ayuda", valor: 0},
      {text: "Cuida", valor: 0},
      {text: "Bendice", valor: 1},
      {text: "Ama", valor: 0}
    ],
    [
      {text: "Descuidamos", valor: 0},
      {text: "Bendecimos", valor: 0},
      {text: "Ayudamos", valor: 0},
      {text: "Maldecimos", valor: 1}
    ],
    [
      {text: "Macedonia", valor: 1},
      {text: "Belen", valor: 0},
      {text: "Jordan", valor: 0},
      {text: "Israel", valor: 0}
    ],

    [
      {text: "Cualidades", valor: 0},
      {text: "Defectos", valor: 0},
      {text: "Habilidades", valor: 1},
      {text: "Aptitudes", valor: 0}
    ],
    [
      {text: "Dar", valor: 0},
      {text: "Proveer", valor: 1},
      {text: "Robar", valor: 0},
      {text: "Quitar", valor: 0}
    ],
    [
      {text: "Multiplicar", valor: 1},
      {text: "Restar", valor: 0},
      {text: "Sumar", valor: 0},
      {text: "Dividir", valor: 0}
    ],
    [
      {text: "Costo", valor: 0},
      {text: "Sacrificio", valor: 1},
      {text: "Parte", valor: 0},
      {text: "Algo", valor: 0}
    ],
    [
      {text: "Simple", valor: 0},
      {text: "Alguna", valor: 0},
      {text: "Ninguna", valor: 0},
      {text: "Costosa", valor: 1}
    ],

    [
      {text: "Negado", valor: 0},
      {text: "Positivo", valor: 0},
      {text: "Aceptado", valor: 1},
      {text: "Negativo", valor: 0}
    ],
    [
      {text: "Aceptado", valor: 0},
      {text: "Rechazado", valor: 1},
      {text: "Pasado", valor: 0},
      {text: "Futuro", valor: 0}
    ],
    [
      {text: "Gracia", valor: 0},
      {text: "Alabanza", valor: 0},
      {text: "Honor", valor: 0},
      {text: "Favor", valor: 1}
    ],
    [
      {text: "Peor", valor: 0},
      {text: "Algo", valor: 0},
      {text: "Ninguno", valor: 0},
      {text: "Mejor", valor: 1}
    ],
    [
      {text: "Todo", valor: 0},
      {text: "Ninguno", valor: 0},
      {text: "Nada", valor: 1},
      {text: "Alguno", valor: 0}
    ],

    [
      {text: "No Costara", valor: 0},
      {text: "Gratis", valor: 0},
      {text: "Costara", valor: 1},
      {text: "Libre", valor: 0}
    ],
    [
      {text: "Encontro", valor: 1},
      {text: "Perdio", valor: 0},
      {text: "Lucho", valor: 0},
      {text: "Gano", valor: 0}
    ],
    [
      {text: "Adoracion", valor: 0},
      {text: "Gloria", valor: 0},
      {text: "Amistad", valor: 0},
      {text: "Salvacion", valor: 1}
    ],
    [
      {text: "Mundo", valor: 1},
      {text: "Pais", valor: 0},
      {text: "Nacion", valor: 0},
      {text: "Familia", valor: 0}
    ],
    [
      {text: "Si", valor: 0},
      {text: "No", valor: 1},
      {text: "Tal vez", valor: 0},
      {text: "Quizas", valor: 0}
    ],

    [
      {text: "Privado", valor: 0},
      {text: "Publico", valor: 1},
      {text: "Ausente", valor: 0},
      {text: "Pendiente", valor: 0}
    ],
    [
      {text: "Amor", valor: 0},
      {text: "Amabilidad", valor: 0},
      {text: "comprension", valor: 0},
      {text: "Respeto", valor: 1}
    ],
    [
      {text: "Seriamente", valor: 1},
      {text: "Con Alegria", valor: 0},
      {text: "Agradecer", valor: 0},
      {text: "Dar", valor: 0}
    ],
    [
      {text: "Planeado", valor: 1},
      {text: "Libre", valor: 0},
      {text: "Gratis", valor: 0},
      {text: "Privado", valor: 0}
    ],
    [
      {text: "Planeado", valor: 0},
      {text: "Lejano", valor: 0},
      {text: "Espontaneo", valor: 1},
      {text: "Cercano", valor: 0}
    ],

    [
      {text: "Atencion", valor: 0},
      {text: "Dirija", valor: 1},
      {text: "Va", valor: 0},
      {text: "Elija", valor: 0}
    ],
    [
      {text: "Certidumbre", valor: 0},
      {text: "Duda", valor: 0},
      {text: "Conviccion", valor: 1},
      {text: "Inseguridad", valor: 0}
    ],
    [
      {text: "Triste", valor: 0},
      {text: "Serio", valor: 0},
      {text: "Amargado", valor: 0},
      {text: "Alegre", valor: 1}
    ],
    [
      {text: "Alegremente", valor: 0},
      {text: "Tristemente", valor: 0},
      {text: "Gozosamente", valor: 1},
      {text: "Melancolicamente", valor: 0}
    ],
    [
      {text: "Mala gana", valor: 1},
      {text: "buena gana", valor: 0},
      {text: "obligado", valor: 0},
      {text: "porque si", valor: 0}
    ],

    [
      {text: "Crece", valor: 0},
      {text: "Prospera", valor: 1},
      {text: "Se Mantiene", valor: 0},
      {text: "Se Estanca", valor: 0}
    ],
    [
      {text: "Retrasando", valor: 0},
      {text: "Refrescando", valor: 1},
      {text: "invadiendo", valor: 0},
      {text: "Ayudando", valor: 0}
    ],
    [
      {text: "Mandato", valor: 0},
      {text: "Obligacion", valor: 0},
      {text: "Sacramento", valor: 1},
      {text: "Ayuda", valor: 0}
    ],
    [
      {text: "Creyentes", valor: 0},
      {text: "No creyentes", valor: 1},
      {text: "Familiares", valor: 0},
      {text: "Amigos", valor: 0}
    ],
    [
      {text: "Energico", valor: 0},
      {text: "Servicial", valor: 0},
      {text: "Perezoso", valor: 1},
      {text: "Amistoso", valor: 0}
    ],

    [
      {text: "No Trabajan", valor: 0},
      {text: "Ayudan", valor: 0},
      {text: "Trabajan", valor: 1},
      {text: "No Ayudan", valor: 0}
    ],
    [
      {text: "Hablando", valor: 0},
      {text: "Asistiendo", valor: 0},
      {text: "Predicando", valor: 1},
      {text: "Dando", valor: 0}
    ],
    [
      {text: "Alejar", valor: 0},
      {text: "Ayudar", valor: 0},
      {text: "Asistir", valor: 0},
      {text: "Apoyar", valor: 1}
    ],
    [
      {text: "Esposas", valor: 0},
      {text: "Novias", valor: 0},
      {text: "Viudas", valor: 1},
      {text: "Hermanas", valor: 0}
    ],
    [
      {text: "Ricos", valor: 0},
      {text: "Amigos", valor: 0},
      {text: "Pobres", valor: 1},
      {text: "Mentores", valor: 0}
    ]

  ];

  captura: any;
  progreso: any;
  public unregisterBackButtonAction: any;

  constructor(public navCtrl: NavController,public navParams: NavParams,public actionSheetCtrl: ActionSheetController,
              public alertCtrl: AlertController,public authServiceProvider: AuthServiceProvider,public viewCtrl: ViewController,public platform: Platform) {

    this.ocultar1 = false;     this.ocultar2 = true;     this.ocultar3 = false;     this.captura=this.navParams.get('modulo');
    console.log(this.captura);

    //validar si esta marcada completada
    if (this.captura.modulo[0].datos[1].color=="#e94b67"){
      for (let index in this.respuestas){
        for (let j = 0; j < 4; j++) {
          if (this.possibleButtons[index][j].valor==1) {
            this.respuestas[index].text = this.possibleButtons[index][j].text;
            this.respuestas[index].valor = this.possibleButtons[index][j].valor;
          }
        }
      }
    }else{
      if (localStorage.getItem("progreso6")) {
        const data = JSON.parse(localStorage.getItem("progreso6"));
        console.log(data);
        this.respuestas=data;
      }else{
        localStorage.setItem("progreso6", JSON.stringify(this.respuestas));
        //console.log(this.progreso);
      }
    }

    for (let index in this.respuestas) {
      if (this.respuestas[index].text==" ") {
        this.respuestas[index].text = " ";
      }
    }
    console.log("tamaño "+this.possibleButtons.length);
  }

  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Cuestionario1Page');
    this.initializeBackButtonCustomHandler(this.captura,this.viewCtrl);

  }

  openActionSheet(pos) {
    console.log("opening");
    let actionsheet = this.actionSheetCtrl.create({
      title: "Selecciona una respuesta",
      buttons: this.createButtons(pos)
    });
    actionsheet.present();
  }

  createButtons(pos) {
    let buttons = [];
    for (let j = 0; j < 4; j++) {
      let button = {
        text: this.possibleButtons[pos][j].text,
        icon: "checkmark",
        handler: () => {
          this.respuestas[pos].text = this.possibleButtons[pos][j].text;
          this.respuestas[pos].valor = this.possibleButtons[pos][j].valor;
          localStorage.setItem("progreso6", JSON.stringify(this.respuestas));
          return true;
        }
      };
      buttons.push(button);
    }
    return buttons;
  }

  calificar(){
    let correctas=0;
    let malas=0;
    for (let index in this.respuestas) {
      if (this.respuestas[index].valor==1){
        correctas++;
      }else{
        malas++;
        this.respuestas[index].color="#e94b67";
      }
    }
    if (correctas==this.respuestas.length){
       if(this.captura.modulo[0].datos[1].color!="#e94b67"){         localStorage.removeItem("progreso6");       }
      this.showAlert("Felicidades has respondido todas las preguntas bien");
      this.captura.modulo[0].datos[1].color="#e94b67";
      this.initializeBackButtonCustomHandler(this.captura,this.viewCtrl);


    } else{
      this.showAlert("ups, encontramos "+malas+" respuestas malas, revisa las respuestas" );
    }

  }

  showAlert(texto) {
    const alert = this.alertCtrl.create({
      title: 'Resultados',
      subTitle: texto,
      buttons: ['OK']
    });
    alert.present();
  }

  dismiss() {
    this.viewCtrl.dismiss(this.captura);
  }

  initializeBackButtonCustomHandler(captura,ctr): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(function(event){
      console.log('Prevent Back Button Page Change');
      ctr.dismiss(captura);
    }, 101); // Priority 101 will override back button handling (we set in app.component.ts) as it is bigger then priority 100 configured in app.component.ts file */
  }
  siguiente() {
    let currentIndex = this.slides.getActiveIndex();
    this.slides.slideTo(currentIndex+1, 500);
    this.ocultar1=true;
    if(this.slides.isEnd()){
      this.ocultar2=false;
      this.ocultar3=true;
    }else {
      this.ocultar2=true;
      this.ocultar3=false;
    }

  }

  atras() {
    let currentIndex = this.slides.getActiveIndex();
    this.slides.slideTo(currentIndex-1, 500);
    if(this.slides.isBeginning()){
      this.ocultar1=false;
    }
    if(this.slides.isEnd()){
      this.ocultar2=false;
      this.ocultar3=true;
    }else {
      this.ocultar2=true;
      this.ocultar3=false;
    }
  }
  slideChanged() {
    if (this.slides.isBeginning()) {
      this.ocultar1 = false;
    }else{
      this.ocultar1 = true;
    }
    if (this.slides.isEnd()) {
      this.ocultar2 = false;
      this.ocultar3 = true;
    } else {
      this.ocultar2 = true;
      this.ocultar3 = false;
    }
  }
}
