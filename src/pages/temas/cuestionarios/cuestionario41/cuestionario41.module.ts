import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario41Page } from './cuestionario41';

@NgModule({
  declarations: [
    Cuestionario41Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario41Page),
  ],
})
export class Cuestionario41PageModule {}
