import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario33Page } from './cuestionario33';

@NgModule({
  declarations: [
    Cuestionario33Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario33Page),
  ],
})
export class Cuestionario33PageModule {}
