import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario16Page } from './cuestionario16';

@NgModule({
  declarations: [
    Cuestionario16Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario16Page),
  ],
})
export class Cuestionario16PageModule {}
