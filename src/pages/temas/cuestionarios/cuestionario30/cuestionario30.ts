import {Component, ViewChild} from '@angular/core';
import {
  ActionSheetController,
  AlertController,
  IonicPage,
  NavController,
  NavParams,
  Platform, Slides,
  ViewController
} from 'ionic-angular'; import {AuthServiceProvider} from "../../../../providers/auth-service/auth-service";

/**
 * Generated class for the Cuestionario30Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cuestionario30',
  templateUrl: 'cuestionario30.html',
})
export class Cuestionario30Page {
  @ViewChild(Slides) slides: Slides;
  ocultar1: boolean;
  ocultar2: boolean;
  ocultar3: boolean;
  respuestas=[
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },

  ];
  possibleButtons=[
    [
      { text: "Equipo ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Implantado",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Hechos ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Unión ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Principio",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Desunir (disunite)",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Desasociar  ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Destruye ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Sucumbe ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Imposible ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],

    [
      { text: "Ojos",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Naturaleza ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Mueve ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Seguro ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Soporte ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Fortaleza ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Diferencia ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Crítica ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Mezcla",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Compañeros ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],

    [
      { text: "Construir",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Extensión ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Forma",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Colaborativamente",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Cooperar  ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Retroalimentarse",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Discipulado ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Individualmente ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Convertiremos",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Propósito ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],

    [
      { text: "Prevalece",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Puentes",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Dones ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Parte",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Miembros",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Cuerpo",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Complementarnos",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Mandamiento",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Saludable ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Relación ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],

    [
      { text: "Base",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Planeamiento",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Importante",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Plan ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Diseñar",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Abrazar ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Trabajar ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Visión ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Herramienta ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Fuerza",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],

    [
      { text: "Producir",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Confortables ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Unidad",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Clave",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Juntos",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Basado",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Primero",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Creceremos ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Obligatorio",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "equipo",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],

    [
      { text: "Inicios",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Logró ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Soñó",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Trabajo en equipo",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "CMFI",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Dirigido ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Creyente",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Responsabilidad ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Acerca ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Estableciendo ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],

  ];
  captura: any;
  public unregisterBackButtonAction: any;
  constructor(public navCtrl: NavController,public navParams: NavParams,public actionSheetCtrl: ActionSheetController,
              public alertCtrl: AlertController,public authServiceProvider: AuthServiceProvider,public viewCtrl: ViewController,public platform: Platform) {

    this.ocultar1 = false;     this.ocultar2 = true;     this.ocultar3 = false;     this.captura=this.navParams.get('modulo');
    console.log(this.captura);
    for (let index in this.possibleButtons) {
      for (let index2 in this.possibleButtons[index]){
        if (this.possibleButtons[index][index2].text=="mala") {
          this.possibleButtons[index][index2].text=this.authServiceProvider.ramdomRespuestas();
        }
      }
    }
    console.log("ramdom");
    //validar si esta marcada completada
    if (this.captura.modulo[0].datos[1].color=="#e94b67"){
      for (let index in this.respuestas){
        for (let j = 0; j < 4; j++) {
          if (this.possibleButtons[index][j].valor==1) {
            this.respuestas[index].text = this.possibleButtons[index][j].text;
            this.respuestas[index].valor = this.possibleButtons[index][j].valor;
          }
        }
      }
    }else{
      if (localStorage.getItem("progreso30")) {
        const data = JSON.parse(localStorage.getItem("progreso30"));
        console.log(data);
        this.respuestas=data;
      }else{
        localStorage.setItem("progreso30", JSON.stringify(this.respuestas));
        //console.log(this.progreso);
      }
    }

    for (let index in this.respuestas) {
      if (this.respuestas[index].text==" ") {
        this.respuestas[index].text = " ";
      }
    }
    console.log("tamaño "+this.possibleButtons.length);
  }

  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Cuestionario1Page');
    this.initializeBackButtonCustomHandler(this.captura,this.viewCtrl);

  }

  openActionSheet(pos) {
    console.log("opening");
    let actionsheet = this.actionSheetCtrl.create({
      title: "Selecciona una respuesta",
      buttons: this.createButtons(pos)
    });
    actionsheet.present();
  }

  createButtons(pos) {
    let buttons = [];
    for (let j = 0; j < 4; j++) {
      let button = {
        text: this.possibleButtons[pos][j].text,
        icon: "checkmark",
        handler: () => {
          this.respuestas[pos].text = this.possibleButtons[pos][j].text;
          this.respuestas[pos].valor = this.possibleButtons[pos][j].valor;
          localStorage.setItem("progreso30", JSON.stringify(this.respuestas));
          return true;
        }
      };
      buttons.push(button);
    }
    return buttons;
  }

  calificar(){
    let correctas=0;
    let malas=0;
    for (let index in this.respuestas) {
      if (this.respuestas[index].valor==1){
        correctas++;
      }else{
        malas++;
        this.respuestas[index].color="#e94b67";
      }
    }
    if (correctas==this.respuestas.length){
       if(this.captura.modulo[0].datos[1].color!="#e94b67"){         localStorage.removeItem("progreso30");       }
      this.showAlert("Felicidades has respondido todas las preguntas bien");
      this.captura.modulo[0].datos[1].color="#e94b67";
      this.initializeBackButtonCustomHandler(this.captura,this.viewCtrl);


    } else{
      this.showAlert("ups, encontramos "+malas+" respuestas malas, revisa las respuestas" );
    }

  }

  showAlert(texto) {
    const alert = this.alertCtrl.create({
      title: 'Resultados',
      subTitle: texto,
      buttons: ['OK']
    });
    alert.present();
  }

  dismiss() {
    this.viewCtrl.dismiss(this.captura);
  }

  initializeBackButtonCustomHandler(captura,ctr): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(function(event){
      console.log('Prevent Back Button Page Change');
      ctr.dismiss(captura);
    }, 101); // Priority 101 will override back button handling (we set in app.component.ts) as it is bigger then priority 100 configured in app.component.ts file */
  }
  siguiente() {
    let currentIndex = this.slides.getActiveIndex();
    this.slides.slideTo(currentIndex+1, 500);
    this.ocultar1=true;
    if(this.slides.isEnd()){
      this.ocultar2=false;
      this.ocultar3=true;
    }else {
      this.ocultar2=true;
      this.ocultar3=false;
    }

  }

  atras() {
    let currentIndex = this.slides.getActiveIndex();
    this.slides.slideTo(currentIndex-1, 500);
    if(this.slides.isBeginning()){
      this.ocultar1=false;
    }
    if(this.slides.isEnd()){
      this.ocultar2=false;
      this.ocultar3=true;
    }else {
      this.ocultar2=true;
      this.ocultar3=false;
    }
  }
  slideChanged() {
    if (this.slides.isBeginning()) {
      this.ocultar1 = false;
    }else{
      this.ocultar1 = true;
    }
    if (this.slides.isEnd()) {
      this.ocultar2 = false;
      this.ocultar3 = true;
    } else {
      this.ocultar2 = true;
      this.ocultar3 = false;
    }
  }
}
