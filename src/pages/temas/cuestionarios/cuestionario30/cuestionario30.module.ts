import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario30Page } from './cuestionario30';

@NgModule({
  declarations: [
    Cuestionario30Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario30Page),
  ],
})
export class Cuestionario30PageModule {}
