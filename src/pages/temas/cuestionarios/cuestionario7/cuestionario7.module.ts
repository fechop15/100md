import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario7Page } from './cuestionario7';

@NgModule({
  declarations: [
    Cuestionario7Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario7Page),
  ],
})
export class Cuestionario7PageModule {}
