import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario12Page } from './cuestionario12';

@NgModule({
  declarations: [
    Cuestionario12Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario12Page),
  ],
})
export class Cuestionario12PageModule {}
