import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario20Page } from './cuestionario20';

@NgModule({
  declarations: [
    Cuestionario20Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario20Page),
  ],
})
export class Cuestionario20PageModule {}
