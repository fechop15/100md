import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario35Page } from './cuestionario35';

@NgModule({
  declarations: [
    Cuestionario35Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario35Page),
  ],
})
export class Cuestionario35PageModule {}
