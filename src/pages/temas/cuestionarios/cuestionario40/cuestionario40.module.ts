import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario40Page } from './cuestionario40';

@NgModule({
  declarations: [
    Cuestionario40Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario40Page),
  ],
})
export class Cuestionario40PageModule {}
