import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario17Page } from './cuestionario17';

@NgModule({
  declarations: [
    Cuestionario17Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario17Page),
  ],
})
export class Cuestionario17PageModule {}
