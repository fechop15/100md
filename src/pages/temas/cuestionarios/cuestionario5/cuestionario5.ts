import {Component, ViewChild} from '@angular/core';
import {
  ActionSheetController,
  AlertController,
  IonicPage,
  NavController,
  NavParams, Platform, Slides,
  ViewController
} from 'ionic-angular';
import {AuthServiceProvider} from "../../../../providers/auth-service/auth-service";

/**
 * Generated class for the Cuestionario5Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cuestionario5',
  templateUrl: 'cuestionario5.html',
})
export class Cuestionario5Page {
  @ViewChild(Slides) slides: Slides;
  ocultar1: boolean;
  ocultar2: boolean;
  ocultar3: boolean;
  respuestas = [
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"}
  ];


  possibleButtons = [
    [
      {text: "Coversaciòn", valor: 1},
      {text: "Palabra", valor: 0},
      {text: "Via", valor: 0},
      {text: "Vida", valor: 0}
    ],
    [
      {text: "Arriba", valor: 0},
      {text: "En el CEntro", valor: 0},
      {text: "Debajo", valor: 1},
      {text: "Medio", valor: 0}
    ],

    [
      {text: "Amar", valor: 0},
      {text: "Sanar", valor: 1},
      {text: "Perdonar", valor: 0},
      {text: "Proteger", valor: 0}
    ],
    [
      {text: "Salvar", valor: 1},
      {text: "Sanar", valor: 0},
      {text: "Proteger", valor: 0},
      {text: "Amar", valor: 0}
    ],
    [
      {text: "Ver", valor: 0},
      {text: "Liberar", valor: 1},
      {text: "Sanar", valor: 0},
      {text: "Correr", valor: 0}
    ],
    [
      {text: "Cuidar", valor: 0},
      {text: "Amar", valor: 0},
      {text: "Proteger", valor: 1},
      {text: "Descuidar", valor: 0}
    ],
    [
      {text: "Familia", valor: 0},
      {text: "Iglesia", valor: 0},
      {text: "Amabilidad", valor: 0},
      {text: "Palabra", valor: 1}
    ],
    [
      {text: "Confianza", valor: 1},
      {text: "Compañia", valor: 0},
      {text: "Silencio", valor: 0},
      {text: "Quietud", valor: 0}
    ],
    [
      {text: "Amable", valor: 0},
      {text: "Agradecido", valor: 1},
      {text: "Cuidadoso", valor: 0},
      {text: "Gentil", valor: 0}
    ],
    [
      {text: "Familia", valor: 0},
      {text: "Mision", valor: 0},
      {text: "Relacion", valor: 1},
      {text: "Confianza", valor: 0}
    ],
    [
      {text: "Creyente", valor: 1},
      {text: "Personaje", valor: 0},
      {text: "Amigo", valor: 0},
      {text: "Familiar", valor: 0}
    ],
    [
      {text: "Errores", valor: 0},
      {text: "Malos caminos", valor: 0},
      {text: "Abusos", valor: 0},
      {text: "Tentacion", valor: 1}
    ],
    [
      {text: "Perdonar", valor: 0},
      {text: "Con", valor: 0},
      {text: "Contra", valor: 1},
      {text: "Odiar", valor: 0}
    ],
    [
      {text: "Complementan", valor: 0},
      {text: "Dividen", valor: 1},
      {text: "Unen", valor: 0},
      {text: "Despejan", valor: 0}
    ],
    [
      {text: "Compromisos", valor: 0},
      {text: "Objetivos", valor: 0},
      {text: "Perdones", valor: 0},
      {text: "Abusos", valor: 1}
    ],
    [
      {text: "Errores", valor: 0},
      {text: "Perdones", valor: 0},
      {text: "Abusos", valor: 1},
      {text: "Amores", valor: 0}
    ],
    [
      {text: "Dos", valor: 0},
      {text: "Blanda", valor: 0},
      {text: "Una", valor: 1},
      {text: "Dura", valor: 0}
    ],
    [
      {text: "Amor", valor: 0},
      {text: "Compromiso", valor: 1},
      {text: "Comprension", valor: 0},
      {text: "Ternura", valor: 0}
    ],
    [
      {text: "Prosperar", valor: 1},
      {text: "Amar", valor: 0},
      {text: "Salvar", valor: 0},
      {text: "Relacionar", valor: 0}
    ],
    [
      {text: "Tiempo", valor: 0},
      {text: "Amor", valor: 0},
      {text: "Familia", valor: 0},
      {text: "Disciplina", valor: 1}
    ],
    [
      {text: "Falta de amor", valor: 0},
      {text: "Falta de tiempo", valor: 0},
      {text: "Falta de perdon", valor: 1},
      {text: "Falta de familia", valor: 0}
    ],
    [
      {text: "Perdon", valor: 1},
      {text: "Amigo", valor: 0},
      {text: "Amor", valor: 0},
      {text: "Querer", valor: 0}
    ],
    [
      {text: "Amate", valor: 0},
      {text: "Quierete", valor: 0},
      {text: "Perdonarte", valor: 1},
      {text: "Olvida", valor: 0}
    ],
    [
      {text: "Si", valor: 0},
      {text: "Tal vez", valor: 0},
      {text: "Pueden", valor: 0},
      {text: "No", valor: 1}
    ],

  ];
  progreso: any;

  captura: any; public unregisterBackButtonAction: any;

  constructor(public navCtrl: NavController,public navParams: NavParams,public actionSheetCtrl: ActionSheetController,
              public alertCtrl: AlertController,public authServiceProvider: AuthServiceProvider,public viewCtrl: ViewController,public platform: Platform) {

    this.ocultar1 = false;     this.ocultar2 = true;     this.ocultar3 = false;     this.captura=this.navParams.get('modulo');
    console.log(this.captura);

    //validar si esta marcada completada
    if (this.captura.modulo[0].datos[1].color=="#e94b67"){
      for (let index in this.respuestas){
        for (let j = 0; j < 4; j++) {
          if (this.possibleButtons[index][j].valor==1) {
            this.respuestas[index].text = this.possibleButtons[index][j].text;
            this.respuestas[index].valor = this.possibleButtons[index][j].valor;
          }
        }
      }
    }else{
      if (localStorage.getItem("progreso5")) {
        const data = JSON.parse(localStorage.getItem("progreso5"));
        console.log(data);
        this.respuestas=data;
      }else{
        localStorage.setItem("progreso5", JSON.stringify(this.respuestas));
        //console.log(this.progreso);
      }
    }

    for (let index in this.respuestas) {
      if (this.respuestas[index].text==" ") {
        this.respuestas[index].text = " ";
      }
    }
    console.log("tamaño "+this.possibleButtons.length);
  }

  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Cuestionario1Page');
    this.initializeBackButtonCustomHandler(this.captura,this.viewCtrl);

  }

  openActionSheet(pos) {
    console.log("opening");
    let actionsheet = this.actionSheetCtrl.create({
      title: "Selecciona una respuesta",
      buttons: this.createButtons(pos)
    });
    actionsheet.present();
  }

  createButtons(pos) {
    let buttons = [];
    for (let j = 0; j < 4; j++) {
      let button = {
        text: this.possibleButtons[pos][j].text,
        icon: "checkmark",
        handler: () => {
          this.respuestas[pos].text = this.possibleButtons[pos][j].text;
          this.respuestas[pos].valor = this.possibleButtons[pos][j].valor;
          localStorage.setItem("progreso5", JSON.stringify(this.respuestas));
          return true;
        }
      };
      buttons.push(button);
    }
    return buttons;
  }

  calificar(){
    let correctas=0;
    let malas=0;
    for (let index in this.respuestas) {
      if (this.respuestas[index].valor==1){
        correctas++;
      }else{
        malas++;
        this.respuestas[index].color="#e94b67";
      }
    }
    if (correctas==this.respuestas.length){
       if(this.captura.modulo[0].datos[1].color!="#e94b67"){         localStorage.removeItem("progreso5");       }
      this.showAlert("Felicidades has respondido todas las preguntas bien");
      this.captura.modulo[0].datos[1].color="#e94b67";
      this.initializeBackButtonCustomHandler(this.captura,this.viewCtrl);


    } else{
      this.showAlert("ups, encontramos "+malas+" respuestas malas, revisa las respuestas" );
    }

  }

  showAlert(texto) {
    const alert = this.alertCtrl.create({
      title: 'Resultados',
      subTitle: texto,
      buttons: ['OK']
    });
    alert.present();
  }

  dismiss() {
    this.viewCtrl.dismiss(this.captura);
  }

  initializeBackButtonCustomHandler(captura,ctr): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(function(event){
      console.log('Prevent Back Button Page Change');
      ctr.dismiss(captura);
    }, 101); // Priority 101 will override back button handling (we set in app.component.ts) as it is bigger then priority 100 configured in app.component.ts file */
  }
  siguiente() {
    let currentIndex = this.slides.getActiveIndex();
    this.slides.slideTo(currentIndex+1, 500);
    this.ocultar1=true;
    if(this.slides.isEnd()){
      this.ocultar2=false;
      this.ocultar3=true;
    }else {
      this.ocultar2=true;
      this.ocultar3=false;
    }

  }

  atras() {
    let currentIndex = this.slides.getActiveIndex();
    this.slides.slideTo(currentIndex-1, 500);
    if(this.slides.isBeginning()){
      this.ocultar1=false;
    }
    if(this.slides.isEnd()){
      this.ocultar2=false;
      this.ocultar3=true;
    }else {
      this.ocultar2=true;
      this.ocultar3=false;
    }
  }
  slideChanged() {
    if (this.slides.isBeginning()) {
      this.ocultar1 = false;
    }else{
      this.ocultar1 = true;
    }
    if (this.slides.isEnd()) {
      this.ocultar2 = false;
      this.ocultar3 = true;
    } else {
      this.ocultar2 = true;
      this.ocultar3 = false;
    }
  }
}
