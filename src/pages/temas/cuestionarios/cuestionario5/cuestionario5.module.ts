import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario5Page } from './cuestionario5';

@NgModule({
  declarations: [
    Cuestionario5Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario5Page),
  ],
})
export class Cuestionario5PageModule {}
