import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario18Page } from './cuestionario18';

@NgModule({
  declarations: [
    Cuestionario18Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario18Page),
  ],
})
export class Cuestionario18PageModule {}
