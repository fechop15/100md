import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario15Page } from './cuestionario15';

@NgModule({
  declarations: [
    Cuestionario15Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario15Page),
  ],
})
export class Cuestionario15PageModule {}
