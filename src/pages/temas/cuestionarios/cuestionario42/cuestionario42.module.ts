import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario42Page } from './cuestionario42';

@NgModule({
  declarations: [
    Cuestionario42Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario42Page),
  ],
})
export class Cuestionario42PageModule {}
