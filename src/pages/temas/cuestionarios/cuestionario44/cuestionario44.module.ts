import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario44Page } from './cuestionario44';

@NgModule({
  declarations: [
    Cuestionario44Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario44Page),
  ],
})
export class Cuestionario44PageModule {}
