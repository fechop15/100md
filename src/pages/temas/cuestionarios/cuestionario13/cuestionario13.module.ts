import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario13Page } from './cuestionario13';

@NgModule({
  declarations: [
    Cuestionario13Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario13Page),
  ],
})
export class Cuestionario13PageModule {}
