import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario48Page } from './cuestionario48';

@NgModule({
  declarations: [
    Cuestionario48Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario48Page),
  ],
})
export class Cuestionario48PageModule {}
