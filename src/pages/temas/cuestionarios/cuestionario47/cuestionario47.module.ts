import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario47Page } from './cuestionario47';

@NgModule({
  declarations: [
    Cuestionario47Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario47Page),
  ],
})
export class Cuestionario47PageModule {}
