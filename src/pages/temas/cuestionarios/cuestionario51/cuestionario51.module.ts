import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario51Page } from './cuestionario51';

@NgModule({
  declarations: [
    Cuestionario51Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario51Page),
  ],
})
export class Cuestionario51PageModule {}
