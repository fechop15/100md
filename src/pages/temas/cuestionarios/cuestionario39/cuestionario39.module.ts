import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario39Page } from './cuestionario39';

@NgModule({
  declarations: [
    Cuestionario39Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario39Page),
  ],
})
export class Cuestionario39PageModule {}
