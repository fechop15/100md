import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario9Page } from './cuestionario9';

@NgModule({
  declarations: [
    Cuestionario9Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario9Page),
  ],
})
export class Cuestionario9PageModule {}
