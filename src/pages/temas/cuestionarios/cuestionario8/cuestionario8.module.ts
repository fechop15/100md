import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario8Page } from './cuestionario8';

@NgModule({
  declarations: [
    Cuestionario8Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario8Page),
  ],
})
export class Cuestionario8PageModule {}
