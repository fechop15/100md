import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario49Page } from './cuestionario49';

@NgModule({
  declarations: [
    Cuestionario49Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario49Page),
  ],
})
export class Cuestionario49PageModule {}
