import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario14Page } from './cuestionario14';

@NgModule({
  declarations: [
    Cuestionario14Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario14Page),
  ],
})
export class Cuestionario14PageModule {}
