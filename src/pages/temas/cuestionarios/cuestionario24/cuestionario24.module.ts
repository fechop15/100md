import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario24Page } from './cuestionario24';

@NgModule({
  declarations: [
    Cuestionario24Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario24Page),
  ],
})
export class Cuestionario24PageModule {}
