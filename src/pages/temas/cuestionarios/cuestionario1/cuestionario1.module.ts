import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario1Page } from './cuestionario1';

@NgModule({
  declarations: [
    Cuestionario1Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario1Page),
  ],
})
export class Cuestionario1PageModule {}
