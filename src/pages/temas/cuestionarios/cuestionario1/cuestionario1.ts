import {Component, ViewChild} from '@angular/core';
import {
  ActionSheetController,
  AlertController,
  IonicPage,
  NavController,
  NavParams, Platform,
  ViewController,
  Slides
} from 'ionic-angular';
import {AuthServiceProvider} from "../../../../providers/auth-service/auth-service";

/**
 * Generated class for the Cuestionario1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-cuestionario1',
  templateUrl: 'cuestionario1.html',
})
export class Cuestionario1Page {
  @ViewChild(Slides) slides: Slides;
  ocultar1: boolean;
  ocultar2: boolean;
  ocultar3: boolean;

  respuestas = [

    {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"}, {
      text: " ",
      valor: 0,
      color: "#19a7bc"
    }, {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"}, {
      text: " ",
      valor: 0,
      color: "#19a7bc"
    }, {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"}, {
      text: " ",
      valor: 0,
      color: "#19a7bc"
    }, {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"}, {
      text: " ",
      valor: 0,
      color: "#19a7bc"
    }, {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"}, {
      text: " ",
      valor: 0,
      color: "#19a7bc"
    }, {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"}, {
      text: " ",
      valor: 0,
      color: "#19a7bc"
    }, {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"}, {
      text: " ",
      valor: 0,
      color: "#19a7bc"
    }, {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"}, {
      text: " ",
      valor: 0,
      color: "#19a7bc"
    }, {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"}, {
      text: " ",
      valor: 0,
      color: "#19a7bc"
    }, {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"}, {
      text: " ",
      valor: 0,
      color: "#19a7bc"
    }, {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"}, {
      text: " ",
      valor: 0,
      color: "#19a7bc"
    }, {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"}, {
      text: " ",
      valor: 0,
      color: "#19a7bc"
    }, {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"}, {
      text: " ",
      valor: 0,
      color: "#19a7bc"
    }, {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"}, {text: " ", valor: 0, color: "#19a7bc"}
  ];

  possibleButtons = [
    [
      {text: "Gran Mandamiento", valor: 1},
      {text: "Esfuerzo", valor: 0},
      {text: "Amor", valor: 0},
      {text: "Odio", valor: 0}
    ],
    [
      {text: "Amistad", valor: 0},
      {text: "Gran Comisión", valor: 1},
      {text: "Sabiduria", valor: 0},
      {text: "Paciencia", valor: 0}
    ],
    [
      {text: "Tradición", valor: 1},
      {text: "Costumbres", valor: 0},
      {text: "Habitos", valor: 0},
      {text: "Acciones", valor: 0}
    ],
    [
      {text: "Habitos", valor: 0},
      {text: "Pertenencias", valor: 0},
      {text: "Personalidad", valor: 0},
      {text: "Finanzas", valor: 1}
    ],
    [
      {text: "Rumbo", valor: 0},
      {text: "Direccion", valor: 0},
      {text: "Propósito", valor: 1},
      {text: "Guia", valor: 0}
    ],
    [
      {text: "Enderezarlos", valor: 0},
      {text: "Reafirmarlos", valor: 1},
      {text: "Mantenerlos", valor: 0},
      {text: "Utilizarlos", valor: 0}
    ],
    [
      {text: "Evidencia", valor: 0},
      {text: "Astucia", valor: 0},
      {text: "Gracia", valor: 1},
      {text: "Direccion", valor: 0}
    ],
    [
      {text: "Afirmacion", valor: 0},
      {text: "Gracia", valor: 0},
      {text: "Habitacion", valor: 0},
      {text: "Línea de plomo", valor: 1}
    ],
    [
      {text: "Formulados", valor: 1},
      {text: "Regulados", valor: 0},
      {text: "Amarrados", valor: 0},
      {text: "Utilizados", valor: 0}
    ],
    [
      {text: "Efectiva", valor: 0},
      {text: "Dirigida", valor: 1},
      {text: "Buena", valor: 0},
      {text: "Generosa", valor: 0}
    ],
    [
      {text: "Definidas", valor: 1},
      {text: "Afirmadas", valor: 0},
      {text: "Utilizadas", valor: 0},
      {text: "Evadidas", valor: 0}
    ],
    [
      {text: "Biblia", valor: 0},
      {text: "Palabra", valor: 0},
      {text: "Adoración", valor: 1},
      {text: "Danza", valor: 0}
    ],
    [
      {text: "Amando", valor: 0},
      {text: "Adorando", valor: 1},
      {text: "Olvidando", valor: 0},
      {text: "Orando", valor: 0}
    ],
    [
      {text: "Ministerio", valor: 0},
      {text: "Compromiso", valor: 0},
      {text: "Servicio", valor: 1},
      {text: "Valor", valor: 0}
    ],
    [
      {text: "Ocupamos", valor: 1},
      {text: "Distraemos", valor: 0},
      {text: "Vamos", valor: 0},
      {text: "Entristecemos", valor: 0}
    ],
    [
      {text: "Oracion", valor: 0},
      {text: "Palabra", valor: 0},
      {text: "Biblia", valor: 0},
      {text: "Adoración", valor: 1}
    ],
    [
      {text: "Enseñados", valor: 0},
      {text: "Vivos", valor: 0},
      {text: "Dirigidos", valor: 0},
      {text: "Ordenados", valor: 1}
    ],
    [
      {text: "Deber", valor: 1},
      {text: "Adorar", valor: 0},
      {text: "Cumplir", valor: 0},
      {text: "Aparentar", valor: 0}
    ],
    [
      {text: "Gozo", valor: 0},
      {text: "Deber", valor: 0},
      {text: "Amor", valor: 1},
      {text: "Compromiso", valor: 0}
    ],
    [
      {text: "Pastor", valor: 0},
      {text: "Evangelismo", valor: 1},
      {text: "Deber", valor: 0},
      {text: "Paraiso", valor: 0}
    ],
    [
      {text: "Deber", valor: 0},
      {text: "Peso", valor: 0},
      {text: "Privilegio", valor: 1},
      {text: "Poder", valor: 0}
    ],
    [
      {text: "Gloria", valor: 0},
      {text: "Vida", valor: 0},
      {text: "Familia", valor: 1},
      {text: "Mision", valor: 0}
    ],
    [
      {text: "Estemos", valor: 0},
      {text: "A la hora", valor: 0},
      {text: "Buenas donde", valor: 0},
      {text: "Donde sea", valor: 1}
    ],
    [
      {text: "Buenas", valor: 0},
      {text: "Malas", valor: 0},
      {text: "En todo lugar", valor: 1},
      {text: "Vivas", valor: 0}
    ],
    [
      {text: "Llamandonos", valor: 0},
      {text: "Con nosotros", valor: 0},
      {text: "Vivo", valor: 1},
      {text: "Esperandonos", valor: 0}
    ],
    [
      {text: "Mandamiento", valor: 0},
      {text: "Pastor", valor: 0},
      {text: "Evangelio", valor: 0},
      {text: "Discipulado", valor: 1}
    ],
    [
      {text: "Proclamad", valor: 0},
      {text: "Bautizad", valor: 1},
      {text: "Regresar", valor: 0},
      {text: "Ver", valor: 0}
    ],
    [
      {text: "Elementos", valor: 1},
      {text: "Mandamientos", valor: 0},
      {text: "Objetivos", valor: 0},
      {text: "Propositos", valor: 0}
    ],
    [
      {text: "Vida", valor: 0},
      {text: "Mision", valor: 0},
      {text: "Asociación", valor: 1},
      {text: "Amistad", valor: 0}
    ],
    [
      {text: "Amigos", valor: 0},
      {text: "Miembros", valor: 1},
      {text: "Cercanos", valor: 0},
      {text: "Hermanos", valor: 0}
    ],
    [
      {text: "Edificar", valor: 1},
      {text: "Atraer", valor: 0},
      {text: "Mirar", valor: 0},
      {text: "Enseñar", valor: 0}
    ],
    [
      {text: "Personas", valor: 0},
      {text: "Familia", valor: 0},
      {text: "Cristo", valor: 1},
      {text: "Amigos", valor: 0}
    ],
    [
      {text: "Amarlas", valor: 0},
      {text: "Enseñar", valor: 1},
      {text: "Salvarlas", valor: 0},
      {text: "Ayudarles", valor: 0}
    ],
    [
      {text: "Grandes", valor: 0},
      {text: "Buenas", valor: 0},
      {text: "Alegres", valor: 0},
      {text: "Maduras ", valor: 1}
    ],
    [
      {text: "Discipulado", valor: 1},
      {text: "Amor", valor: 0},
      {text: "Evangelio", valor: 0},
      {text: "Compromiso", valor: 0}
    ],
    [
      {text: "Mision", valor: 0},
      {text: "Palabra", valor: 0},
      {text: "Relación de calidad", valor: 1},
      {text: "Vision", valor: 0}
    ],
    [
      {text: "Armonia", valor: 0},
      {text: "Amistad", valor: 0},
      {text: "Paz", valor: 1},
      {text: "Amor", valor: 0}
    ],
    [
      {text: "Demuestra", valor: 1},
      {text: "Procura", valor: 0},
      {text: "Predica", valor: 0},
      {text: "Muestra", valor: 0}
    ],
    [
      {text: "Estable", valor: 0},
      {text: "Buena", valor: 0},
      {text: "Duradera", valor: 1},
      {text: "Agradable", valor: 0}
    ],
    [
      {text: "Bueno", valor: 0},
      {text: "Agradable", valor: 0},
      {text: "Malo", valor: 0},
      {text: "Ministerio", valor: 1}
    ],
    [
      {text: "Base", valor: 0},
      {text: "Guia", valor: 0},
      {text: "Clave", valor: 1},
      {text: "Mision", valor: 0}
    ],
    [
      {text: "Base", valor: 1},
      {text: "Clave", valor: 0},
      {text: "Mision", valor: 0},
      {text: "Guia", valor: 0}
    ],
    [
      {text: "Guardar", valor: 0},
      {text: "Valorar", valor: 1},
      {text: "Agradecer", valor: 0},
      {text: "Llevar", valor: 0}
    ],
    [
      {text: "Amarnos", valor: 0},
      {text: "Cuidarnos", valor: 0},
      {text: "Guardarnos", valor: 0},
      {text: "Ministrarnos", valor: 1}
    ],
    [
      {text: "Vision", valor: 0},
      {text: "Misión", valor: 1},
      {text: "Amistad", valor: 0},
      {text: "Familia", valor: 0}
    ],
    [
      {text: "Pueblo", valor: 0},
      {text: "Pais", valor: 0},
      {text: "Mundo", valor: 1},
      {text: "Que pueda", valor: 0}
    ],
    [
      {text: "Elegidos", valor: 0},
      {text: "Nosotros", valor: 0},
      {text: "Familia", valor: 0},
      {text: "Llamados", valor: 1}
    ],
    [
      {text: "Velar", valor: 0},
      {text: "Orar", valor: 1},
      {text: "Caminar", valor: 0},
      {text: "Ir", valor: 0}
    ],
    [
      {text: "Invertir", valor: 1},
      {text: "Administrar", valor: 0},
      {text: "Cuidar", valor: 0},
      {text: "Cristo", valor: 0}
    ],
    [
      {text: "Traer", valor: 0},
      {text: "Iglesia", valor: 0},
      {text: "Enviar", valor: 1},
      {text: "Mision", valor: 0}
    ],
    [
      {text: "Compromiso", valor: 1},
      {text: "Poder", valor: 0},
      {text: "Cuidado", valor: 0},
      {text: "Valor", valor: 0}
    ],
    [
      {text: "Sabiduria", valor: 0},
      {text: "Familia", valor: 0},
      {text: "Inteligencia", valor: 0},
      {text: "Voluntad", valor: 1}
    ],
    [
      {text: "Evangelio", valor: 0},
      {text: "Cumplimiento", valor: 1},
      {text: "Acercamiento", valor: 0},
      {text: "Poder", valor: 0}
    ],
    [
      {text: "Queremos", valor: 0},
      {text: "Amamos", valor: 0},
      {text: "Traemos", valor: 1},
      {text: "Olvidamos", valor: 0}
    ],
    [
      {text: "Queremos", valor: 0},
      {text: "Invitamos", valor: 0},
      {text: "Mision", valor: 0},
      {text: "Edificamos", valor: 1}
    ],
    [
      {text: "Enviamos", valor: 0},
      {text: "Amamos", valor: 0},
      {text: "Entrenamos", valor: 1},
      {text: "Guiamos", valor: 0}
    ],
    [
      {text: "Enviamos", valor: 1},
      {text: "Entrenamos", valor: 0},
      {text: "Edificamos", valor: 0},
      {text: "Queremos", valor: 0}
    ],
    [
      {text: "Familia", valor: 0},
      {text: "Amigos", valor: 0},
      {text: "Miembros", valor: 1},
      {text: "Alguien", valor: 0}
    ],
    [
      {text: "Victoria", valor: 0},
      {text: "Madurez", valor: 1},
      {text: "Mision", valor: 0},
      {text: "Vision", valor: 0}
    ],
    [
      {text: "Campamento", valor: 0},
      {text: "Deber", valor: 0},
      {text: "Enseñar", valor: 0},
      {text: "Ministerio", valor: 1}
    ],
    [
      {text: "Misión", valor: 1},
      {text: "Familia", valor: 0},
      {text: "Vision", valor: 0},
      {text: "Adoracion", valor: 0}
    ],
    [
      {text: "Familia", valor: 0},
      {text: "Personas", valor: 1},
      {text: "Alguien", valor: 0},
      {text: "Vision", valor: 0}
    ],
    [
      {text: "Leyendo", valor: 0},
      {text: "Mirando", valor: 0},
      {text: "Ver", valor: 0},
      {text: "Apropiándome", valor: 1}
    ],
    [
      {text: "Familia", valor: 0},
      {text: "Seguidores", valor: 1},
      {text: "Pastores", valor: 0},
      {text: "Iguales", valor: 0}
    ],
    [
      {text: "Familia", valor: 0},
      {text: "Amigos", valor: 0},
      {text: "Perdidos", valor: 1},
      {text: "Demas", valor: 0}
    ],
    [
      {text: "Victoria", valor: 0},
      {text: "Gloria", valor: 0},
      {text: "Madurez", valor: 1},
      {text: "Sabiduria", valor: 0}
    ],
    [
      {text: "Tarea", valor: 0},
      {text: "Mision", valor: 0},
      {text: "Vision", valor: 0},
      {text: "Ambición", valor: 1}
    ]
  ];
  progreso: any;
  captura: any;
  public unregisterBackButtonAction: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public actionSheetCtrl: ActionSheetController,
              public alertCtrl: AlertController, public authServiceProvider: AuthServiceProvider, public viewCtrl: ViewController, public platform: Platform) {

    this.ocultar1 = false;
    this.ocultar2 = true;
    this.ocultar3 = false;
    this.captura = this.navParams.get('modulo');
    console.log(this.captura);

    //validar si esta marcada completada
    if (this.captura.modulo[0].datos[1].color == "#e94b67") {
      for (let index in this.respuestas) {
        for (let j = 0; j < 4; j++) {
          if (this.possibleButtons[index][j].valor == 1) {
            this.respuestas[index].text = this.possibleButtons[index][j].text;
            this.respuestas[index].valor = this.possibleButtons[index][j].valor;
          }
        }
      }
    } else {
      if (localStorage.getItem('progreso1')) {
        const data = JSON.parse(localStorage.getItem('progreso1'));
        console.log(data);
        this.respuestas = data;
      } else {
        localStorage.setItem('progreso1', JSON.stringify(this.respuestas));
        //console.log(this.progreso);
      }
    }

    for (let index in this.respuestas) {
      if (this.respuestas[index].text == " ") {
        this.respuestas[index].text = " ";
      }
    }
    console.log("tamaño " + this.possibleButtons.length);
  }

  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Cuestionario1Page');
    this.initializeBackButtonCustomHandler(this.captura, this.viewCtrl);

  }

  openActionSheet(pos) {
    console.log("opening");
    let actionsheet = this.actionSheetCtrl.create({
      title: "Selecciona una respuesta",
      buttons: this.createButtons(pos)
    });
    actionsheet.present();
  }

  createButtons(pos) {
    let buttons = [];
    for (let j = 0; j < 4; j++) {
      let button = {
        text: this.possibleButtons[pos][j].text,
        icon: "checkmark",
        handler: () => {
          this.respuestas[pos].text = this.possibleButtons[pos][j].text;
          this.respuestas[pos].valor = this.possibleButtons[pos][j].valor;
          localStorage.setItem('progreso1', JSON.stringify(this.respuestas));
          return true;
        }
      };
      buttons.push(button);
    }
    return buttons;
  }

  calificar() {
    let correctas = 0;
    let malas = 0;
    for (let index in this.respuestas) {
      if (this.respuestas[index].valor == 1) {
        correctas++;
      } else {
        malas++;
        this.respuestas[index].color = "#e94b67";
      }
    }
    if (correctas == this.respuestas.length) {
      if (this.captura.modulo[0].datos[1].color != "#e94b67") {
        localStorage.removeItem('progreso1');
      }
      this.showAlert("Felicidades has respondido todas las preguntas bien");
      this.captura.modulo[0].datos[1].color = "#e94b67";
      this.initializeBackButtonCustomHandler(this.captura, this.viewCtrl);


    } else {
      this.showAlert("ups, encontramos " + malas + " respuestas malas, revisa las respuestas");
    }

  }

  showAlert(texto) {
    const alert = this.alertCtrl.create({
      title: 'Resultados',
      subTitle: texto,
      buttons: ['OK']
    });
    alert.present();
  }

  dismiss() {
    this.viewCtrl.dismiss(this.captura);
  }

  initializeBackButtonCustomHandler(captura, ctr): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(function (event) {
      console.log('Prevent Back Button Page Change');
      ctr.dismiss(captura);
    }, 101); // Priority 101 will override back button handling (we set in app.component.ts) as it is bigger then priority 100 configured in app.component.ts file */
  }

  siguiente() {
    let currentIndex = this.slides.getActiveIndex();
    this.slides.slideTo(currentIndex + 1, 500);
    this.ocultar1 = true;
    if (this.slides.isEnd()) {
      this.ocultar2 = false;
      this.ocultar3 = true;
    } else {
      this.ocultar2 = true;
      this.ocultar3 = false;
    }

  }

  atras() {
    let currentIndex = this.slides.getActiveIndex();
    this.slides.slideTo(currentIndex - 1, 500);
    if (this.slides.isBeginning()) {
      this.ocultar1 = false;
    }
    if (this.slides.isEnd()) {
      this.ocultar2 = false;
      this.ocultar3 = true;
    } else {
      this.ocultar2 = true;
      this.ocultar3 = false;
    }
  }

  slideChanged() {
    if (this.slides.isBeginning()) {
      this.ocultar1 = false;
    }else{
      this.ocultar1 = true;
    }
    if (this.slides.isEnd()) {
      this.ocultar2 = false;
      this.ocultar3 = true;
    } else {
      this.ocultar2 = true;
      this.ocultar3 = false;
    }
  }

}
