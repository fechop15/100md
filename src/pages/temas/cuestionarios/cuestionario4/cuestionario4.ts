import {Component, ViewChild} from '@angular/core';
import {
  ActionSheetController,
  AlertController,
  IonicPage,
  NavController,
  NavParams, Platform, Slides,
  ViewController
} from 'ionic-angular';
import {AuthServiceProvider} from "../../../../providers/auth-service/auth-service";

/**
 * Generated class for the Cuestionario4Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cuestionario4',
  templateUrl: 'cuestionario4.html',
})
export class Cuestionario4Page {
  @ViewChild(Slides) slides: Slides;
  ocultar1: boolean;
  ocultar2: boolean;
  ocultar3: boolean;
  respuestas = [
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"}
  ];


  possibleButtons = [
    [
      {text: "Adoracion", valor: 1},
      {text: "Santidad", valor: 0},
      {text: "Oracion", valor: 0},
      {text: "Sanar", valor: 0}
    ],
    [
      {text: "Buena", valor: 0},
      {text: "Estable", valor: 0},
      {text: "Uno a Uno", valor: 0},
      {text: "Intima", valor: 1}
    ],
    [
      {text: "Sanidad", valor: 0},
      {text: "Sacrificio", valor: 0},
      {text: "Santidad", valor: 1},
      {text: "Alabanza", valor: 0}
    ],
    [
      {text: "Biblia", valor: 0},
      {text: "Relacion", valor: 0},
      {text: "Palabra", valor: 1},
      {text: "Mano", valor: 0}
    ],
    [
      {text: "Valor", valor: 0},
      {text: "Sacrificio", valor: 1},
      {text: "Objeto", valor: 0},
      {text: "Establo", valor: 0}
    ],
    [
      {text: "Grande", valor: 0},
      {text: "Abierto", valor: 1},
      {text: "Amoroso", valor: 0},
      {text: "Cariñoso", valor: 0}
    ],
    [
      {text: "Abierto", valor: 0},
      {text: "Amable", valor: 0},
      {text: "Arrepentido", valor: 1},
      {text: "Grande", valor: 0}
    ],

    [
      {text: "Confesar", valor: 1},
      {text: "Hablar", valor: 0},
      {text: "Palabra", valor: 0},
      {text: "Escuchar", valor: 0}
    ],
    [
      {text: "Corazon", valor: 0},
      {text: "Palabra", valor: 0},
      {text: "Mandato", valor: 1},
      {text: "Mano", valor: 0}
    ],
    [
      {text: "Algo", valor: 0},
      {text: "Veneno Amargo", valor: 1},
      {text: "Vida", valor: 0},
      {text: "Pecado", valor: 0}
    ],
    [
      {text: "Santa Presencia", valor: 1},
      {text: "Manto", valor: 0},
      {text: "Reino", valor: 0},
      {text: "Familia", valor: 0}
    ],
    [
      {text: "Hablar", valor: 0},
      {text: "Corazon", valor: 0},
      {text: "Confesion", valor: 1},
      {text: "escuchar", valor: 0}
    ],
    [
      {text: "Adoracion", valor: 0},
      {text: "Accion", valor: 0},
      {text: "Pecado", valor: 0},
      {text: "Alabanza", valor: 1}
    ],
    [
      {text: "Oracion", valor: 0},
      {text: "Accion de gracias", valor: 1},
      {text: "Adoracion", valor: 0},
      {text: "Alabanza", valor: 0}
    ],
    [
      {text: "Oracion", valor: 0},
      {text: "Alabanza", valor: 0},
      {text: "Corazon", valor: 0},
      {text: "Adoracion", valor: 1}
    ],
    [
      {text: "Alabanza", valor: 1},
      {text: "Familia", valor: 0},
      {text: "Adoracion", valor: 0},
      {text: "Corazon", valor: 0}
    ],
    [
      {text: "Apaga", valor: 1},
      {text: "Muestra", valor: 0},
      {text: "Resalta", valor: 0},
      {text: "Escucha", valor: 0}
    ],
    [
      {text: "Paz", valor: 0},
      {text: "Humildad", valor: 1},
      {text: "Descanso", valor: 0},
      {text: "Preocupaciones", valor: 0}
    ],
    [
      {text: "Amigo", valor: 0},
      {text: "Familiar", valor: 0},
      {text: "Enemigo", valor: 1},
      {text: "Cobarde", valor: 0}
    ],
    [
      {text: "Amabilidad", valor: 0},
      {text: "Familia", valor: 0},
      {text: "Amistad", valor: 0},
      {text: "Queja", valor: 1}
    ],
    [
      {text: "Energias", valor: 0},
      {text: "Mareas", valor: 0},
      {text: "Personas", valor: 0},
      {text: "Bendiciones", valor: 1}
    ],
    [
      {text: "Amor", valor: 0},
      {text: "Presencia", valor: 1},
      {text: "Justicia", valor: 0},
      {text: "Poder", valor: 0}
    ],
    [
      {text: "Liberados", valor: 0},
      {text: "Renovados", valor: 1},
      {text: "Amados", valor: 0},
      {text: "Maltratados", valor: 0}
    ],
    [
      {text: "Cosas", valor: 0},
      {text: "Bendiciones", valor: 0},
      {text: "Milagros", valor: 1},
      {text: "Justicias", valor: 0}
    ]

  ];
  progreso: any;

  captura: any;
  public unregisterBackButtonAction: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public actionSheetCtrl: ActionSheetController,
              public alertCtrl: AlertController, public authServiceProvider: AuthServiceProvider, public viewCtrl: ViewController, public platform: Platform) {

    this.ocultar1 = false;
    this.ocultar2 = true;
    this.ocultar3 = false;
    this.captura = this.navParams.get('modulo');
    console.log(this.captura);

    //validar si esta marcada completada
    if (this.captura.modulo[0].datos[1].color == "#e94b67") {
      for (let index in this.respuestas) {
        for (let j = 0; j < 4; j++) {
          if (this.possibleButtons[index][j].valor == 1) {
            this.respuestas[index].text = this.possibleButtons[index][j].text;
            this.respuestas[index].valor = this.possibleButtons[index][j].valor;
          }
        }
      }
    } else {
      if (localStorage.getItem("progreso4")) {
        const data = JSON.parse(localStorage.getItem("progreso4"));
        console.log(data);
        this.respuestas = data;
      } else {
        localStorage.setItem("progreso4", JSON.stringify(this.respuestas));
        //console.log(this.progreso);
      }
    }

    for (let index in this.respuestas) {
      if (this.respuestas[index].text == " ") {
        this.respuestas[index].text = " ";
      }
    }
    console.log("tamaño " + this.possibleButtons.length);
  }

  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Cuestionario1Page');
    this.initializeBackButtonCustomHandler(this.captura, this.viewCtrl);

  }

  openActionSheet(pos) {
    console.log("opening");
    let actionsheet = this.actionSheetCtrl.create({
      title: "Selecciona una respuesta",
      buttons: this.createButtons(pos)
    });
    actionsheet.present();
  }

  createButtons(pos) {
    let buttons = [];
    for (let j = 0; j < 4; j++) {
      let button = {
        text: this.possibleButtons[pos][j].text,
        icon: "checkmark",
        handler: () => {
          this.respuestas[pos].text = this.possibleButtons[pos][j].text;
          this.respuestas[pos].valor = this.possibleButtons[pos][j].valor;
          localStorage.setItem("progreso4", JSON.stringify(this.respuestas));
          return true;
        }
      };
      buttons.push(button);
    }
    return buttons;
  }

  calificar() {
    let correctas = 0;
    let malas = 0;
    for (let index in this.respuestas) {
      if (this.respuestas[index].valor == 1) {
        correctas++;
      } else {
        malas++;
        this.respuestas[index].color = "#e94b67";
      }
    }
    if (correctas == this.respuestas.length) {
      if (this.captura.modulo[0].datos[1].color != "#e94b67") {
        localStorage.removeItem("progreso4");
      }


      this.showAlert("Felicidades has respondido todas las preguntas bien");
      this.captura.modulo[0].datos[1].color = "#e94b67";
      this.initializeBackButtonCustomHandler(this.captura, this.viewCtrl);

    } else {
      this.showAlert("ups, encontramos " + malas + " respuestas malas, revisa las respuestas");
    }

  }

  showAlert(texto) {
    const alert = this.alertCtrl.create({
      title: 'Resultados',
      subTitle: texto,
      buttons: ['OK']
    });
    alert.present();
  }

  dismiss() {
    this.viewCtrl.dismiss(this.captura);
  }

  initializeBackButtonCustomHandler(captura, ctr): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(function (event) {
      console.log('Prevent Back Button Page Change');
      ctr.dismiss(captura);
    }, 101); // Priority 101 will override back button handling (we set in app.component.ts) as it is bigger then priority 100 configured in app.component.ts file */
  }

  siguiente() {
    let currentIndex = this.slides.getActiveIndex();
    this.slides.slideTo(currentIndex + 1, 500);
    this.ocultar1 = true;
    if (this.slides.isEnd()) {
      this.ocultar2 = false;
      this.ocultar3 = true;
    } else {
      this.ocultar2 = true;
      this.ocultar3 = false;
    }

  }

  atras() {
    let currentIndex = this.slides.getActiveIndex();
    this.slides.slideTo(currentIndex - 1, 500);
    if (this.slides.isBeginning()) {
      this.ocultar1 = false;
    }
    if (this.slides.isEnd()) {
      this.ocultar2 = false;
      this.ocultar3 = true;
    } else {
      this.ocultar2 = true;
      this.ocultar3 = false;
    }
  }

  slideChanged() {
    if (this.slides.isBeginning()) {
      this.ocultar1 = false;
    }else{
      this.ocultar1 = true;
    }
    if (this.slides.isEnd()) {
      this.ocultar2 = false;
      this.ocultar3 = true;
    } else {
      this.ocultar2 = true;
      this.ocultar3 = false;
    }
  }
}
