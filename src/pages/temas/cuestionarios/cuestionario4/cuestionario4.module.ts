import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario4Page } from './cuestionario4';

@NgModule({
  declarations: [
    Cuestionario4Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario4Page),
  ],
})
export class Cuestionario4PageModule {}
