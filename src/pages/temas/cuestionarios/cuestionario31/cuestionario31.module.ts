import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario31Page } from './cuestionario31';

@NgModule({
  declarations: [
    Cuestionario31Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario31Page),
  ],
})
export class Cuestionario31PageModule {}
