import {Component, ViewChild} from '@angular/core';
import {
  ActionSheetController,
  AlertController,
  IonicPage,
  NavController,
  NavParams,
  Platform, Slides,
  ViewController
} from 'ionic-angular'; import {AuthServiceProvider} from "../../../../providers/auth-service/auth-service";

/**
 * Generated class for the Cuestionario31Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cuestionario31',
  templateUrl: 'cuestionario31.html',
})
export class Cuestionario31Page {
  @ViewChild(Slides) slides: Slides;
  ocultar1: boolean;
  ocultar2: boolean;
  ocultar3: boolean;
  respuestas=[
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" }

  ];
  possibleButtons=[
    [
      { text: "mala",valor: 0 },
      { text: "Cuerpo",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Llenura ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Eterna",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Ekklesia ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Universal",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Local",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Llenura",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Cabeza",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Familia",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Relación",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Morada",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Mantener",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Democrática",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Ciudadanía",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Comprometidos ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Relación ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Juntos ",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Dispersa  ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Propósito",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Tierra",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Padre",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Nace",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Universal",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Miembro",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Familiar",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Iglesia ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Celestial",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Crees",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Miembro",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Pertenecer",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Conectas",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Miembro",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Cristo",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Organismo",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Específico ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Propósitos ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Crecemos",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Comunitario ",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Creyente",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Descubrir",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Ministeriales",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Cobertura",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Cuentas",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Adoración ",valor: 1 }
    ],
    [
      { text: "Comunidad ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Discipulado ",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Ministerio",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Evangelismo",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Compromiso",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Inmersión ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Membresía ",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Habitar ",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Miembros ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Rehusarse ",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Liderazgo",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Orar ",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Invitando ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Bienvenida",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Descubriendo ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Servir",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Siervo",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Asistir",valor: 1 }
    ],
    [
      { text: "Vivir",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Dar ",valor: 1 },
      { text: "mala",valor: 0 }
    ],

  ];
  captura: any;
  public unregisterBackButtonAction: any;
  constructor(public navCtrl: NavController,public navParams: NavParams,public actionSheetCtrl: ActionSheetController,
                                                                    public alertCtrl: AlertController,public authServiceProvider: AuthServiceProvider,public viewCtrl: ViewController,public platform: Platform) {

    this.ocultar1 = false;     this.ocultar2 = true;     this.ocultar3 = false;     this.captura=this.navParams.get('modulo');
    console.log(this.captura);
    for (let index in this.possibleButtons) {
      for (let index2 in this.possibleButtons[index]){
        if (this.possibleButtons[index][index2].text=="mala") {
          this.possibleButtons[index][index2].text=this.authServiceProvider.ramdomRespuestas();
        }
      }
    }
    console.log("ramdom");
    //validar si esta marcada completada
    if (this.captura.modulo[0].datos[1].color=="#e94b67"){
      for (let index in this.respuestas){
        for (let j = 0; j < 4; j++) {
          if (this.possibleButtons[index][j].valor==1) {
            this.respuestas[index].text = this.possibleButtons[index][j].text;
            this.respuestas[index].valor = this.possibleButtons[index][j].valor;
          }
        }
      }
    }else{
      if (localStorage.getItem("progreso31")) {
        const data = JSON.parse(localStorage.getItem("progreso31"));
        console.log(data);
        this.respuestas=data;
      }else{
        localStorage.setItem("progreso31", JSON.stringify(this.respuestas));
        //console.log(this.progreso);
      }
    }

    for (let index in this.respuestas) {
      if (this.respuestas[index].text==" ") {
        this.respuestas[index].text = " ";
      }
    }
    console.log("tamaño "+this.possibleButtons.length);
  }

  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Cuestionario1Page');
    this.initializeBackButtonCustomHandler(this.captura,this.viewCtrl);

  }

  openActionSheet(pos) {
    console.log("opening");
    let actionsheet = this.actionSheetCtrl.create({
      title: "Selecciona una respuesta",
      buttons: this.createButtons(pos)
    });
    actionsheet.present();
  }

  createButtons(pos) {
    let buttons = [];
    for (let j = 0; j < 4; j++) {
      let button = {
        text: this.possibleButtons[pos][j].text,
        icon: "checkmark",
        handler: () => {
          this.respuestas[pos].text = this.possibleButtons[pos][j].text;
          this.respuestas[pos].valor = this.possibleButtons[pos][j].valor;
          localStorage.setItem("progreso31", JSON.stringify(this.respuestas));
          return true;
        }
      };
      buttons.push(button);
    }
    return buttons;
  }

  calificar(){
    let correctas=0;
    let malas=0;
    for (let index in this.respuestas) {
      if (this.respuestas[index].valor==1){
        correctas++;
      }else{
        malas++;
        this.respuestas[index].color="#e94b67";
      }
    }
    if (correctas==this.respuestas.length){
       if(this.captura.modulo[0].datos[1].color!="#e94b67"){         localStorage.removeItem("progreso31");       }
      this.showAlert("Felicidades has respondido todas las preguntas bien");
      this.captura.modulo[0].datos[1].color="#e94b67";
      this.initializeBackButtonCustomHandler(this.captura,this.viewCtrl);


    } else{
      this.showAlert("ups, encontramos "+malas+" respuestas malas, revisa las respuestas" );
    }

  }

  showAlert(texto) {
    const alert = this.alertCtrl.create({
      title: 'Resultados',
      subTitle: texto,
      buttons: ['OK']
    });
    alert.present();
  }

  dismiss() {
    this.viewCtrl.dismiss(this.captura);
  }

  initializeBackButtonCustomHandler(captura,ctr): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(function(event){
      console.log('Prevent Back Button Page Change');
      ctr.dismiss(captura);
    }, 101); // Priority 101 will override back button handling (we set in app.component.ts) as it is bigger then priority 100 configured in app.component.ts file */
  }
  siguiente() {
    let currentIndex = this.slides.getActiveIndex();
    this.slides.slideTo(currentIndex+1, 500);
    this.ocultar1=true;
    if(this.slides.isEnd()){
      this.ocultar2=false;
      this.ocultar3=true;
    }else {
      this.ocultar2=true;
      this.ocultar3=false;
    }

  }

  atras() {
    let currentIndex = this.slides.getActiveIndex();
    this.slides.slideTo(currentIndex-1, 500);
    if(this.slides.isBeginning()){
      this.ocultar1=false;
    }
    if(this.slides.isEnd()){
      this.ocultar2=false;
      this.ocultar3=true;
    }else {
      this.ocultar2=true;
      this.ocultar3=false;
    }
  }
  slideChanged() {
    if (this.slides.isBeginning()) {
      this.ocultar1 = false;
    }else{
      this.ocultar1 = true;
    }
    if (this.slides.isEnd()) {
      this.ocultar2 = false;
      this.ocultar3 = true;
    } else {
      this.ocultar2 = true;
      this.ocultar3 = false;
    }
  }
}
