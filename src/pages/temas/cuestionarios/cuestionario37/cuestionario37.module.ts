import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario37Page } from './cuestionario37';

@NgModule({
  declarations: [
    Cuestionario37Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario37Page),
  ],
})
export class Cuestionario37PageModule {}
