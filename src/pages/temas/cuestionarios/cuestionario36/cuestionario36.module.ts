import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario36Page } from './cuestionario36';

@NgModule({
  declarations: [
    Cuestionario36Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario36Page),
  ],
})
export class Cuestionario36PageModule {}
