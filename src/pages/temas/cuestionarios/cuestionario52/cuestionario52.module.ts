import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario52Page } from './cuestionario52';

@NgModule({
  declarations: [
    Cuestionario52Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario52Page),
  ],
})
export class Cuestionario52PageModule {}
