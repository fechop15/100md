import {Component, ViewChild} from '@angular/core';
import {
  ActionSheetController,
  AlertController,
  IonicPage,
  NavController,
  NavParams,
  Platform, Slides,
  ViewController
} from 'ionic-angular';
import {AuthServiceProvider} from "../../../../providers/auth-service/auth-service";

/**
 * Generated class for the Cuestionario11Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cuestionario11',
  templateUrl: 'cuestionario11.html',
})
export class Cuestionario11Page {
  @ViewChild(Slides) slides: Slides;
  //92
  ocultar1: boolean;
  ocultar2: boolean;
  ocultar3: boolean;
  respuestas = [
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},

    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
  ];

  possibleButtons = [
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "Principal", valor: 1},
      {text: "mala", valor: 0}
    ],
    [
      {text: "Líderazgo", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "Desarrollo", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "Preparando", valor: 1},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "Obediencia", valor: 1}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "Anarquía", valor: 1},
      {text: "mala", valor: 0}
    ],

    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "Confusión", valor: 1},
      {text: "mala", valor: 0}
    ],
    [
      {text: "Pastor", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "Deseoso", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "Guiar", valor: 1}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "Coraje", valor: 1},
      {text: "mala", valor: 0}
    ],

    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "fe", valor: 1},
      {text: "mala", valor: 0}
    ],
    [
      {text: "nunca ", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "responsable", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "identificado", valor: 1}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "dirección", valor: 1},
      {text: "mala", valor: 0}
    ],

    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "actividades", valor: 1},
      {text: "mala", valor: 0}
    ],
    [
      {text: "inspirar", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "influencia", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "acciones", valor: 1}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "correcta", valor: 1},
      {text: "mala", valor: 0}
    ],

    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "ir", valor: 1},
      {text: "mala", valor: 0}
    ],
    [
      {text: "brusco", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "características", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "sigue", valor: 1}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "señal", valor: 1},
      {text: "mala", valor: 0}
    ],

    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "enseñar ", valor: 1},
      {text: "mala", valor: 0}
    ],
    [
      {text: "prueba", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "efectivo", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "motivar", valor: 1}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "correcta", valor: 1},
      {text: "mala", valor: 0}
    ],

    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "alcanzado", valor: 1},
      {text: "mala", valor: 0}
    ],
    [
      {text: "meta", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "leer", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "futuro", valor: 1}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "querrán", valor: 1},
      {text: "mala", valor: 0}
    ],

    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "productos", valor: 1},
      {text: "mala", valor: 0}
    ],
    [
      {text: "fallado", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "visionaria", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "desafíos", valor: 1}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "práctica", valor: 1},
      {text: "mala", valor: 0}
    ],

    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "formular", valor: 1},
      {text: "mala", valor: 0}
    ],
    [
      {text: "realista", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "reconoce", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "cosechar", valor: 1}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "crecer", valor: 1},
      {text: "mala", valor: 0}
    ],

    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "constructor", valor: 1},
      {text: "mala", valor: 0}
    ],
    [
      {text: "levantar", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "diseñarlo", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "presupuesto", valor: 1}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "Evalúa", valor: 1},
      {text: "mala", valor: 0}
    ],

    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "planeado", valor: 1},
      {text: "mala", valor: 0}
    ],
    [
      {text: "construir", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "plan", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "estrategia", valor: 1}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "involucrarse", valor: 1},
      {text: "mala", valor: 0}
    ],

    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "investigación", valor: 1},
      {text: "mala", valor: 0}
    ],
    [
      {text: "estrategias", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "comunicados", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "claro", valor: 1}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "visión", valor: 1},
      {text: "mala", valor: 0}
    ],

    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "socios", valor: 1},
      {text: "mala", valor: 0}
    ],
    [
      {text: "habilidades", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "comunicarse", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "estimular", valor: 1}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "cumplimiento", valor: 1},
      {text: "mala", valor: 0}
    ],

    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "informarlos", valor: 1},
      {text: "mala", valor: 0}
    ],
    [
      {text: "consistentemente", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "trabajo", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "impulso", valor: 1}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "modelo", valor: 1},
      {text: "mala", valor: 0}
    ],

    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "persuadirles", valor: 1},
      {text: "mala", valor: 0}
    ],
    [
      {text: "comprometido", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "motivadas", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "seguidores", valor: 1}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "provee", valor: 1},
      {text: "mala", valor: 0}
    ],

    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "credibilidad", valor: 1},
      {text: "mala", valor: 0}
    ],
    [
      {text: "tiempo", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "autoridad", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "sometidos", valor: 1}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "obedecer", valor: 1},
      {text: "mala", valor: 0}
    ],

    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "preguntar", valor: 1},
      {text: "mala", valor: 0}
    ],
    [
      {text: "batalla", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "estructura", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "rol", valor: 1}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "sujeto", valor: 1},
      {text: "mala", valor: 0}
    ],

    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "fe", valor: 1},
      {text: "mala", valor: 0}
    ],
    [
      {text: "formular", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "maximizar", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "administrar", valor: 1}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "competente", valor: 1},
      {text: "mala", valor: 0}
    ],

    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "excelente", valor: 1},
      {text: "mala", valor: 0}
    ],
    [
      {text: "conductor", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "músico", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "armonía", valor: 1}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "ejecutada", valor: 1},
      {text: "mala", valor: 0}
    ],

    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "precisa", valor: 1},
      {text: "mala", valor: 0}
    ],
    [
      {text: "autoridad", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "ejercer", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "autoridad", valor: 1}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "darse cuenta", valor: 1},
      {text: "mala", valor: 0}
    ],

    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "importante", valor: 1},
      {text: "mala", valor: 0}
    ],
    [
      {text: "depende", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "moral", valor: 1},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "unidad", valor: 1}
    ],
    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "completo", valor: 1},
      {text: "mala", valor: 0}
    ],

    [
      {text: "mala", valor: 0},
      {text: "mala", valor: 0},
      {text: "lograr", valor: 1},
      {text: "mala", valor: 0}
    ],
  ];

  captura: any;
  public unregisterBackButtonAction: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public actionSheetCtrl: ActionSheetController,
              public alertCtrl: AlertController, public authServiceProvider: AuthServiceProvider, public viewCtrl: ViewController, public platform: Platform) {
    this.ocultar1 = false;
    this.ocultar2 = true;
    this.ocultar3 = false;
    this.captura = this.navParams.get('modulo');
    console.log(this.captura);
    for (let index in this.possibleButtons) {
      for (let index2 in this.possibleButtons[index]) {
        if (this.possibleButtons[index][index2].text == "mala") {
          this.possibleButtons[index][index2].text = this.authServiceProvider.ramdomRespuestas();
        }
      }
    }
    console.log("ramdom");
    //validar si esta marcada completada
    if (this.captura.modulo[0].datos[1].color == "#e94b67") {
      for (let index in this.respuestas) {
        for (let j = 0; j < 4; j++) {
          if (this.possibleButtons[index][j].valor == 1) {
            this.respuestas[index].text = this.possibleButtons[index][j].text;
            this.respuestas[index].valor = this.possibleButtons[index][j].valor;
          }
        }
      }
    } else {
      if (localStorage.getItem("progreso11")) {
        const data = JSON.parse(localStorage.getItem("progreso11"));
        console.log(data);
        this.respuestas = data;
      } else {
        localStorage.setItem("progreso11", JSON.stringify(this.respuestas));
        //console.log(this.progreso);
      }
    }

    for (let index in this.respuestas) {
      if (this.respuestas[index].text == " ") {
        this.respuestas[index].text = " ";
      }
    }
    console.log("tamaño " + this.possibleButtons.length);
  }

  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Cuestionario1Page');
    this.initializeBackButtonCustomHandler(this.captura, this.viewCtrl);

  }

  openActionSheet(pos) {
    console.log("opening");
    let actionsheet = this.actionSheetCtrl.create({
      title: "Selecciona una respuesta",
      buttons: this.createButtons(pos)
    });
    actionsheet.present();
  }

  createButtons(pos) {
    let buttons = [];
    for (let j = 0; j < 4; j++) {
      let button = {
        text: this.possibleButtons[pos][j].text,
        icon: "checkmark",
        handler: () => {
          this.respuestas[pos].text = this.possibleButtons[pos][j].text;
          this.respuestas[pos].valor = this.possibleButtons[pos][j].valor;
          localStorage.setItem("progreso11", JSON.stringify(this.respuestas));
          return true;
        }
      };
      buttons.push(button);
    }
    return buttons;
  }

  calificar() {
    let correctas = 0;
    let malas = 0;
    for (let index in this.respuestas) {
      if (this.respuestas[index].valor == 1) {
        correctas++;
      } else {
        malas++;
        this.respuestas[index].color = "#e94b67";
      }
    }
    if (correctas == this.respuestas.length) {
      if (this.captura.modulo[0].datos[1].color != "#e94b67") {
        localStorage.removeItem("progreso11");
      }
      this.showAlert("Felicidades has respondido todas las preguntas bien");
      this.captura.modulo[0].datos[1].color = "#e94b67";
      this.initializeBackButtonCustomHandler(this.captura, this.viewCtrl);
    } else {
      this.showAlert("ups, encontramos " + malas + " respuestas malas, revisa las respuestas");
    }

  }

  showAlert(texto) {
    const alert = this.alertCtrl.create({
      title: 'Resultados',
      subTitle: texto,
      buttons: ['OK']
    });
    alert.present();
  }

  dismiss() {
    this.viewCtrl.dismiss(this.captura);
  }

  initializeBackButtonCustomHandler(captura, ctr): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(function (event) {
      console.log('Prevent Back Button Page Change');
      ctr.dismiss(captura);
    }, 101); // Priority 101 will override back button handling (we set in app.component.ts) as it is bigger then priority 100 configured in app.component.ts file */
  }

  siguiente() {
    let currentIndex = this.slides.getActiveIndex();
    this.slides.slideTo(currentIndex + 1, 500);
    this.ocultar1 = true;
    if (this.slides.isEnd()) {
      this.ocultar2 = false;
      this.ocultar3 = true;
    } else {
      this.ocultar2 = true;
      this.ocultar3 = false;
    }

  }

  atras() {
    let currentIndex = this.slides.getActiveIndex();
    this.slides.slideTo(currentIndex - 1, 500);
    if (this.slides.isBeginning()) {
      this.ocultar1 = false;
    }
    if (this.slides.isEnd()) {
      this.ocultar2 = false;
      this.ocultar3 = true;
    } else {
      this.ocultar2 = true;
      this.ocultar3 = false;
    }
  }

  slideChanged() {
    if (this.slides.isBeginning()) {
      this.ocultar1 = false;
    } else {
      this.ocultar1 = true;
    }
    if (this.slides.isEnd()) {
      this.ocultar2 = false;
      this.ocultar3 = true;
    } else {
      this.ocultar2 = true;
      this.ocultar3 = false;
    }
  }
}
