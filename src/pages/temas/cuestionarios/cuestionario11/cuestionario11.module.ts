import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario11Page } from './cuestionario11';

@NgModule({
  declarations: [
    Cuestionario11Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario11Page),
  ],
})
export class Cuestionario11PageModule {}
