import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario3Page } from './cuestionario3';

@NgModule({
  declarations: [
    Cuestionario3Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario3Page),
  ],
})
export class Cuestionario3PageModule {}
