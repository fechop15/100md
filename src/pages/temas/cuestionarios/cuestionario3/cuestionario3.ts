import {Component, ViewChild} from '@angular/core';
import {
  ActionSheetController,
  AlertController,
  IonicPage,
  NavController,
  NavParams, Platform, Slides,
  ViewController
} from 'ionic-angular';
import {AuthServiceProvider} from "../../../../providers/auth-service/auth-service";

/**
 * Generated class for the Cuestionario3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cuestionario3',
  templateUrl: 'cuestionario3.html',
})
export class Cuestionario3Page {
  @ViewChild(Slides) slides: Slides;
  ocultar1: boolean;
  ocultar2: boolean;
  ocultar3: boolean;
  respuestas = [
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"},
    {text: " ", valor: 0, color: "#19a7bc"}
  ];

  possibleButtons = [
    [
      {text: "Aleja", valor: 0},
      {text: "Trae", valor: 1},
      {text: "Invita", valor: 0},
      {text: "Lleva", valor: 0}
    ],
    [
      {text: "Vida", valor: 0},
      {text: "Mision", valor: 0},
      {text: "Estrategia", valor: 0},
      {text: "Perspectiva", valor: 1}
    ],
    [
      {text: "Corazon", valor: 0},
      {text: "Mente", valor: 1},
      {text: "Vida", valor: 0},
      {text: "Esfuerzo", valor: 0}
    ],
    [
      {text: "Corazon", valor: 0},
      {text: "Vida", valor: 0},
      {text: "Mente", valor: 0},
      {text: "Fe", valor: 1}
    ],
    [
      {text: "Ventajas", valor: 0},
      {text: "Desventajas", valor: 0},
      {text: "Armas", valor: 1},
      {text: "Esfuerzos", valor: 0}
    ],
    [
      {text: "Simples", valor: 0},
      {text: "Permanentes", valor: 1},
      {text: "Fuertes", valor: 0},
      {text: "Buenas", valor: 0}
    ],
    [
      {text: "Agradable", valor: 0},
      {text: "Amigable", valor: 0},
      {text: "Bueno", valor: 1},
      {text: "Sociable", valor: 0}
    ],
    [
      {text: "Buena", valor: 0},
      {text: "Eterna", valor: 1},
      {text: "Justa", valor: 0},
      {text: "Mala", valor: 0}
    ],
    [
      {text: "Va", valor: 0},
      {text: "Viene", valor: 0},
      {text: "Perdura", valor: 1},
      {text: "Esta", valor: 0}
    ],
    [
      {text: "Palabra", valor: 0},
      {text: "Adoracion", valor: 0},
      {text: "Accion de gracias", valor: 1},
      {text: "Vida", valor: 0}
    ],
    [
      {text: "Es", valor: 0},
      {text: "Que es", valor: 0},
      {text: "Viviente", valor: 0},
      {text: "Sobreabundante", valor: 1}
    ],
    [
      {text: "Adorar", valor: 0},
      {text: "Alabar", valor: 1},
      {text: "Hablar", valor: 0},
      {text: "Llamar", valor: 0}
    ],
    [
      {text: "Clave", valor: 1},
      {text: "Buena", valor: 0},
      {text: "Lo Mejor", valor: 0},
      {text: "Mala", valor: 0}
    ],
    [
      {text: "Aleja", valor: 0},
      {text: "Atrae", valor: 0},
      {text: "Ve", valor: 0},
      {text: "Silencia", valor: 1}
    ],
    [
      {text: "Seguras", valor: 0},
      {text: "Sobrenatural", valor: 1},
      {text: "Siempre", valor: 0},
      {text: "Esta", valor: 0}
    ],
    [
      {text: "Dulces", valor: 1},
      {text: "Amargas", valor: 0},
      {text: "Seguras", valor: 0},
      {text: "Tardia", valor: 0}
    ],
    [
      {text: "Ata", valor: 0},
      {text: "Relaja", valor: 0},
      {text: "Mantiene", valor: 0},
      {text: "Libera", valor: 1}
    ],
    [
      {text: "Hace", valor: 0},
      {text: "Libera", valor: 0},
      {text: "Prepara", valor: 1},
      {text: "Ve", valor: 0}
    ],
    [
      {text: "Da", valor: 0},
      {text: "Impacta", valor: 1},
      {text: "Muestra", valor: 0},
      {text: "Prepara", valor: 0}
    ],
    [
      {text: "Mision", valor: 0},
      {text: "Continuamente", valor: 1},
      {text: "Adoracion", valor: 0},
      {text: "Oracion", valor: 0}
    ]
  ];
  progreso: any;
  captura: any;
  public unregisterBackButtonAction: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public actionSheetCtrl: ActionSheetController,
              public alertCtrl: AlertController, public authServiceProvider: AuthServiceProvider, public viewCtrl: ViewController, public platform: Platform) {

    this.ocultar1 = false;
    this.ocultar2 = true;
    this.ocultar3 = false;

    this.captura = this.navParams.get('modulo');
    console.log(this.captura);

    //validar si esta marcada completada
    if (this.captura.modulo[0].datos[1].color == "#e94b67") {
      for (let index in this.respuestas) {
        for (let j = 0; j < 4; j++) {
          if (this.possibleButtons[index][j].valor == 1) {
            this.respuestas[index].text = this.possibleButtons[index][j].text;
            this.respuestas[index].valor = this.possibleButtons[index][j].valor;
          }
        }
      }
    } else {
      if (localStorage.getItem("progreso3")) {
        const data = JSON.parse(localStorage.getItem("progreso3"));
        console.log(data);
        this.respuestas = data;
      } else {
        localStorage.setItem("progreso3", JSON.stringify(this.respuestas));
        //console.log(this.progreso);
      }
    }

    for (let index in this.respuestas) {
      if (this.respuestas[index].text == " ") {
        this.respuestas[index].text = " ";
      }
    }
    console.log("tamaño " + this.possibleButtons.length);
  }

  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Cuestionario3Page');
    this.initializeBackButtonCustomHandler(this.captura, this.viewCtrl);

  }

  openActionSheet(pos) {
    console.log("opening");
    let actionsheet = this.actionSheetCtrl.create({
      title: "Selecciona una respuesta",
      buttons: this.createButtons(pos)
    });
    actionsheet.present();
  }

  createButtons(pos) {
    let buttons = [];
    for (let j = 0; j < 4; j++) {
      let button = {
        text: this.possibleButtons[pos][j].text,
        icon: "checkmark",
        handler: () => {
          this.respuestas[pos].text = this.possibleButtons[pos][j].text;
          this.respuestas[pos].valor = this.possibleButtons[pos][j].valor;
          localStorage.setItem("progreso3", JSON.stringify(this.respuestas));
          return true;
        }
      };
      buttons.push(button);
    }
    return buttons;
  }

  calificar() {
    let correctas = 0;
    let malas = 0;
    for (let index in this.respuestas) {
      if (this.respuestas[index].valor == 1) {
        correctas++;
      } else {
        malas++;
        this.respuestas[index].color = "#e94b67";
      }
    }
    if (correctas == this.respuestas.length) {
      if (this.captura.modulo[0].datos[1].color != "#e94b67") {
        localStorage.removeItem("progreso3");
      }
      this.showAlert("Felicidades has respondido todas las preguntas bien");
      this.captura.modulo[0].datos[1].color = "#e94b67";
      this.initializeBackButtonCustomHandler(this.captura, this.viewCtrl);


    } else {
      this.showAlert("ups, encontramos " + malas + " respuestas malas, revisa las respuestas");
    }

  }

  showAlert(texto) {
    const alert = this.alertCtrl.create({
      title: 'Resultados',
      subTitle: texto,
      buttons: ['OK']
    });
    alert.present();
  }

  dismiss() {
    this.viewCtrl.dismiss(this.captura);
  }

  initializeBackButtonCustomHandler(captura, ctr): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(function (event) {
      console.log('Prevent Back Button Page Change');
      ctr.dismiss(captura);
    }, 101); // Priority 101 will override back button handling (we set in app.component.ts) as it is bigger then priority 100 configured in app.component.ts file */
  }

  siguiente() {
    let currentIndex = this.slides.getActiveIndex();
    this.slides.slideTo(currentIndex + 1, 500);
    this.ocultar1 = true;
    if (this.slides.isEnd()) {
      this.ocultar2 = false;
      this.ocultar3 = true;
    } else {
      this.ocultar2 = true;
      this.ocultar3 = false;
    }

  }

  atras() {
    let currentIndex = this.slides.getActiveIndex();
    this.slides.slideTo(currentIndex - 1, 500);
    if (this.slides.isBeginning()) {
      this.ocultar1 = false;
    }
    if (this.slides.isEnd()) {
      this.ocultar2 = false;
      this.ocultar3 = true;
    } else {
      this.ocultar2 = true;
      this.ocultar3 = false;
    }
  }
  slideChanged() {
    if (this.slides.isBeginning()) {
      this.ocultar1 = false;
    }else{
      this.ocultar1 = true;
    }
    if (this.slides.isEnd()) {
      this.ocultar2 = false;
      this.ocultar3 = true;
    } else {
      this.ocultar2 = true;
      this.ocultar3 = false;
    }
  }
}
