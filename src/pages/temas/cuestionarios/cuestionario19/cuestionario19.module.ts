import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario19Page } from './cuestionario19';

@NgModule({
  declarations: [
    Cuestionario19Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario19Page),
  ],
})
export class Cuestionario19PageModule {}
