import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario22Page } from './cuestionario22';

@NgModule({
  declarations: [
    Cuestionario22Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario22Page),
  ],
})
export class Cuestionario22PageModule {}
