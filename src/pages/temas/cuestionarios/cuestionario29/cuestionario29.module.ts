import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario29Page } from './cuestionario29';

@NgModule({
  declarations: [
    Cuestionario29Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario29Page),
  ],
})
export class Cuestionario29PageModule {}
