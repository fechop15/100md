import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario43Page } from './cuestionario43';

@NgModule({
  declarations: [
    Cuestionario43Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario43Page),
  ],
})
export class Cuestionario43PageModule {}
