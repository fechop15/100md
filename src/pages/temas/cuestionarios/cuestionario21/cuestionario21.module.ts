import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario21Page } from './cuestionario21';

@NgModule({
  declarations: [
    Cuestionario21Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario21Page),
  ],
})
export class Cuestionario21PageModule {}
