import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario34Page } from './cuestionario34';

@NgModule({
  declarations: [
    Cuestionario34Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario34Page),
  ],
})
export class Cuestionario34PageModule {}
