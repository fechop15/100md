import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario50Page } from './cuestionario50';

@NgModule({
  declarations: [
    Cuestionario50Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario50Page),
  ],
})
export class Cuestionario50PageModule {}
