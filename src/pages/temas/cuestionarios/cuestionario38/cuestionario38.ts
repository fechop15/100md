import {Component, ViewChild} from '@angular/core';
import {
  ActionSheetController,
  AlertController,
  IonicPage,
  NavController,
  NavParams,
  Platform, Slides,
  ViewController
} from 'ionic-angular'; import {AuthServiceProvider} from "../../../../providers/auth-service/auth-service";

/**
 * Generated class for the Cuestionario38Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cuestionario38',
  templateUrl: 'cuestionario38.html',
})
export class Cuestionario38Page {
  @ViewChild(Slides) slides: Slides;
  ocultar1: boolean;
  ocultar2: boolean;
  ocultar3: boolean;
  respuestas=[
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },
    { text: " ",valor: 0,color: "#19a7bc" },

  ];
  possibleButtons=[
    [
      { text: "mala",valor: 0 },
      { text: "Espíritu ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Cuerpo",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Alma",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Trino",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Uno",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Esencia ",valor: 1 }
    ],
    [
      { text: "Personas",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Un",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Personalidades",valor: 1 }
    ],
    [
      { text: "Juntas",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Perfecta",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Trinidad",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Seres ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Lámpara",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Busca ",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Profundo",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Interior",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Medio ",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Espíritu",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "No",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Espiritual",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Eterna",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Mismo",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Poder",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Pueden",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Facultad",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Espíritu",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Puede",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Mental/intelectual",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Espíritual",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Espíritu",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Persona",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Fuerza",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Mejor",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Escucha",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Oyen ",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Deliberadamente",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Paciencia",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Compiten",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Guíe ",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Énfasis ",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Alimentarás",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Crece",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Comer",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Principal",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Llena",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Completamente",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Renovada ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "Acondicionada",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Deseo",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Discierne",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Prueba",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Uno",valor: 1 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Fuera ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Consistente",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Voluntad ",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Someterte",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Dirección ",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Altar ",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Muerto",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Deseos",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Responsabilidad ",valor: 1 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Levanta ",valor: 1 }
    ],
    [
      { text: "Alinearte",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "Tres",valor: 1 },
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 }
    ],
    [
      { text: "mala",valor: 0 },
      { text: "mala",valor: 0 },
      { text: "Inseparables ",valor: 1 },
      { text: "mala",valor: 0 }
    ],

  ];
  captura: any;
  public unregisterBackButtonAction: any;
  constructor(public navCtrl: NavController,public navParams: NavParams,public actionSheetCtrl: ActionSheetController,
              public alertCtrl: AlertController,public authServiceProvider: AuthServiceProvider,public viewCtrl: ViewController,public platform: Platform) {

    this.ocultar1 = false;     this.ocultar2 = true;     this.ocultar3 = false;     this.captura=this.navParams.get('modulo');
    console.log(this.captura);
    for (let index in this.possibleButtons) {
      for (let index2 in this.possibleButtons[index]){
        if (this.possibleButtons[index][index2].text=="mala") {
          this.possibleButtons[index][index2].text=this.authServiceProvider.ramdomRespuestas();
        }
      }
    }
    console.log("ramdom");
    //validar si esta marcada completada
    if (this.captura.modulo[0].datos[1].color=="#e94b67"){
      for (let index in this.respuestas){
        for (let j = 0; j < 4; j++) {
          if (this.possibleButtons[index][j].valor==1) {
            this.respuestas[index].text = this.possibleButtons[index][j].text;
            this.respuestas[index].valor = this.possibleButtons[index][j].valor;
          }
        }
      }
    }else{
      if (localStorage.getItem("progreso38")) {
        const data = JSON.parse(localStorage.getItem("progreso38"));
        console.log(data);
        this.respuestas=data;
      }else{
        localStorage.setItem("progreso38", JSON.stringify(this.respuestas));
        //console.log(this.progreso);
      }
    }

    for (let index in this.respuestas) {
      if (this.respuestas[index].text==" ") {
        this.respuestas[index].text = " ";
      }
    }
    console.log("tamaño "+this.possibleButtons.length);
  }

  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Cuestionario1Page');
    this.initializeBackButtonCustomHandler(this.captura,this.viewCtrl);

  }

  openActionSheet(pos) {
    console.log("opening");
    let actionsheet = this.actionSheetCtrl.create({
      title: "Selecciona una respuesta",
      buttons: this.createButtons(pos)
    });
    actionsheet.present();
  }

  createButtons(pos) {
    let buttons = [];
    for (let j = 0; j < 4; j++) {
      let button = {
        text: this.possibleButtons[pos][j].text,
        icon: "checkmark",
        handler: () => {
          this.respuestas[pos].text = this.possibleButtons[pos][j].text;
          this.respuestas[pos].valor = this.possibleButtons[pos][j].valor;
          localStorage.setItem("progreso38", JSON.stringify(this.respuestas));
          return true;
        }
      };
      buttons.push(button);
    }
    return buttons;
  }

  calificar(){
    let correctas=0;
    let malas=0;
    for (let index in this.respuestas) {
      if (this.respuestas[index].valor==1){
        correctas++;
      }else{
        malas++;
        this.respuestas[index].color="#e94b67";
      }
    }
    if (correctas==this.respuestas.length){
       if(this.captura.modulo[0].datos[1].color!="#e94b67"){         localStorage.removeItem("progreso38");       }
      this.showAlert("Felicidades has respondido todas las preguntas bien");
      this.captura.modulo[0].datos[1].color="#e94b67";
      this.initializeBackButtonCustomHandler(this.captura,this.viewCtrl);


    } else{
      this.showAlert("ups, encontramos "+malas+" respuestas malas, revisa las respuestas" );
    }

  }

  showAlert(texto) {
    const alert = this.alertCtrl.create({
      title: 'Resultados',
      subTitle: texto,
      buttons: ['OK']
    });
    alert.present();
  }

  dismiss() {
    this.viewCtrl.dismiss(this.captura);
  }

  initializeBackButtonCustomHandler(captura,ctr): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(function(event){
      console.log('Prevent Back Button Page Change');
      ctr.dismiss(captura);
    }, 101); // Priority 101 will override back button handling (we set in app.component.ts) as it is bigger then priority 100 configured in app.component.ts file */
  }
  siguiente() {
    let currentIndex = this.slides.getActiveIndex();
    this.slides.slideTo(currentIndex+1, 500);
    this.ocultar1=true;
    if(this.slides.isEnd()){
      this.ocultar2=false;
      this.ocultar3=true;
    }else {
      this.ocultar2=true;
      this.ocultar3=false;
    }

  }

  atras() {
    let currentIndex = this.slides.getActiveIndex();
    this.slides.slideTo(currentIndex-1, 500);
    if(this.slides.isBeginning()){
      this.ocultar1=false;
    }
    if(this.slides.isEnd()){
      this.ocultar2=false;
      this.ocultar3=true;
    }else {
      this.ocultar2=true;
      this.ocultar3=false;
    }
  }
  slideChanged() {
    if (this.slides.isBeginning()) {
      this.ocultar1 = false;
    }else{
      this.ocultar1 = true;
    }
    if (this.slides.isEnd()) {
      this.ocultar2 = false;
      this.ocultar3 = true;
    } else {
      this.ocultar2 = true;
      this.ocultar3 = false;
    }
  }
}

