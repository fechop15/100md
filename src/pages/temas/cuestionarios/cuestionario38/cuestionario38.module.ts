import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario38Page } from './cuestionario38';

@NgModule({
  declarations: [
    Cuestionario38Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario38Page),
  ],
})
export class Cuestionario38PageModule {}
