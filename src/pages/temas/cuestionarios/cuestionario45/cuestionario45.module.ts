import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario45Page } from './cuestionario45';

@NgModule({
  declarations: [
    Cuestionario45Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario45Page),
  ],
})
export class Cuestionario45PageModule {}
