import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Cuestionario32Page } from './cuestionario32';

@NgModule({
  declarations: [
    Cuestionario32Page,
  ],
  imports: [
    IonicPageModule.forChild(Cuestionario32Page),
  ],
})
export class Cuestionario32PageModule {}
