import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, PopoverController, ViewController} from 'ionic-angular';
import {PopoverComponent} from "../../components/popover/popover";
import {WelcomePage} from "../welcome/welcome";

/**
 * Generated class for the ArticuloPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-articulo',
  templateUrl: 'articulo.html',
})
export class ArticuloPage {
  contenido:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public popoverCtrl: PopoverController,public viewCtrl: ViewController) {
    this.contenido=this.navParams.get("contenido");
    console.log(this.contenido);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ArticuloPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  backToWelcome() {
    this.navCtrl.push(WelcomePage).then(() => {
      let index = this.navCtrl.length()-2;
      this.navCtrl.remove(index);
    });
  }
  logout() {
    localStorage.clear();
    setTimeout(() => this.backToWelcome(), 1000);
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverComponent);
    popover.present({
      ev: myEvent
    });
    popover.onDidDismiss(data => {
      if (data) {
        console.log(data);
        if (data.id==4){
          this.logout();
        }
      }
    });
  }
}
