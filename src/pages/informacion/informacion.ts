import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams, PopoverController, ViewController} from 'ionic-angular';
import {WelcomePage} from "../welcome/welcome";
import {PopoverComponent} from "../../components/popover/popover";
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";

/**
 * Generated class for the InformacionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-informacion',
  templateUrl: 'informacion.html',
})
export class InformacionPage {
  contenido:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public popoverCtrl: PopoverController,public viewCtrl: ViewController,public authServiceProvider: AuthServiceProvider,public alertCtrl: AlertController) {
    this.contenido=this.navParams.get("contenido");
    console.log(this.contenido);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InformacionPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  backToWelcome() {
    this.navCtrl.push(WelcomePage).then(() => {
      let index = this.navCtrl.length()-2;
      this.navCtrl.remove(index);
    });
  }
  logout() {
    localStorage.clear();
    setTimeout(() => this.backToWelcome(), 1000);
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverComponent);
    popover.present({
      ev: myEvent
    });
    popover.onDidDismiss(data => {
      if (data) {
        console.log(data);
        if (data.id==4){
          this.logout();
        }
      }
    });
  }

  cambiarCelular(){
    console.log("cambio");
    let datos={"idusers": this.contenido.usuario.idusers, "token": this.contenido.usuario.token, "celular":""};
    this.showPrompt("Actulizacion","Ingrese su nuevo numero de celular",datos,"Celular");
  }

  cambiarPassword(){
    console.log("cambio");
    let datos={"idusers": this.contenido.usuario.idusers, "token": this.contenido.usuario.token, "password":""};
    this.showPrompt("Actulizacion","Ingrese su nueva Contraseña",datos,"Contraseña");

  }
  showPrompt(titulo,contenido,datos,opcion) {
    const prompt = this.alertCtrl.create({
      title: titulo,
      message: contenido,
      inputs: [
        {
          name: 'captura',
          placeholder: opcion
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
            console.log(data);
          }
        },
        {
          text: 'Actualizar',
          handler: data => {
            console.log('Saved clicked');
            if(datos.celular==""){
              console.log('celular');
              datos.celular=data.captura;
              this.authServiceProvider.actualizar(datos)
              this.contenido.usuario.celular=data.captura;
            }else if(datos.password==""){
              console.log('contraseña');
              datos.password=data.captura;
              this.authServiceProvider.actualizar(datos)
            }
          }
        }
      ]
    });
    prompt.present();
  }
}
