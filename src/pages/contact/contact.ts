import {Component} from '@angular/core';
import {ModalController, NavController, PopoverController} from 'ionic-angular';
import {PopoverComponent} from "../../components/popover/popover";
import {WelcomePage} from "../welcome/welcome";
import {InformacionPage} from "../informacion/informacion";
import {DomSanitizer} from "@angular/platform-browser";
import {InAppBrowser} from '@ionic-native/in-app-browser';
import {StreamingAudioOptions, StreamingMedia} from "@ionic-native/streaming-media";

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  public userDetails: any;
  vid1 = "https://www.youtube.com/embed/ALvn4WfrsN8";
  vid2 = "https://www.youtube.com/embed/rwlhKIh3ZFM";
  vid3 = "https://www.youtube.com/embed/_U4cqdYzdsk";
  vid4 = "https://www.youtube.com/embed/2NPTfp8TvK8";
  vid5 = "https://www.youtube.com/embed/eeuz3QRwUEM";
  vid6 = "https://www.youtube.com/embed/T1wu9I1KpIQ";
  vid7 = "https://www.youtube.com/embed/iFSvCENz46U";
  vid8 = "https://www.youtube.com/embed/cJjmnuf06bM";

  constructor(public navCtrl: NavController, public popoverCtrl: PopoverController, public modalCtrl: ModalController, private dom: DomSanitizer, private streamingMedia: StreamingMedia, private iab: InAppBrowser) {
    const data = JSON.parse(localStorage.getItem('userData'));
    this.userDetails = data.userData;
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverComponent);
    popover.present({
      ev: myEvent
    });
    popover.onDidDismiss(data => {
      if (data) {

        console.log(data);
        if (data.id == 4) {
          this.logout();
        } else if (data.id == 1) {
          let moduloModal = this.modalCtrl.create(InformacionPage, {
            contenido: {
              titulo: "Informacion",
              usuario: this.userDetails
            }
          });
          moduloModal.onDidDismiss(data => {
          });
          moduloModal.present();

        }
      }
    });
  }

  logout() {
    localStorage.clear();
    setTimeout(() => this.backToWelcome(), 1000);
  }

  backToWelcome() {
    //this.navCtrl.setRoot(WelcomePage);
    //this.navCtrl.push(WelcomePage);
    this.navCtrl.push(WelcomePage).then(() => {
      let index = this.navCtrl.length() - 2;
      this.navCtrl.remove(index);
    });
  }

  sanitize(url) {
    return this.dom.bypassSecurityTrustResourceUrl(url);
  }

  startAudio() {
    let options: StreamingAudioOptions = {
      successCallback: () => {
        console.log('Finished Audio')
      },
      errorCallback: (e) => {
        console.log('Error: ', e)
      },
      initFullscreen: false, // iOS only!
      bgImage: 'http://www.plan100mildiscipulos.com/app100/assets/img/app100.png',
      bgImageScale: "fit", // other valid values: "stretch", "aspectStretch"
      bgColor: "#46B8C9",
      keepAwake: false, // prevents device from sleeping. true is default. Android only.
    };

    //http://soundbible.com/2196-Baby-Music-Box.html
    this.streamingMedia.playAudio('http://198.49.72.250:9300/stream?type=mp3', options);
  }

  stopAudio() {
    this.streamingMedia.stopAudio();
  }

  verMas() {
    let url = "https://en.wikipedia.org/wiki/Suliasi_Kurulo";
    this.iab.create(url, '_system');
  }
}
