import {Component} from '@angular/core';
import {AlertController, ModalController, NavController, PopoverController} from 'ionic-angular';
import {VisionPage} from '../clases/vision/vision';
import {AdoracionPage} from '../clases/adoracion/adoracion';
import {DiscipuladoPage} from '../clases/discipulado/discipulado';
import {RelacionesPage} from '../clases/relaciones/relaciones';
import {EymPage} from '../clases/eym/eym';
import {MisionPage} from '../clases/mision/mision';

import {AuthServiceProvider} from "../../providers/auth-service/auth-service";
import {PopoverComponent} from "../../components/popover/popover";
import {WelcomePage} from "../welcome/welcome";
import {InformacionPage} from "../informacion/informacion";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {StreamingAudioOptions, StreamingMedia} from "@ionic-native/streaming-media";


@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  public userDetails: any;
  public userData: any;
  progreso: any;

  visionIcon = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEwAAABOCAYAAACKX/AgAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABt1JREFUeNrsXP2VmzgQh33+P1wFoQS2grAVHFtB2AoOV3CkAqcDnAqcrQCnApwKcCrQdsCh2yHRKtJo9GWTZOc9nvEuCGk0H7/5wEniQNM0lfNxmJ6pTV7JyLBq+kH9K0fMDMsEhk1/0tpTD6YN80cBX+/SND2udHPZ/HGaj/N8fIPz0zzf86UnshOErF0ps8pJTc7zvfGYzxfh/N2FGZHPR0G4NNP8/XiN3cukXcsu6HAYNwmmZ3JJ0khYEWoy3Xw0Ftf3wiQqDwa0MBaTFrZDzACnzjD2QcWtKFCBIjHSDu4sbUunYJBIgyTNg+a6WvOMQjdwKIb10rjMJDWSUR2IjOonGrFFdYBhDLmuVjAL24zcMM+aS6et5/suOZi0iRM3GGkqo15I2fJsxOMtNMIzBsK4B8Qud1beFCbGFBMvEFUuDLvFJnfqCEbcaVxREGDdKmaXVA+oMpiNgxMJQTViOnypB+nETENGXXCjkA6qQ+gCLootdkdyTpei1kZKCklUKfinizVpgi0LTS+0amMMNtOUx163ABv4zQ/z354wZs0fNTLkEWJQmen7+fgE59xuvIHrcuH/ifAdozM85xt8fzsfFYL8dWPcw/rdQxKCO6ZISe2Kvg1ecMSgkIUDGqNHLgAdGNFw/4STpHFyC0f0HSoQbWtGgB5VEpsM3qs27PKoGWeA7ybPOFjONcM84zXTKC88DKKyPVHlnJC77ZxDMKPFQgVkkT3Rvu0UkYM1sHVJgmrGLF0Y1FN2EtkpETuZYrtWsF+2VHkwrDGFZLYMMmYGEMzVWqhZ6YGx8kimZASteDF+StDZE2AanmE9ihgMdkEXdP/FrwUmm9TmluMdGK8GvPQOPgsDTvSpS/CNola9OB+eVLs/QIaiIiD6ihAwjz5SAmqKwYkikoQpaQPoWilBBNLl8h8X20VE5pwhj8suihUoqO7cgwNRGeMC7nOhvy+d1+8xt6zJr9kE3AdC0N17zH+0nFPvyzAWAVNhziOYp0Q8JJp43HgKWaYJWkV18aWToN464p76TA2UYSysBrEHE5VJDuirb+yIYarMU7JGkUmElBGjAE6hTDd5g9aQVWXPvNVBSh/XFvd2KmkERvVU9f+VGNYgRQmX9HN/CRDsUvkOoZINkumNTlF7KxC89tbwf6oTeR/IaVgVs6MlCyPCioKQ6Bsh/m0DS6GxXmEq6lYIhikjAddRKN6qshyNQ2o8brpIMrSyx1qylQ1SWK1M/Q3UyrTADBlmZNK8OsJGNOAtd9R0umu7ABNUYBQYGSr4NjmARgEzmMXmdJryITPl8my7eFCgiNixSbBzPupSGHofKF55lMB2aYBGzErCsL4qYcCdkPSjJBBdy/uyPRt06kPAhWIHUKPp1xic0kVIe9FPnTwGgEpNUZs8F2ZzSoNmVIp55oqsS+dVi1QY9NqhxNZH8mQiZYbOnlLKThxiYS1yd2GAMps3OteZBsV6ihjMEgudpHYfg92rLbIFXgk92Lylb7aT1S5GQN1IUGBHRf8WrQL5FKbPi+z+qW1aLkDVOT9EbUaxTLlQHUMPUrVEIbkiaXiRvojccixTONQrFlNO4TsLRWY2oVWxN3mhgBIrJvpyy2zoZWJDT6koI6i5nF0tiSHL1ZmVKvScH/8IuaiPaZpuXTciee5apNCH+TmtQ0VaR9t5vI8Uu0uonfICyz4orNB4zcJSzUK0mDNLR9W7wBcv4GrAZc1k7h5UhTKZg2oebDfXlmFYXfKNcM4X/RUVS8l+Jc9NuAnU/3hJ/v7/Zg68YXi5/jM0suwtVHo/3/PgoLqfkpevMiaGWqtf8G3RODJMtP5SlwTkdd87N4gpAw9YAX5aQhEqlKCU32w7EstrMquZIhIxC2tTSBkuyZ8bXZlMocMPcGD9C0e4Zgv2SqbFxnw2zKtYirAEl//oU1YLUryV1EuVNBxNdkThGesAkCFYeV8AyCxInCmmoYlqWyO7WBuSk9dgGHOuFAXqq8gt7m8tbFgfWcImG7zp1Cqg+FGPp4v/cIadpvSyyVheOpNscmMCvz4NdXvBIJ+S9RK3T6Vm488zc+6S5y7vBWjzT44n71VC4MwwR1RNpUywn1HfKoOmmXspUcCZzOHMndzVuFmxVFDR+1Mgxm15+Jf8eKcgg/PbNdiVIqCXzKlRiwPcyFYjQlOY15x3hOdYMUxMTa3Rg7WO2VVGhRMuDPujKTTDNitZFEfZ76n5K2peLgatxUvmOqykoC+WY59+R4bFxFnb5JVe6deR2JUY/dLChh2v+Yuea7FhnFn/Wlx/NYbdrIRhNvHg+RVcmnvLJp8q/G9nw5agN3nu6cgRyfpw7UTlfwIMAKz0HP75+8P8AAAAAElFTkSuQmCC";
  adoracionIcon = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFQAAABRCAYAAABMpoFyAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABVhJREFUeNrsXP91mzAQxnn+v2wQsgGZoHSCOBPUmSDOBKETOJ3AzgQkE+BOgDOB6QSwAZXaI7lcJQySEBjr3uPFLzbi9Ol+fHcgPG/kUlWVz45D9U/WnhNtQKPqQ4qx63txYvj6DtAzEweoA9QB6gB1MnJAGd1ZsmM1IPUK2BFzCjYFHrlCPDLW5KGVog51YVDwQuHULRRP4PsAC7pgfwKky8kD+oI+c9cLLWN6gz7vZ7NZPpSrrtmRwgrrjpUhr93YdHlw81pWg+BC62ddqyJxtLAFKCRDLL4BMGtJu2ZFLAcdZQTjRZYATdCpiW4sJnNIPM3V1VUITy6wBGiMTl1o6p8Srw1UBtkQUGMNhXxYpNByDF2Y4J/E3SMdEDICqnaSss1DDQEZgy6B7oAByZSFTeozJKAkZFS9TYpb7UCAHoZaSONzJtTHtqWk4BlLi7V/oZ2EOsSTyd4sGyRv8BgK3LQYoJTse27GmI1qsO7HHYbvjGlzb52AnUwATFrNZX23+pqsNJ0AoKERagiVhFJXCUBNpxJHdeYDXht6xMzdoy5qrOC9T0H7hXXsCBxUrb37E3517KCgFrZr9hO0ynX1v2T4BykF1UHXmquKwyXllw46KaAJab5Hx7JVrBpHIYTQC8YjACEiXnhQvccEnDUBS/X7VFoUj613qCTJQyabMbvCoWqW5UB6FUf0isZeaVRQF0ekc5PC/6K+qRm6zlKgV0gWfzMWEH2ktLBfesTdUtMxCPSQWWQqScAZ6LkcrOo7onjV0Eip+mpCCLpFbQE1ttAXiorzWMh5lwkguEWYeKKD6/JoyF54TE2tAQpgYtl3OPd6xsT7/MzTV0MLUy9wKblOk/BzcjyeShK9ULCEiFgmV/ya/f3R4nT+sFYN/s8eIxK+zltLMK/YOVdkAW5sWei7IMV3LRUfo/AFKAUL0DmkzTtaZu1a5yI+mnfe5lHIeQsgTSagU5MQJyeGBffCW2TN3VweWnibMwVTKfsfi6E62Xwq0in7XzSVkt7Hs+lcvnXI5lORHGX/XRua12ShPsnmuw7ZfDKAonj5C/0/6I02OXGAOkAdoA7Qs5Rc8llL5iOZXAC3YUPguq+IVchoHWchS0RhOvUJ2PhbaNvyjP00OUC9j54or0Z4o3jLJn3XwJFTAxUcv+4l/N1PCVCR8NsRvxmoMQEzMAEmVDuPiHPfTi2G5lCF4Xh2L/jdPQFzC4eKdQqLmClYaN1hL5nl8HhWP7Xyt31G4iluHz6x7x7A4krPwK2UqVjoe4O3qTUmkFfJ50kAioE4ha5ULtF9UJcvEQ3ZM7e7g9j00oeyqtYv0XcL1MsntKnU0b0zoDyeQZzjseyZcjvB72VAWxF2/Rd2/QcBcPw7Ef/cArf1PYVW5VxRyYeuJHpIE5UAJ/ttqUOhXC3vankHqAPUyWkCukeU5tngeKWh8Yzz0FrBULFWbpNNr8c6Xociph2gtYKc/HYsB20KX+ydpWt9QZ/flFy+buLafDZe8Kh43mAdl5YXT3tyse0NB+SZ+ELw/Wqgd5C02uRwLClhC7ixpDvugYpKVezigY0dHIJr7FUHWph8d1xH65RaAtlZklkAdGPkerDLo7Dx/g3BprG0w0JvetTLN/qGR7I6vbwhVrIDLzxyTmpjr79gt0ignXX7fEuMZHvOSsFy6u0wgUHd6PubNn3EkKrSf59oCPvNDzp7Lxv2liYQj3XvjKZGrbPBGg4aYy1N7gUFK8pMb9gVbCKLTceShYlXtUl2sGm7KoxbyHb0KYyX9M4kwE0znQQAFpXCsTa5txK9ozSB8VeaYSmDsVov9h8BBgDSaRvflBbhDQAAAABJRU5ErkJggg==";
  discupuladoIcon = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFsAAABTCAYAAADwZXv0AAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABqZJREFUeNrsXOtx2zgQJm/8P0wFkSswrwLTFUSuIEwFUSqwXIFyFchXgXQVUKqAdAVSKpBSAY/wLX2rFUguSDyoCDvDoSzzAXxY7ONbQEEwMinLMi3/l6w6ksCLMbCz8lxW1RF5dPSDvSrlknvA9YMdVceiOg4SwDOPkDnQlxLAZx4dc6BTwA8eFbOA5wTw9BL78ceFtPOZ/H3vVdCsdntH6Sr+9mbEy28D9hZ93vihG2YmpijqOIDZmOGsEXiT9+8gFk8h69zBvTsfi6snL1jE/yfknnlDlunT+xag85IvO6TBHDl4wJuzQxPiNRxstC2Zj6HPoUOwd9VpYvGVt2EY7q8u9BNabRloIc75FFdx9mcH7/xylWbEgQmp5WNlSo7XptkTR++NfbruuREvHuwLFlcOMndhPyvnGF6jZhdX8s5RgP2Pg3durjldF0sSbBJE15mug/xlU6tdA+1as4VW7yxp90MFtnMz4kyzIW22od3rMQA9ClGs1KjKAZfTPNhlGRsEO/UInwOeGgB6efUZJGzbENnjXfAf+7etbOocwNGliSKBeagO8a5v1SH8wyucxf/2LqKT0CCoEYAqOvwJPjel6F+rzr/AGr5EE9BiILOOaGcPh7jnF5zfBsQE7x1qAnaCwLyDs4pjOgJAouOrAYCL+x/hnAfDefM6itmiWXGsBqKwArZwaAjMe/isI1Y+QjxcQI0yJp1+n/pk1mCg1/A5C8wTXe8mqTp+wrnGRPzvmQ5K2KGtOQBZPzix0AERe//A0xjaQrX0RMNgAMQgPQXuKkE0a33ggp1ZALfLJOw7Zk6tBFHguOTFAfumJWJIHDd2wtDQaATtbHPUrHT9i0SDvLQ70jX57ie96KbhZjolb9H3CUQc0ysF9gjgvoKp2CBrMG3TbA7YOObc1OEQ2Wqxh+/jEdpOHeYAg8tNhpKAFCxumCPZ6cyqRnyVhId3khDt4rS2p3zganaBNFRZUyEkK1oSH53xuQut5ThEnoJJNuvHkmtO1kD3zTyFrYO96qblAP2am/pZDQldHHNumpGbFrI4nFwTDWhkItnWMYdzPmAR/BIYRSt+RILbkqtxrdslJNo41Qj2vOGaGQB4kLQvq7XW1U4DCW4lLPpPum7M2gCQ7BxYmQSbXL/TNasMAN5kEnMVAEo6QhINi3WZEQXtyRyDG4HiLbpMnqp2499ukj08G9Bo1nNUZ4FLcJV+ugO06KBajupTZOWaBttgDwAXO+oFNzJJdYRaXXZVsk0vdQG2RnCnvXwJVMDrSGA3INbdwQDMJPY/5ZgS3WDbBDfs20BCSqmWwWjmVUAWR+P5xyqrW0sy0V0bb8xoewJZbNIjQ66zzi28m82IhhytDk7LYBEQT6/w0jeiylA6/l4qo/adDG7jokkT4JK+iorRDzbYAOgCOlDX1VQIpD3iGApEO06I9icDQC+gw+L8jTzrpSbCDIEbk2dSJRL1xjkXbBM7AQoMUK2daKbca6ZkX3o+rw+4VMR6wsdOsMHr21o9tEH2eYOq5QmiZJPAbMFWB7hUvnNMSQhptquqCzUPYgbsDThg3eDuSXtYS5JDScXlwTHvvMfa38MB6wZ3TzjvAsLNp6Fgn9kemOIZeXkQ2F2b0eWAa8euA9wNmmnS0K4v2LRS88pozN+wEDIKzpecmeKNBZgp6ix1wBt49xKUI+qrtT3bx3ofBZv9S5Aw4puAFDWRs/sUmK0/1oOb9nDQygmJ5DlPBLc1x4zgXVsiQP8oSWpyWUyrku6TaCO2ZHpw6LnR9WCY1ZjFEwXvW86NqzbOWkJ/5gMbmkpYwqwHu9hE+05tbO2Q0M9pr85Lrsk11hvnTYMLpFAC16xaSK9GQssir51K2hRxWK/Wiouk1JOaALvh+tFuSJIowwGOVIVLzjtozZ1GjZi1XMsul1kEeAJ9aKWb26KR75BF1tMgBseJs7sj+r944YzLdkkcFzdsoo506wJcErNzZtax66GzHpWYuKdmcOuNSibHpuZ2OOppa5wttLS66LNCXCw0UjhOTP53hljAfWBu4a1k5uqHsnpqrizj7FwbSJOaR0gSVNLck8wRZXc4vS4kDcRORNjt+e8G7pmiMRqmi5TC9GpAaF2h1bdUu2E64sU/LJJ+LOAqgc3IBnWm42eZqaTeKGbJn5cC7mCwWwYg0UBIndGrwfl+xhOGbczgGgG7KUYm5kdXMiIG5PkSwLUGdgN5g2eAChU6RJyB6wxsCw54dOCOCmxNDni04I4ebIYDnlwKuFT+FWAAKEMQ+BLibhsAAAAASUVORK5CYII=";
  relacionesIcon = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEMAAABDCAYAAADHyrhzAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3hJREFUeNrsXIFxmzAUBV8HYAOTCUonCJmgjIAnqDOByQR2JiCdwMkE2BPgToA7Ad6ASjn56lIBEvr/I9l+dzonsQ3i8fT1/5OI5ymiaZqMtbr5iy1rgXdrYBe9buQob42IsOlHei3XOlP4TGj4/lWRcTO4k3En407GIL5czhodwTAaOMacfTeW/P3k+/7hmvIIU5SsRS4RkTW4qFwio27wkRj0L+CJnUj/K8mxC6HsCIIMCmQjSch1VWhCPNVsctIkgqf4fHjppvp8AtgKtQRjlLFFVkWt07ERauhTSTRGjiUiEekERFyeX7l28lvSlH1xPiDXHWv7jvfeWK5x1BgaOcIQ5bnOE+vHCSLAxtDBscMmqKcO4Lak4yvWMF2zlcpwmZwMEVwpDKIfLiiDyilLXCDjkeg84dBUq0LGCbmTlIWcGRmiDO8rxd9M7xghGSHEMFmwdpQoZqGaR/TgAYDQSSJ/Ikr+JfQCkshnqikLRh+RuFjI8ixNrqAjU9JuYJrNVSL/SLyw82dUykkVC79tX6mNUKOcEVMNnzESL7s6iERIgE1EjjGWEarpAjWNBu5sjlzApZhkYJhCOZJhXWISgemoJxIF1lYGTgLv4T+b0JD8zPaAqXUBCntEQIKmrxs02UtNkLLw5Oyhde7SoKh7Z+2Dv/bZf7olfOLRQFZu7wy9DB6cKzHkAggyvhKWQm3i9wDHDITFWMi8DV0yKL2HOaKvEskIsXl/Rtt7OAIfP2gT4sxmFQDfpI+QwCkyEPd5cCLWtpNxkHQaC9x6CHXJ2BOS8ZvYK13NDO8WJt5bv2MvKST+iLFbEdylAwuY36jPOyZm/CRQxaskeIaebRAlNaaLXU5UHDbayhCFzgKJ665j//LwV/aMFLJEuDnpgI9SWKUM0bFYzPuQd4vPVEFXcsUzUNae2I8bG5QQirFLsW+0t9QW6zPgUA2Y62YacOKXHf1ak5LBJUuw/qkC6cOB0A69P1AYFcg1gW5M+WfXniCogurjrCs+WEbE2ZDJJdP8C5hN0EEGJyK2dFZ/ZiRsMFL1mSxSW0zEZ3Up2cb4iqIMokLMFBumjudW7KhBlSGW9mwn4tOMkcSOHfQw+e6ICxhINrvsocmIPXcAucgkJSN0iIxHScVLbu5YCYjHRmetjPOmcakMp/8xCMTmtWt6/NtY2X8EGACPQl7lQGkHegAAAABJRU5ErkJggg==";
  eymIcon = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAE0AAABOCAYAAABlnZseAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA7dJREFUeNrsXOF1m0AMhjz+h05gOkHpBCET1CPgDeIJSiYgnYBmAtIJTCbAngA6gd0JqC5PvBJ6B9zBhTPoe0/PzntBBx+SThIytjURqqpy4SMA8UHuQDwUEY4gJcgJJLNtO9Ok/xX1Hy0TwC4EJARJq/E4gyQg25b+B5CDDv1zkJXgiehAgOtEmvQXTLfq9TuKxyUgW+t6wdz6OxB3Abd9kj34RnFR11oGlK7jxiIQaUQakUakEWkEIo1II9KINCKNQKQRaUTaQNTdh42JJ+cYShrrdbGW9pZIGw4fhdyTYhqRRiDSiDQijUgj0gizJrdHlN8gWUeZxJLXL/jpGaRfDzgDKSkOwriK+jwcdMkFsxdvgysj9PsC/dFcpAUT62YExriG8o3o0B+NJc242tO27RI+9quKaWgZdcHNvm9aMaXEeHSp4xMQdZGxxFZBf8eJeX9wnbJvWHA20nBQjp18oNKdgONLDOq/4CJfODeB6f+G+vvcNWgdb9W6OzaOWTaCKXFuBP5Uk34jY1rWcJNMYA23aJUBJ10IUXgoUfep4eK8dGNj8ftyrimWVuCOFyjqC/D4QmAdOaYMnkqMRavljbt+aMqxReLSqQd/kcAE9ccqRPUQGOJNyKfUTSAQCNcK+xpPGndpD6VZcbD041Gmwlgcabi7eZjLDW3zsN9B3S+atEat2kxKPU7SK4NPOq3N0WQZzcy8xm3rb19jdu7rrDPtkQTVhbpnmTVGYJaloTvFHbXh3MjGENYyBrdR776C3p8qCv2O+tAUjKl/+7o3uVQnGevNs+GExYpkyRhCMlRxWJmPWINl8XAY6pImW1gh02nBBzdjmpvpNROWyzytwrZQNMGabt8iJgX9ug0eyvTBGmSdtRIm6M7OZU3sglUe1kxFVoVcuJ15GgbVYOZ8i+VE7J0a5dB3amC6Ub+3Y6qO8hOsv++sCDCopoYmriVKGzpuMEuQ97yE1uZkw7m1nLchqIJZ9k5k4e1Rq4QIe3PHr10hwWlYWWRAHJvb9XdDxhjsOh9Dt1ytdcl0fJ2GW64RGQZ7qTdeOeiW/srIEu6Mg5qQFY7VrIisHxjslXtubPfcrYSsR5DPQFY0tqtbbwThQuMa2xGfx1qWMLnFHfSwkDyNDQY+twcEJyftyonLrH/vf3z58IfFmojjDd+J/u8kIOWd2+FAszmYsPlYYLxcB0YSx457WGW6rEgcTRhKEhdbhHfEFT3uGBJT/xPnCn7wdVbp4a+ZOIpfksTJzTUsCH8FGAA1RqejMJf/vwAAAABJRU5ErkJggg==";
  misionIcon = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGIAAABiCAYAAACrpQYOAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABeRJREFUeNrsXY1x2jAUhkzgDeoN4kwQugGdoGaCuht4A3cDZwOnEwATmExgOoHZQJUuj2tKsH7fk2TQu9ORS4L+Pr1fPUnLxUyJcbr2+yWnOY5nmYBIQCQgEhAJiAREAiIBkYBIQCQgEhC3DASf+5x/iJLx0k382zdeTrwcOSbHBATOpK94eeSlgJ9taMfLgZc38fOcwAk6+bxUvPSMjkTdDQCd6AKAkpct808ClPLeJz/jpeZlYOFpgL5k98gBMQBwSWMoDll6BkAo3cZB8Qo6QhG0v/jbM3yerStbEsp9wxX74Ra5oLZcoR0o8JVFmyv4bgd1mVJ9a7rAVBG3vKwJ+rKGukdDhZ7PHYTCYNADrN7M0+KoDPTUCGJ1tgpZd5BV4H7qLpbyVkFoYjAZgUOamwJDE4TBRvl6EqXD7MHQBKGN2XEC7mh9g7HEXE38o1f8209um//CmCwIBBYQiV1AxFXY/QfexgmhjQp8Hhk9ReVrwCoaqVcQcFynsVo7xPZUhkYeExA9JQggu20isr2rLtIAo48FhJoYhBIhhlQR96EODUJBPAENw6PWVWco6i9CAiELXXQRcAI2d7bRiSjFRA0uJirs1I2MhnKHfmUKXVX6BiFTOD4rQk5zpS2hOB69+kgKBd041p172AAqCHVX7ROIgWpFICtoqsUi85vGGHRDhVB/7wGIgXgeSh9AbKkGB/V7IWLJ0FODkBNzQ8H80YqYK4ysswfDtmXbly8IWM8tneUVgo3X6AclEN+nQMCIeM6NYMyvE39ehRBLa6Q2Mo+iKUfq85q6DV15OCK3Mxtl/aHPo6v1ZCKaHid+v0PGfOdBqvjq8zMFEFPe6B55UL89AIHdxt5wzkhExgq5nXwu+kHH7PamqIkMg44QhC1Rn+lBhxxSMm/aoL0oHDnD0IxWe7o6YsrROhLZ5zskB/GTAwZ1U9DJyUmNkc01Ipy2Zx8ywv46idMHx/b3VAMDr/UbYpUbYu//zeXLD4uICcTIBgmE15jHGjUQAMaLIxgbqGORgHCnoyS4JlXOEiUaF0WsrHPEU6fnU6M5YX9b8tiXJMK4JRiQbjb2FvpVQ9E9q02SjS5pu8RsxItDB9l0JiZrffFdE1O2Qu57T+5AUoc4oH6bXKbxQx02PgfaQUVvqZiEyWQrB8etRZDRI8IY/AT9FDKwcqjTNb+1QIzalo4i1U82hyTxqwsEQm+wWEjBkHBjSwEE2lYpUqZ36atejfGM5BaTJuuvDeWpazBvtJgUktxY78kDChOt1fy+KovcOXeV2d35YX2sQCKWSPZqVHpCK8SMmGCcW3IuaqKyIlTfUAJhnXKJOEGdRj87asA1dBLtUS6JeBooYzEm+ghxq7VVtBMmCVljFZQSFvaaTs/wbkjLsOaBAgyjgyoM72BijeBkYZjJMqPDz0EVDcukIRRLmUEfMyrxpBh/7RMI1cZ+gezxWnnwSEp7qxtXYr4PM2qIm//scIZ3SrRE6p8VEEx9vDfMBWCKTrUEokkLDIZ7WF53HOHu5GCaV0AwuwsWrcBABmF75myNesPe+ccMLkVhuBfvloQgDFf6zaJQ0A4i6tOkASfVzP0ob4kIwvny9sIQXBSRtEQCQoQCRIdkFsNkfhHskIkJ+AKf+UL/JuNzzpNu7P8IRdw89mfxfuPZbkrMKeoVqTpPGE8hRH+V3MU2pglA5wl/H6hh8rEGCIK+EiY1k5m0pOksyD5Si2lKxwzGwCK8WZjpX1kXNwgWijOmC3hrLD9mrmCMIQdnaFLPC4QLVo/1knYTAJxzn2IAIzf0F0aWni0I6oFPgXJ+yKOw5MjKYvK9e8whnrZpF24HwQ/gSIlyeVzqEZzKDKGN23za5oqCpLrF0ilfit3bc2gfTMYxEgDu7/mzCQ7pAwCQHgSUWFgNMSjDtShraJrDo7HPi39vRdgqXlHEmfBoH42d2zPK54c7cklkdANR19OcrJ70sHgCIgGRgEhAJCASEAmIBEQCIgGRgEhAzBSIvwIMABZq5xG8XCDDAAAAAElFTkSuQmCC";


  //userData = { "username": "", "idusers": "","token":"" };

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public authServiceProvider: AuthServiceProvider, public popoverCtrl: PopoverController, public modalCtrl: ModalController, private streamingMedia: StreamingMedia, private iab: InAppBrowser) {
    //actualizar userData con la bd
    //this.userDetails=this.authServiceProvider.actualizarLogin();
    const data = JSON.parse(localStorage.getItem('userData'));
    this.userData = data.userData;

    this.userDetails = this.authServiceProvider.consultarDatos();
    console.log("datos")
    console.log(this.userDetails);
  }

  goVisionPage() {
    //this.descargarAppFull();
    this.sincronizarProgreso();
    if (this.userDetails.nivel == 0) {
      this.showAlert("Estamos Validando su cuenta..");
    } else if (this.userDetails.nivel >= 1) {
      this.navCtrl.push(VisionPage);
    }

  }

  goAdoracionPage() {
    //this.descargarAppFull();

    this.sincronizarProgreso();
    if (this.userDetails.nivel == 0) {
      this.showAlert("Estamos Validando su cuenta..");
    } else if (this.userDetails.nivel >= 2) {
      this.navCtrl.push(AdoracionPage);
    } else {
      this.showAlert("Nececita terminar el curso Visión");
    }
  }

  goDiscipuladoPage() {
    //this.descargarAppFull();

    this.sincronizarProgreso();
    if (this.userDetails.nivel == 0) {
      this.showAlert("Estamos Validando su cuenta..");
    } else if (this.userDetails.nivel >= 7) {
      this.navCtrl.push(DiscipuladoPage);
    } else {
      this.showAlert("Nececita terminar el curso Adoración");
    }
  }

  goRelacionesPage() {
    //this.descargarAppFull();

    this.sincronizarProgreso();
    if (this.userDetails.nivel == 0) {
      this.showAlert("Estamos Validando su cuenta..");
    } else if (this.userDetails.nivel >= 2) {
      this.navCtrl.push(RelacionesPage);
    } else {
      this.showAlert("Nececita terminar el curso Discipulado");
    }
  }

  goEyMPage() {
    // this.descargarAppFull();
    //
    this.sincronizarProgreso();
    if (this.userDetails.nivel == 0) {
      this.showAlert("Estamos Validando su cuenta..");
    } else if (this.userDetails.nivel >= 2) {
      this.navCtrl.push(EymPage);
    } else {
      this.showAlert("Nececita terminar el curso Relaciones");
    }
  }

  goMisionPage() {
    //this.descargarAppFull();
    this.sincronizarProgreso();
    if (this.userDetails.nivel == 0) {
      this.showAlert("Estamos Validando su cuenta..");
    } else if (this.userDetails.nivel >= 2) {
      this.navCtrl.push(MisionPage);
    } else {
      this.showAlert("Nececita terminar el curso E&M");
    }
  }

  showAlert(texto) {

    const alert = this.alertCtrl.create({
      title: 'Alerta',
      subTitle: texto,
      buttons: ['OK']
    });
    alert.present();
  }

  sincronizarProgreso() {
    this.authServiceProvider.actualizarLogin();
    this.userDetails = this.authServiceProvider.consultarDatos();
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverComponent);
    popover.present({
      ev: myEvent
    });
    popover.onDidDismiss(data => {
      if (data) {

        console.log(data);
        if (data.id == 4) {
          this.logout();
        } else if (data.id == 1) {
          let moduloModal = this.modalCtrl.create(InformacionPage, {
            contenido: {
              titulo: "Informacion",
              usuario: this.userData
            }
          });
          moduloModal.onDidDismiss(data => {
          });
          moduloModal.present();

        }

      }
    });
  }

  logout() {
    localStorage.clear();
    setTimeout(() => this.backToWelcome(), 1000);
  }

  backToWelcome() {
    this.navCtrl.push(WelcomePage).then(() => {
      let index = this.navCtrl.length() - 2;
      this.navCtrl.remove(index);
    });
  }

  startAudio() {
    let options: StreamingAudioOptions = {
      successCallback: () => {
        console.log('Finished Audio')
      },
      errorCallback: (e) => {
        console.log('Error: ', e)
      },
      initFullscreen: false, // iOS only!
      bgImage: 'http://www.plan100mildiscipulos.com/app100/assets/img/app100.png',
      bgImageScale: "fit", // other valid values: "stretch", "aspectStretch"
      bgColor: "#46B8C9",
      keepAwake: false, // prevents device from sleeping. true is default. Android only.
    };

    //http://soundbible.com/2196-Baby-Music-Box.html
    this.streamingMedia.playAudio('http://198.49.72.250:9300/stream?type=mp3', options);
  }

  stopAudio() {
    this.streamingMedia.stopAudio();
  }

  verLibro() {
    let url = "http://guimun.com/ecuador/seccion/5402/51884/7/misiones-internacionales";

    this.iab.create(url, '_system');
  }


  descargarAppFull() {
    let alert = this.alertCtrl.create({
      title: 'Versión Gratuita',
      message: 'Para acceder a los cursos descarga la versión premium',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Descargar',
          handler: () => {
            console.log('Buy clicked');

            let url = "market://details?id=com.co.gatewayti.full.p100md";
            this.iab.create(url, '_system');

          }
        }
      ]
    });
    alert.present();
  }

}
