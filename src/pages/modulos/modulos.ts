import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, Platform, ViewController} from 'ionic-angular';
import {EscrituraClavePage } from '../temas/escritura-clave/escritura-clave';
import {AplicacionPage } from '../temas/aplicacion/aplicacion';
import {MiCompromisoPage } from '../temas/mi-compromiso/mi-compromiso';

import { Cuestionario1Page } from '../temas/cuestionarios/cuestionario1/cuestionario1';
import { Cuestionario2Page } from '../temas/cuestionarios/cuestionario2/cuestionario2';
import { Cuestionario3Page } from '../temas/cuestionarios/cuestionario3/cuestionario3';
import { Cuestionario4Page } from '../temas/cuestionarios/cuestionario4/cuestionario4';
import { Cuestionario5Page } from '../temas/cuestionarios/cuestionario5/cuestionario5';
import { Cuestionario6Page } from '../temas/cuestionarios/cuestionario6/cuestionario6';
import { Cuestionario7Page } from '../temas/cuestionarios/cuestionario7/cuestionario7';
import { Cuestionario8Page } from '../temas/cuestionarios/cuestionario8/cuestionario8';
import { Cuestionario9Page } from '../temas/cuestionarios/cuestionario9/cuestionario9';
import { Cuestionario10Page } from '../temas/cuestionarios/cuestionario10/cuestionario10';
import { Cuestionario11Page } from '../temas/cuestionarios/cuestionario11/cuestionario11';
import { Cuestionario12Page } from '../temas/cuestionarios/cuestionario12/cuestionario12';
import { Cuestionario13Page } from '../temas/cuestionarios/cuestionario13/cuestionario13';
import { Cuestionario14Page } from '../temas/cuestionarios/cuestionario14/cuestionario14';
import { Cuestionario15Page } from '../temas/cuestionarios/cuestionario15/cuestionario15';
import { Cuestionario16Page } from '../temas/cuestionarios/cuestionario16/cuestionario16';
import { Cuestionario17Page } from '../temas/cuestionarios/cuestionario17/cuestionario17';
import { Cuestionario18Page } from '../temas/cuestionarios/cuestionario18/cuestionario18';
import { Cuestionario19Page } from '../temas/cuestionarios/cuestionario19/cuestionario19';
import { Cuestionario20Page } from '../temas/cuestionarios/cuestionario20/cuestionario20';
import { Cuestionario21Page } from '../temas/cuestionarios/cuestionario21/cuestionario21';
import { Cuestionario22Page } from '../temas/cuestionarios/cuestionario22/cuestionario22';
import { Cuestionario23Page } from '../temas/cuestionarios/cuestionario23/cuestionario23';
import { Cuestionario24Page } from '../temas/cuestionarios/cuestionario24/cuestionario24';
import { Cuestionario25Page } from '../temas/cuestionarios/cuestionario25/cuestionario25';
import { Cuestionario26Page } from '../temas/cuestionarios/cuestionario26/cuestionario26';
import { Cuestionario27Page } from '../temas/cuestionarios/cuestionario27/cuestionario27';
import { Cuestionario28Page } from '../temas/cuestionarios/cuestionario28/cuestionario28';
import { Cuestionario29Page } from '../temas/cuestionarios/cuestionario29/cuestionario29';
import { Cuestionario30Page } from '../temas/cuestionarios/cuestionario30/cuestionario30';
import { Cuestionario31Page } from '../temas/cuestionarios/cuestionario31/cuestionario31';
import { Cuestionario32Page } from '../temas/cuestionarios/cuestionario32/cuestionario32';
import { Cuestionario33Page } from '../temas/cuestionarios/cuestionario33/cuestionario33';
import { Cuestionario34Page } from '../temas/cuestionarios/cuestionario34/cuestionario34';
import { Cuestionario35Page } from '../temas/cuestionarios/cuestionario35/cuestionario35';
import { Cuestionario36Page } from '../temas/cuestionarios/cuestionario36/cuestionario36';
import { Cuestionario37Page } from '../temas/cuestionarios/cuestionario37/cuestionario37';
import { Cuestionario38Page } from '../temas/cuestionarios/cuestionario38/cuestionario38';
import { Cuestionario39Page } from '../temas/cuestionarios/cuestionario39/cuestionario39';
import { Cuestionario40Page } from '../temas/cuestionarios/cuestionario40/cuestionario40';
import { Cuestionario41Page } from '../temas/cuestionarios/cuestionario41/cuestionario41';
import { Cuestionario42Page } from '../temas/cuestionarios/cuestionario42/cuestionario42';
import { Cuestionario43Page } from '../temas/cuestionarios/cuestionario43/cuestionario43';
import { Cuestionario44Page } from '../temas/cuestionarios/cuestionario44/cuestionario44';
import { Cuestionario45Page } from '../temas/cuestionarios/cuestionario45/cuestionario45';
import { Cuestionario46Page } from '../temas/cuestionarios/cuestionario46/cuestionario46';
import { Cuestionario47Page } from '../temas/cuestionarios/cuestionario47/cuestionario47';
import { Cuestionario48Page } from '../temas/cuestionarios/cuestionario48/cuestionario48';
import { Cuestionario49Page } from '../temas/cuestionarios/cuestionario49/cuestionario49';
import { Cuestionario50Page } from '../temas/cuestionarios/cuestionario50/cuestionario50';
import { Cuestionario51Page } from '../temas/cuestionarios/cuestionario51/cuestionario51';
import { Cuestionario52Page } from '../temas/cuestionarios/cuestionario52/cuestionario52';
import {InAppBrowser} from "@ionic-native/in-app-browser";

/**
 * Generated class for the ModulosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modulos',
  templateUrl: 'modulos.html',
})
export class ModulosPage {
  modulo:any;
  public unregisterBackButtonAction: any;

  cuestionarios=[Cuestionario1Page,
    Cuestionario2Page,
    Cuestionario3Page,
    Cuestionario4Page,
    Cuestionario5Page,
    Cuestionario6Page,
    Cuestionario7Page,
    Cuestionario8Page,
    Cuestionario9Page,
    Cuestionario10Page,
    Cuestionario11Page,
    Cuestionario12Page,
    Cuestionario13Page,
    Cuestionario14Page,
    Cuestionario15Page,
    Cuestionario16Page,
    Cuestionario17Page,
    Cuestionario18Page,
    Cuestionario19Page,
    Cuestionario20Page,
    Cuestionario21Page,
    Cuestionario22Page,
    Cuestionario23Page,
    Cuestionario24Page,
    Cuestionario25Page,
    Cuestionario26Page,
    Cuestionario27Page,
    Cuestionario28Page,
    Cuestionario29Page,
    Cuestionario30Page,
    Cuestionario31Page,
    Cuestionario32Page,
    Cuestionario33Page,
    Cuestionario34Page,
    Cuestionario35Page,
    Cuestionario36Page,
    Cuestionario37Page,
    Cuestionario38Page,
    Cuestionario39Page,
    Cuestionario40Page,
    Cuestionario41Page,
    Cuestionario42Page,
    Cuestionario43Page,
    Cuestionario44Page,
    Cuestionario45Page,
    Cuestionario46Page,
    Cuestionario47Page,
    Cuestionario48Page,
    Cuestionario49Page,
    Cuestionario50Page,
    Cuestionario51Page,
    Cuestionario52Page];
  pdf=[
    "https://www.plan100mildiscipulos.com/app100/docs/1.%20Visi%C3%B3n.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/2.%20El%20coraz%C3%B3n%20de%20un%20adorador.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/3.%20Acci%C3%B3n%20de%20Gracia%20%20y%20Alabanza.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/4.%20El%20proposito%20de%20la%20adoraci%C3%B3n.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/5.%20La%20importancia%20de%20la%20oraci%C3%B3n.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/6.%20Dar%20%E2%80%93%20un%20acto%20de%20adoraci%C3%B3n.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/7.%20El%20llamado%20al%20discipulado.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/8.%20Las%20Marcas%20de%20un%20Disc%C3%ADpulo.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/9.%20Las%20decisiones%20del%20discipulado.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/10.%20El%20proceso%20del%20discipulado.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/11.%20Principios%20de%20un%20liderazgo%20cristiano%20efectivo.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/12%20La%20toma%20de%20decisiones%20en%20el%20liderazgo.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/13.%20El%20arte%20de%20delegar.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/14.%20Manej%C3%A1ndote%20en%20el%20uso%20del%20tiempo.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/15.%20formacion%20de%20caracter.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/16.%20La%20palabra%20de%20Dios%20nuestra%20base%20para%20la%20victoria.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/17.%20Escuchando%20la%20voz%20de%20Dios.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/18.%20Confesando%20la%20palabra%20de%20Dios.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/19.%20Caminando%20por%20Fe.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/20.%20Caminando%20en%20Victoria.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/21.%20Qu%20ees%20la%20oraci%C3%B3n%20y%20el%20ayuno.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/22.%20Venciendo%20la%20tentaci%C3%B3n.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/23.%20Los%20principios%20b%C3%ADblicos%20de%20la%20mayordom%C3%ADa.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/24.%20Sirviendo%20bajo%20un%20liderazgo,%20una%20vida%20de%20sumisi%C3%B3n.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/25.%20La%20esencia%20de%20la%20relaci%C3%B3n.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/26.%20Las%20relaciones%20interpersonales.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/27.%20La%20Familia%20la%20base%20de%20la%20sociedad.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/28.%20El%20significado%20de%20la%20comuni%C3%B3n.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/29.%20%E2%80%9CUnos%20a%20otros%E2%80%9D%20un%20estilo%20de%20vida.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/30.%20El%20Valor%20del%20trabajo%20en%20equipo.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/31.%20La%20importancia%20de%20pertenecer%20a%20una%20iglesia%20local.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/32.%20La%20tarea%20del%20Evangelismo.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/33.%20Una%20forma%20pr%C3%A1ctica%20de%20guiar%20a%20una%20persona%20a%20Cristo.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/34.%20La%20persona%20y%20el%20ministerio%20del%20Esp%C3%ADritu%20Santo.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/35.%20Unci%C3%B3n%20del%20Esp%C3%ADritu%20Santo.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/36.%20Entendiendo%20la%20unci%C3%B3n.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/37.%20C%C3%B3mo%20ser%20llenos%20con%20el%20Esp%C3%ADritu%20Santo.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/38.%20C%C3%B3mo%20ser%20guiado%20por%20el%20Esp%C3%ADritu%20Santo.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/39.%20Movi%C3%A9ndonos%20en%20los%20dones%20espirituales.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/40.%20El%20misterio%20de%20hablar%20en%20lenguas.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/41.%20C%C3%B3mo%20ministrar%20sanidad.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/42.%20C%C3%B3mo%20ministrar%20liberaci%C3%B3n.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/43.%20Viviendo%20un%20estilo%20de%20vida%20de%20perd%C3%B3n.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/44.%20Consejer%C3%ADa%20y%20sanidad%20interior.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/45.%20Entendiendo%20la%20guerra%20espiritual.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/46.%20C%C3%B3mo%20empezar%20un%20grupo%20celular.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/47.%20Como%20dirigir%20y%20desarrollar%20un%20grupo%20celular.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/48.%20El%20coraz%C3%B3n%20de%20un%20pastor.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/49.%20Nuestro%20involucramiento%20en%20las%20misiones..pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/50.%20Entendiendo%20la%20Gran%20Comisi%C3%B3n.pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/51.%20C%C3%B3mo%20ministrar%20en%20las%20misiones%20transculturales%20y%20transdenominacionales..pdf",
    "https://www.plan100mildiscipulos.com/app100/docs/52.%20Movilizando%20a%20la%20iglesia%20para%20las%20misiones.pdf"
  ];
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController,public modalCtrl: ModalController,public platform: Platform,private iab: InAppBrowser) {
    this.modulo=this.navParams.get('modulo');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModulosPage');
    console.log(this.modulo);
    this.initializeBackButtonCustomHandler(this.modulo,this.viewCtrl);

  }

  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  initializeBackButtonCustomHandler(modulo,ctr): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(function(event){
      console.log('Prevent Back Button Page Change');
      ctr.dismiss(modulo);
    }, 101); // Priority 101 will override back button handling (we set in app.component.ts) as it is bigger then priority 100 configured in app.component.ts file */
  }

  openNavDetailsPage(datos){
    console.log(datos);

    if (datos.id==1){
      let moduloModal = this.modalCtrl.create(EscrituraClavePage, {modulo: this.modulo});
      moduloModal.onDidDismiss(data => {
        this.modulo=data;
        console.log(data);
        this.initializeBackButtonCustomHandler(this.modulo,this.viewCtrl);

      });
      moduloModal.present();
      //this.navCtrl.push(EscrituraClavePage,{modulo:this.modulo});
    } else if (datos.id==2){
      //this.navCtrl.push(EscrituraClavePage);
      console.log(this.modulo.id);
      let moduloModal = this.modalCtrl.create(this.cuestionarios[this.modulo.id-1], {modulo: this.modulo});
      moduloModal.onDidDismiss(data => {
        this.modulo=data;
        console.log(data);
        this.initializeBackButtonCustomHandler(this.modulo,this.viewCtrl);

      });

      moduloModal.present();

    } else if (datos.id==3){
      let moduloModal = this.modalCtrl.create(MiCompromisoPage, {modulo: this.modulo});
      moduloModal.onDidDismiss(data => {
        this.modulo=data;
        console.log(data);
        this.initializeBackButtonCustomHandler(this.modulo,this.viewCtrl);

      });
      moduloModal.present();
      //this.navCtrl.push(MiCompromisoPage,{modulo:this.modulo});
    } else if (datos.id==4){
      let moduloModal = this.modalCtrl.create(AplicacionPage, {modulo: this.modulo});
      moduloModal.onDidDismiss(data => {
        this.modulo=data;
        console.log(data);
        this.initializeBackButtonCustomHandler(this.modulo,this.viewCtrl);
      });

      moduloModal.present();
      //this.navCtrl.push(AplicacionPage,{modulo:this.modulo});
    }

  }

  dismiss() {
    this.viewCtrl.dismiss(this.modulo);
  }

  verLibro() {
    let url = this.pdf[this.modulo.id-1];
    this.iab.create(url, '_system');
  }

}
