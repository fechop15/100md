import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController, LoadingController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { TabsPage } from '../tabs/tabs';
import { WelcomePage } from "../welcome/welcome";

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  responseData: any;

  nuevo=
    [{
      vision:
        {id:1,color:"#f193a3",nombre:"1. La Visión de CMF",modulo:[{
            titulo:"VISIÓN",
            icono:"vision.png",
            nombre:"La Visión de CMF",
            idModulo:"1",
            datos:[
              {id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
      adoracion:[
        {id:2,color:"#f193a3",nombre:"1. El Corazón de un adorador",modulo:[{
            titulo:"ADORACIÓN",
            icono:"adoracion.png",
            nombre:"El Corazón de un adorador",
            idModulo:"2",
            datos:[
              {id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:3,color:"#f193a3",nombre:"2. Acción de Gracias y Alabanza",modulo:[{
            titulo:"ADORACIÓN",
            icono:"adoracion.png",
            nombre:"Acción de Gracias y Alabanza",
            idModulo:"3",
            datos:[
              {id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:4,color:"#f193a3",nombre:"3. El propósito y poder de la adoración",modulo:[{
            titulo:"ADORACIÓN",
            icono:"adoracion.png",
            nombre:"El propósito y poder de la adoración",
            idModulo:"4",
            datos:[
              {id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:5,color:"#f193a3",nombre:"4. La importancia de la Oración",modulo:[{
            titulo:"ADORACIÓN",
            icono:"adoracion.png",
            nombre:"La importancia de la Oración",
            idModulo:"5",
            datos:[
              {id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:6,color:"#f193a3",nombre:"5. Dar – un acto de adoración",modulo:[{
            titulo:"ADORACIÓN",
            icono:"adoracion.png",
            nombre:"Dar – un acto de adoración",
            idModulo:"6",
            datos:[
              {id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]}
      ],
      discipulado:[
        {id:7,color:"#f193a3",nombre:"1. El llamado al discipulado",modulo:[{
            titulo:"DISCIPULADO",
            icono:"discipulado.png",
            nombre:"El llamado al discipulado",
            idModulo:"7",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:8,color:"#f193a3",nombre:"2. Las Marcas de un Discípulo",modulo:[{
            titulo:"DISCIPULADO",
            icono:"discipulado.png",
            nombre:"Las Marcas de un Discípulo",
            idModulo:"8",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:9,color:"#f193a3",nombre:"3. Las decisiones del discipulado",modulo:[{
            titulo:"DISCIPULADO",
            icono:"discipulado.png",
            nombre:"Las decisiones del discipulado",
            idModulo:"9",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:10,color:"#f193a3",nombre:"4. El proceso del discipulado",modulo:[{
            titulo:"DISCIPULADO",
            icono:"discipulado.png",
            nombre:"El proceso del discipulado",
            idModulo:"10",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:11,color:"#f193a3",nombre:"5. Principios de un liderazgo cristiano efectivo",modulo:[{
            titulo:"DISCIPULADO",
            icono:"discipulado.png",
            nombre:"Principios de un liderazgo cristiano efectivo",
            idModulo:"11",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:12,color:"#f193a3",nombre:"6. La toma de decisiones en el liderazgo",modulo:[{
            titulo:"DISCIPULADO",
            icono:"discipulado.png",
            nombre:"La toma de decisiones en el liderazgo",
            idModulo:"12",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:13,color:"#f193a3",nombre:"7. El arte de delegar",modulo:[{
            titulo:"DISCIPULADO",
            icono:"discipulado.png",
            nombre:"El arte de delegar",
            idModulo:"13",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:14,color:"#f193a3",nombre:"8. Manejándote en el uso del tiempo",modulo:[{
            titulo:"DISCIPULADO",
            icono:"discipulado.png",
            nombre:"Manejándote en el uso del tiempo",
            idModulo:"14",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:15,color:"#f193a3",nombre:"9. Formación de Carácter ¿Cómo se forma el carácter?",modulo:[{
            titulo:"DISCIPULADO",
            icono:"discipulado.png",
            nombre:"Formación de Carácter ¿Cómo se forma el carácter?",
            idModulo:"15",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:16,color:"#f193a3",nombre:"10. La palabra de Dios: nuestra base para la victoria",modulo:[{
            titulo:"DISCIPULADO",
            icono:"discipulado.png",
            nombre:"La palabra de Dios: nuestra base para la victoria",
            idModulo:"16",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:17,color:"#f193a3",nombre:"11. Escuchando la voz de Dios",modulo:[{
            titulo:"DISCIPULADO",
            icono:"discipulado.png",
            nombre:"Escuchando la voz de Dios",
            idModulo:"17",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:18,color:"#f193a3",nombre:"12. Confesando la palabra de Dios",modulo:[{
            titulo:"DISCIPULADO",
            icono:"discipulado.png",
            nombre:"Confesando la palabra de Dios",
            idModulo:"18",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:19,color:"#f193a3",nombre:"13. Caminando por Fe",modulo:[{
            titulo:"DISCIPULADO",
            icono:"discipulado.png",
            nombre:"Caminando por Fe",
            idModulo:"19",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:20,color:"#f193a3",nombre:"14. Caminando en Victoria",modulo:[{
            titulo:"DISCIPULADO",
            icono:"discipulado.png",
            nombre:"Caminando en Victoria",
            idModulo:"20",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:21,color:"#f193a3",nombre:"15. ¿Qué es la oración y el ayuno?",modulo:[{
            titulo:"DISCIPULADO",
            icono:"discipulado.png",
            nombre:"¿Qué es la oración y el ayuno?",
            idModulo:"21",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:22,color:"#f193a3",nombre:"16. Venciendo la tentación",modulo:[{
            titulo:"DISCIPULADO",
            icono:"discipulado.png",
            nombre:"Venciendo la tentación",
            idModulo:"22",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:23,color:"#f193a3",nombre:"17. Los principios bíblicos de la mayordomía",modulo:[{
            titulo:"DISCIPULADO",
            icono:"discipulado.png",
            nombre:"Los principios bíblicos de la mayordomía",
            idModulo:"23",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:24,color:"#f193a3",nombre:"18. Sirviendo bajo un liderazgo, una vida de sumisión",modulo:[{
            titulo:"DISCIPULADO",
            icono:"discipulado.png",
            nombre:"Sirviendo bajo un liderazgo, una vida de sumisión",
            idModulo:"24",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
      ],
      relaciones:[
        {id:25,color:"#f193a3",nombre:"1. La esencia de la relación", modulo:[{
            titulo:"RELACIONES",
            icono:"relaciones.png",
            nombre:"La esencia de la relación",
            idModulo:"25",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:26,color:"#f193a3",nombre:"2. Las relaciones interpersonales", modulo:[{
            titulo:"RELACIONES",
            icono:"relaciones.png",
            nombre:"Las relaciones interpersonales",
            idModulo:"26",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:27,color:"#f193a3",nombre:"3. La Familia: la base de la sociedad", modulo:[{
            titulo:"RELACIONES",
            icono:"relaciones.png",
            nombre:"La Familia: la base de la sociedad",
            idModulo:"27",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:28,color:"#f193a3",nombre:"4. El significado de la comunión", modulo:[{
            titulo:"RELACIONES",
            icono:"relaciones.png",
            nombre:"El significado de la comunión",
            idModulo:"28",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:29,color:"#f193a3",nombre:"5. “Unos a otros” un estilo de vida", modulo:[{
            titulo:"RELACIONES",
            icono:"relaciones.png",
            nombre:"“Unos a otros” un estilo de vida",
            idModulo:"29",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:30,color:"#f193a3",nombre:"6. El Valor del trabajo en equipo", modulo:[{
            titulo:"RELACIONES",
            icono:"relaciones.png",
            nombre:"El Valor del trabajo en equipo",
            idModulo:"30",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:31,color:"#f193a3",nombre:"7. La importancia de pertenecer a una iglesia local", modulo:[{
            titulo:"RELACIONES",
            icono:"relaciones.png",
            nombre:"La importancia de pertenecer a una iglesia local",
            idModulo:"31",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]}
      ],
      eym:[
        {id:32,color:"#f193a3",nombre:"1. La tarea del Evangelismo",modulo:[{
            titulo:"E & M",
            icono:"eym.png",
            nombre:"La tarea del Evangelismo",
            idModulo:"32",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:33,color:"#f193a3",nombre:"2. Una forma práctica de guiar a una persona a Cristo",modulo:[{
            titulo:"E & M",
            icono:"eym.png",
            nombre:"Una forma práctica de guiar a una persona a Cristo",
            idModulo:"33",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:34,color:"#f193a3",nombre:"3. La persona y el ministerio del Espíritu Santo",modulo:[{
            titulo:"E & M",
            icono:"eym.png",
            nombre:"La persona y el ministerio del Espíritu Santo",
            idModulo:"34",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:35,color:"#f193a3",nombre:"4. Unción del Espíritu Santo",modulo:[{
            titulo:"E & M",
            icono:"eym.png",
            nombre:"Unción del Espíritu Santo",
            idModulo:"35",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:36,color:"#f193a3",nombre:"5. Entendiendo la unción",modulo:[{
            titulo:"E & M",
            icono:"eym.png",
            nombre:"Entendiendo la unción",
            idModulo:"36",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:37,color:"#f193a3",nombre:"6. Cómo ser llenos con el Espíritu Santo",modulo:[{
            titulo:"E & M",
            icono:"eym.png",
            nombre:"Cómo ser llenos con el Espíritu Santo",
            idModulo:"37",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:38,color:"#f193a3",nombre:"7. Cómo ser guiado por el Espíritu Santo",modulo:[{
            titulo:"E & M",
            icono:"eym.png",
            nombre:"Cómo ser guiado por el Espíritu Santo",
            idModulo:"38",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:39,color:"#f193a3",nombre:"8. Moviéndonos en los dones espirituales",modulo:[{
            titulo:"E & M",
            icono:"eym.png",
            nombre:"Moviéndonos en los dones espirituales",
            idModulo:"39",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:40,color:"#f193a3",nombre:"9. El misterio de hablar en lenguas",modulo:[{
            titulo:"E & M",
            icono:"eym.png",
            nombre:"El misterio de hablar en lenguas",
            idModulo:"40",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:41,color:"#f193a3",nombre:"10. Cómo ministrar sanidad",modulo:[{
            titulo:"E & M",
            icono:"eym.png",
            nombre:"Cómo ministrar sanidad",
            idModulo:"41",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:42,color:"#f193a3",nombre:"11. Cómo ministrar liberación",modulo:[{
            titulo:"E & M",
            icono:"eym.png",
            nombre:"Cómo ministrar liberación",
            idModulo:"42",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:43,color:"#f193a3",nombre:"12. Viviendo un estilo de vida de perdón",modulo:[{
            titulo:"E & M",
            icono:"eym.png",
            nombre:"Viviendo un estilo de vida de perdón",
            idModulo:"43",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:44,color:"#f193a3",nombre:"13. Consejería y sanidad interior",modulo:[{
            titulo:"E & M",
            icono:"eym.png",
            nombre:"Consejería y sanidad interior",
            idModulo:"44",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:45,color:"#f193a3",nombre:"14. Entendiendo la guerra espiritual",modulo:[{
            titulo:"E & M",
            icono:"eym.png",
            nombre:"Entendiendo la guerra espiritual",
            idModulo:"45",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:46,color:"#f193a3",nombre:"15. Cómo empezar un grupo celular",modulo:[{
            titulo:"E & M",
            icono:"eym.png",
            nombre:"Cómo empezar un grupo celular",
            idModulo:"46",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:47,color:"#f193a3",nombre:"16. Como dirigir y desarrollar un grupo celular",modulo:[{
            titulo:"E & M",
            icono:"eym.png",
            nombre:"Como dirigir y desarrollar un grupo celular",
            idModulo:"47",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]},
        {id:48,color:"#f193a3",nombre:"17. El corazón de un pastor",modulo:[{
            titulo:"E & M",
            icono:"eym.png",
            nombre:"El corazón de un pastor",
            idModulo:"48",
            datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
              {id:2,color:"#f193a3",nombre:"Verdad Clave"},
              {id:3,color:"#f193a3",nombre:"Mi compromiso"},
              {id:4,color:"#f193a3",nombre:"Aplicación"},
            ]}]}
      ],
      mision:[
        {id: 49, color: "#f193a3", nombre: "1. Nuestro involucramiento en las misiones",modulo:[{
            titulo: "MISIÓN",
            icono: "mision.png",
            nombre: "Nuestro involucramiento en las misiones",
            idModulo:"49",
            datos: [
              {id: 1, color: "#f193a3", nombre: "Escritura Clave"},
              {id: 2, color: "#f193a3", nombre: "Verdad Clave"},
              {id: 3, color: "#f193a3", nombre: "Mi compromiso"},
              {id: 4, color: "#f193a3", nombre: "Aplicación"},
            ]
          }]},
        {id: 50, color: "#f193a3", nombre: "2. Entendiendo la Gran Comisión",modulo:[{
            titulo: "MISIÓN",
            icono: "mision.png",
            nombre: "Entendiendo la Gran Comisión",
            idModulo:"50",
            datos: [
              {id: 1, color: "#f193a3", nombre: "Escritura Clave"},
              {id: 2, color: "#f193a3", nombre: "Verdad Clave"},
              {id: 3, color: "#f193a3", nombre: "Mi compromiso"},
              {id: 4, color: "#f193a3", nombre: "Aplicación"},
            ]
          }]},
        {id: 51, color: "#f193a3", nombre: "3. Cómo ministrar en las misiones transculturales y transdenominacionales",modulo:[{
            titulo: "MISIÓN",
            icono: "mision.png",
            nombre: "Cómo ministrar en las misiones transculturales y transdenominacionales",
            idModulo:"51",
            datos: [
              {id: 1, color: "#f193a3", nombre: "Escritura Clave"},
              {id: 2, color: "#f193a3", nombre: "Verdad Clave"},
              {id: 3, color: "#f193a3", nombre: "Mi compromiso"},
              {id: 4, color: "#f193a3", nombre: "Aplicación"},
            ]
          }]},
        {id: 52, color: "#f193a3", nombre: "4. Movilizando a la iglesia para las misiones",modulo:[{
            titulo: "MISIÓN",
            icono: "mision.png",
            nombre: "Movilizando a la iglesia para las misiones",
            idModulo:"52",
            datos: [
              {id: 1, color: "#f193a3", nombre: "Escritura Clave"},
              {id: 2, color: "#f193a3", nombre: "Verdad Clave"},
              {id: 3, color: "#f193a3", nombre: "Mi compromiso"},
              {id: 4, color: "#f193a3", nombre: "Aplicación"},
            ]
          }]}
      ]
  }];


  userData = { "username": "", "password": "", "email": "", "celular": "", "pais": "","json":JSON.stringify(this.nuevo) };

  constructor(public navCtrl: NavController, public authService: AuthServiceProvider, public loadingCtrl: LoadingController, private toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  signup() {
    if (this.userData.username && this.userData.password && this.userData.email && this.userData.celular && this.userData.pais) {
      //Api connections
      console.log(this.userData);

      const loader = this.loadingCtrl.create({
        content: "Registrando..."
      });
      loader.present();

      this.authService.postData(this.userData, "signup").then((result) => {
        this.responseData = result;
        console.log(this.responseData);
        if (this.responseData.userData) {
          localStorage.setItem('userData', JSON.stringify(this.responseData));
          localStorage.setItem("json", JSON.stringify(this.responseData.userData.json));

          loader.dismiss();
          this.navCtrl.setRoot(TabsPage);
          
        } else if (this.responseData.error) {
          loader.dismiss();
          this.presentToast(this.responseData.error.text);
          
        }
        else {
          this.presentToast("Ingrese los datos requeridos");
        }

      }, (err) => {
          this.presentToast("Ingrese los datos requeridos");
      });

    }
    else {
      this.presentToast("Ingrese los datos requeridos");
    }

  }

  login() {
    this
      .navCtrl
      .push(WelcomePage);
  }

  presentLoading() {
    const loader = this.loadingCtrl.create({
      content: "Iniciando..."
    });

    loader.onDidDismiss(() => {
      this.navCtrl.setRoot(TabsPage);
    })

    loader.present();

    setTimeout(() => {
      loader.dismiss();
    }, 2000);

  }

  presentToast(texto) {
    let toast = this.toastCtrl.create({
      message: texto,
      duration: 2000,
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

}
