import {Component} from '@angular/core';
import {ModalController, NavController, PopoverController} from 'ionic-angular';
import {WelcomePage} from '../welcome/welcome';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';
import {PopoverComponent} from '../../components/popover/popover';
import {ArticuloPage} from "../articulo/articulo";
import {InformacionPage} from "../informacion/informacion";
import {StreamingMedia, StreamingAudioOptions} from '@ionic-native/streaming-media';

import {InAppBrowser} from '@ionic-native/in-app-browser';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public userDetails: any;
  public dataDetails: any;
  responseData: any;

  constructor(public navCtrl: NavController, public authServiceProvider: AuthServiceProvider, public popoverCtrl: PopoverController, public modalCtrl: ModalController, private streamingMedia: StreamingMedia, private iab: InAppBrowser) {
    const data = JSON.parse(localStorage.getItem('userData'));
    this.userDetails = data.userData;
    console.log(data.userData);
    //this.loadData();
  }

  backToWelcome() {
    this.navCtrl.push(WelcomePage).then(() => {
      let index = this.navCtrl.length() - 2;
      this.navCtrl.remove(index);
    });
  }

  logout() {
    localStorage.clear();
    setTimeout(() => this.backToWelcome(), 1000);
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverComponent);
    popover.present({
      ev: myEvent
    });
    popover.onDidDismiss(data => {
      if (data) {

        console.log(data);
        if (data.id == 4) {
          this.logout();
        } else if (data.id == 1) {
          let moduloModal = this.modalCtrl.create(InformacionPage, {
            contenido: {
              titulo: "Informacion",
              usuario: this.userDetails
            }
          });
          moduloModal.onDidDismiss(data => {
          });
          moduloModal.present();
        }
      }
    });
  }

  goBienvenidos() {
    let moduloModal = this.modalCtrl.create(ArticuloPage, {
      contenido: {
        titulo: "Bienvenido a la escuela de entrenamiento de empoderamiento de los creyentes",
        texto: "Cada persona nacida de Dios ha sido llamada por el Espíritu Santo para llevar adelante la Gran Comisión y derrotar a Satanás a través de la autoridad de las Escrituras y del ministerio de la persona del Espíritu Santo. En estos últimos días Dios está levantando un nuevo pueblo de cristianos radicales que ¡creen en Su palabra sin vergüenza alguna, la obedecen diligentemente y la proclaman con valentía!\n" +
          "<br><br>" +
          "Empoderando a los obreros cristianos de hoy para los desafíos de mañana ha sido diseñado para sistemáticamente enseñar, entrenar y equipar a las personas laicas en la iglesia local para que puedan convertirse en personas fuertes y efectivas en el Señor y en el Ministerio de la iglesia local y de la Misión para el mundo perdido. Permite que el Espíritu Santo sea tu maestro y te guie a medida que estudias las lecciones cada semana. \n" +
          "<br><br>" +
          "La instrucción revelada de este programa de entrenamiento te empoderará y motivará para que empieces a hacer las obras de Jesús al permitirle a Dios que te use para encontrarte con las necesidades de las personas a tu alrededor.\n"
      }
    });
    moduloModal.onDidDismiss(data => {

    });
    moduloModal.present();
  }

  goConsejosPracticos() {
    let moduloModal = this.modalCtrl.create(ArticuloPage, {
      contenido: {
        titulo: "Mi consejo personal para ti es",
        texto: "1.\tInicia cada lección de estudio con una oración pidiéndole al Espíritu Santo que guie y te de entendimiento. <br><br>" +
          "2.\tNo trabajes tan rápido. Tomate el tiempo para leer, estudiar y meditar en cada verdad que estás aprendiendo.  <br><br>" +
          "3.\tMarca tu Biblia con los versículos en cada lección y has notas personas a medida que Dios te habla y te revela el conocimiento.<br><br>" +
          "4.\tSobre todo, determínate en ser un hacedor de la palabra de Dios, no la escuches solamente. La aplicación para la vida es vital para tu crecimiento espiritual. <br><br>" +
          "5.\tComprométete con una iglesia local, y hazte disponible para apoyar a tu pastor enl la visión de la iglesia para alcanzar a la comunidad y el mundo.<br><br>"
      }
    });
    moduloModal.onDidDismiss(data => {

    });
    moduloModal.present();
  }

  startAudio() {
    let options: StreamingAudioOptions = {
      successCallback: () => {
        console.log('Finished Audio')
      },
      errorCallback: (e) => {
        console.log('Error: ', e)
      },
      initFullscreen: false, // iOS only!
      bgImage: 'http://www.plan100mildiscipulos.com/app100/assets/img/app100.png',
      bgImageScale: "fit", // other valid values: "stretch", "aspectStretch"
      bgColor: "#46B8C9",
      keepAwake: false, // prevents device from sleeping. true is default. Android only.
    };

    //http://soundbible.com/2196-Baby-Music-Box.html
    this.streamingMedia.playAudio('http://198.49.72.250:9300/stream?type=mp3', options);
  }

  stopAudio() {
    this.streamingMedia.stopAudio();
  }

  irAWeb() {
    let url = "http://www.plan100mildiscipulos.com/";
    this.iab.create(url, '_system');
  }
}

