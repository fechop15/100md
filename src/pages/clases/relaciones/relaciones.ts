import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {ModulosPage} from "../../modulos/modulos";
import {AuthServiceProvider} from "../../../providers/auth-service/auth-service";

/**
 * Generated class for the RelacionesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-relaciones',
  templateUrl: 'relaciones.html',
})
export class RelacionesPage {

/*  relaciones=[
    {id:1,color:"#f193a3",nombre:"1. La esencia de la relación", modulo:[{
        titulo:"RELACIONES",
        icono:"relaciones.png",
        nombre:"La esencia de la relación",
        idModulo:"25",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:2,color:"#f193a3",nombre:"2. Las relaciones interpersonales", modulo:[{
        titulo:"RELACIONES",
        icono:"relaciones.png",
        nombre:"Las relaciones interpersonales",
        idModulo:"26",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:3,color:"#f193a3",nombre:"3. La Familia: la base de la sociedad", modulo:[{
        titulo:"RELACIONES",
        icono:"relaciones.png",
        nombre:"La Familia: la base de la sociedad",
        idModulo:"27",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:4,color:"#f193a3",nombre:"4. El significado de la comunión", modulo:[{
        titulo:"RELACIONES",
        icono:"relaciones.png",
        nombre:"El significado de la comunión",
        idModulo:"28",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:5,color:"#f193a3",nombre:"5. “Unos a otros” un estilo de vida", modulo:[{
        titulo:"RELACIONES",
        icono:"relaciones.png",
        nombre:"“Unos a otros” un estilo de vida",
        idModulo:"29",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:6,color:"#f193a3",nombre:"6. El Valor del trabajo en equipo", modulo:[{
        titulo:"RELACIONES",
        icono:"relaciones.png",
        nombre:"El Valor del trabajo en equipo",
        idModulo:"30",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:7,color:"#f193a3",nombre:"7. La importancia de pertenecer a una iglesia local", modulo:[{
        titulo:"RELACIONES",
        icono:"relaciones.png",
        nombre:"La importancia de pertenecer a una iglesia local",
        idModulo:"31",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]}
  ];*/

  progreso: any;
  relaciones:any;
  temp: any;
  nivel:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController,public authServiceProvider: AuthServiceProvider) {
    this.temp=this.authServiceProvider.consultarTemasRealizados();
    this.nivel=this.authServiceProvider.consultarDatos();

    console.log("temp");
    console.log(this.temp);
    this.relaciones=this.temp.relaciones;

    console.log("relaciones");
    console.log(this.relaciones);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RelacionesPage');
  }
  openNavDetailsPage(datos){
    console.log(datos);
    if (this.nivel.nivel>=datos.id){

      let moduloModal = this.modalCtrl.create(ModulosPage, {modulo: datos});
    moduloModal.onDidDismiss(data => {
      console.log(data);
      console.log("relaciones");
      for (let i in this.relaciones){
        if (this.relaciones[i].id==data.id){
          console.log("actualizar");
          if (data.modulo[0].datos[0].color=="#e94b67" && data.modulo[0].datos[1].color=="#e94b67" && data.modulo[0].datos[1].color=="#e94b67" && data.modulo[0].datos[3].color=="#e94b67" && this.nivel.nivel==datos.id){
            //nivel a 2// si nivel es >2 no cambiar
            data.color="#e94b67";
            this.nivel.nivel=data.id+1
            console.log("completado")
          }
          this.temp.relaciones[i]=data;
        }
      }
        this.authServiceProvider.guardarProgreso(this.temp,3);
    });
    moduloModal.present();
     //this.navCtrl.push(ModulosPage,{modulo: datos});

  }
  }
}
