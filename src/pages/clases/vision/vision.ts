import { Component } from "@angular/core";
import {
  IonicPage, ModalController,
  NavController
} from "ionic-angular";

import {AuthServiceProvider} from "../../../providers/auth-service/auth-service";
import {ModulosPage} from "../../modulos/modulos";

/**
 * Generated class for the VisionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-vision",
  templateUrl: "vision.html"
})
export class VisionPage {


  vision:any;
  temp: any;
  nivel:any;
  constructor(public navCtrl: NavController,public authServiceProvider: AuthServiceProvider,public modalCtrl: ModalController
  ) {

    this.temp=this.authServiceProvider.consultarTemasRealizados();
    this.nivel=this.authServiceProvider.consultarDatos();
    console.log("temp");
    console.log(this.temp);
    if(this.temp.vision){
      this.vision=this.temp.vision;
    }else{
      this.vision=this.temp[0].vision;
    }

    console.log("vision");
    console.log(this.vision);
    console.log("nivel");
    console.log(this.nivel.nivel);

  }

  openNavDetailsPage(datos){
    console.log("datos vision");
    console.log(datos);

    let moduloModal = this.modalCtrl.create(ModulosPage, {modulo: datos});
      moduloModal.onDidDismiss(data => {
      console.log("vision guardar");
      console.log(data);
      console.log(data.modulo[0].datos[0].color);
      console.log(data.modulo[0].datos[1].color);
      console.log(data.modulo[0].datos[2].color);
      console.log(data.modulo[0].datos[3].color);

      if (data.modulo[0].datos[0].color=="#e94b67" && data.modulo[0].datos[1].color=="#e94b67" && data.modulo[0].datos[1].color=="#e94b67" && data.modulo[0].datos[3].color=="#e94b67"){
        //nivel a 2// si nivel es >2 no cambiar
        data.color="#e94b67";
        this.nivel.nivel=data.id+1
      }
      this.temp.vision=data;
/*      console.log(this.temp.vision);
      console.log(this.temp);*/
        if (this.temp[0]){
          this.authServiceProvider.guardarProgreso(this.temp[0],this.nivel.nivel);
        } else{
          this.authServiceProvider.guardarProgreso(this.temp,this.nivel.nivel);
        }

    });
    moduloModal.present();
    //this.navCtrl.push(ModulosPage,{modulo: datos});
  }




}
