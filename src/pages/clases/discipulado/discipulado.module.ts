import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DiscipuladoPage } from './discipulado';

@NgModule({
  declarations: [
    DiscipuladoPage,
  ],
  imports: [
    IonicPageModule.forChild(DiscipuladoPage),
  ],
})
export class DiscipuladoPageModule {}
