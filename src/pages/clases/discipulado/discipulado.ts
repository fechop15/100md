import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {ModulosPage} from "../../modulos/modulos";
import {AuthServiceProvider} from "../../../providers/auth-service/auth-service";

/**
 * Generated class for the DiscipuladoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-discipulado',
  templateUrl: 'discipulado.html',
})
export class DiscipuladoPage {

/*  discipulado=[
    {id:1,color:"#f193a3",nombre:"1. El llamado al discipulado",modulo:[{
        titulo:"DISCIPULADO",
        icono:"discipulado.png",
        nombre:"El llamado al discipulado",
        idModulo:"7",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:2,color:"#f193a3",nombre:"2. Las Marcas de un Discípulo",modulo:[{
        titulo:"DISCIPULADO",
        icono:"discipulado.png",
        nombre:"Las Marcas de un Discípulo",
        idModulo:"8",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:3,color:"#f193a3",nombre:"3. Las decisiones del discipulado",modulo:[{
        titulo:"DISCIPULADO",
        icono:"discipulado.png",
        nombre:"Las decisiones del discipulado",
        idModulo:"9",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:4,color:"#f193a3",nombre:"4. El proceso del discipulado",modulo:[{
        titulo:"DISCIPULADO",
        icono:"discipulado.png",
        nombre:"El proceso del discipulado",
        idModulo:"10",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:5,color:"#f193a3",nombre:"5. Principios de un liderazgo cristiano efectivo",modulo:[{
        titulo:"DISCIPULADO",
        icono:"discipulado.png",
        nombre:"Principios de un liderazgo cristiano efectivo",
        idModulo:"11",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:6,color:"#f193a3",nombre:"6. La toma de decisiones en el liderazgo",modulo:[{
        titulo:"DISCIPULADO",
        icono:"discipulado.png",
        nombre:"La toma de decisiones en el liderazgo",
        idModulo:"12",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:7,color:"#f193a3",nombre:"7. El arte de delegar",modulo:[{
        titulo:"DISCIPULADO",
        icono:"discipulado.png",
        nombre:"El arte de delegar",
        idModulo:"13",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:8,color:"#f193a3",nombre:"8. Manejándote en el uso del tiempo",modulo:[{
        titulo:"DISCIPULADO",
        icono:"discipulado.png",
        nombre:"Manejándote en el uso del tiempo",
        idModulo:"14",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:9,color:"#f193a3",nombre:"9. Formación de Carácter ¿Cómo se forma el carácter?",modulo:[{
        titulo:"DISCIPULADO",
        icono:"discipulado.png",
        nombre:"Formación de Carácter ¿Cómo se forma el carácter?",
        idModulo:"15",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:10,color:"#f193a3",nombre:"10. La palabra de Dios: nuestra base para la victoria",modulo:[{
        titulo:"DISCIPULADO",
        icono:"discipulado.png",
        nombre:"La palabra de Dios: nuestra base para la victoria",
        idModulo:"16",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:11,color:"#f193a3",nombre:"11. Escuchando la voz de Dios",modulo:[{
        titulo:"DISCIPULADO",
        icono:"discipulado.png",
        nombre:"Escuchando la voz de Dios",
        idModulo:"17",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:12,color:"#f193a3",nombre:"12. Confesando la palabra de Dios",modulo:[{
        titulo:"DISCIPULADO",
        icono:"discipulado.png",
        nombre:"Confesando la palabra de Dios",
        idModulo:"18",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:13,color:"#f193a3",nombre:"13. Caminando por Fe",modulo:[{
        titulo:"DISCIPULADO",
        icono:"discipulado.png",
        nombre:"Caminando por Fe",
        idModulo:"19",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:14,color:"#f193a3",nombre:"14. Caminando en Victoria",modulo:[{
        titulo:"DISCIPULADO",
        icono:"discipulado.png",
        nombre:"Caminando en Victoria",
        idModulo:"20",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:15,color:"#f193a3",nombre:"15. ¿Qué es la oración y el ayuno?",modulo:[{
        titulo:"DISCIPULADO",
        icono:"discipulado.png",
        nombre:"¿Qué es la oración y el ayuno?",
        idModulo:"21",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:16,color:"#f193a3",nombre:"16. Venciendo la tentación",modulo:[{
        titulo:"DISCIPULADO",
        icono:"discipulado.png",
        nombre:"Venciendo la tentación",
        idModulo:"22",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:17,color:"#f193a3",nombre:"17. Los principios bíblicos de la mayordomía",modulo:[{
        titulo:"DISCIPULADO",
        icono:"discipulado.png",
        nombre:"Los principios bíblicos de la mayordomía",
        idModulo:"23",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:18,color:"#f193a3",nombre:"18. Sirviendo bajo un liderazgo, una vida de sumisión",modulo:[{
        titulo:"DISCIPULADO",
        icono:"discipulado.png",
        nombre:"Sirviendo bajo un liderazgo, una vida de sumisión",
        idModulo:"24",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
  ];*/

  progreso: any;
  discipulado:any;
  temp: any;
  nivel:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController,public authServiceProvider: AuthServiceProvider) {

    this.temp=this.authServiceProvider.consultarTemasRealizados();
    this.nivel=this.authServiceProvider.consultarDatos();

    console.log("temp");
    console.log(this.temp);
      this.discipulado=this.temp.discipulado;


    console.log("discipulado");
    console.log(this.discipulado);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DiscipuladoPage');
  }

  openNavDetailsPage(datos){
    console.log(datos);
    if (this.nivel.nivel>=datos.id){

      let moduloModal = this.modalCtrl.create(ModulosPage, {modulo: datos});
    moduloModal.onDidDismiss(data => {
      console.log(data);
      console.log("discipulado");
      for (let i in this.discipulado){
        if (this.discipulado[i].id==data.id){
          console.log("actualizar");
          if (data.modulo[0].datos[0].color=="#e94b67" && data.modulo[0].datos[1].color=="#e94b67" && data.modulo[0].datos[1].color=="#e94b67" && data.modulo[0].datos[3].color=="#e94b67" && this.nivel.nivel==datos.id){
            //nivel a 2// si nivel es >2 no cambiar
            data.color="#e94b67";
            this.nivel.nivel=data.id+1
            console.log("completado")
          }
          this.temp.discipulado[i]=data;
        }
      }
        this.authServiceProvider.guardarProgreso(this.temp,3);

    });
    moduloModal.present();
      //this.navCtrl.push(ModulosPage,{modulo: datos});
  }
  }
}
