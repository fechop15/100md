import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdoracionPage } from './adoracion';

@NgModule({
  declarations: [
    AdoracionPage,
  ],
  imports: [
    IonicPageModule.forChild(AdoracionPage),
  ],
})
export class AdoracionPageModule {}
