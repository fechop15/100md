import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import { ModulosPage } from "../../modulos/modulos";
import {AuthServiceProvider} from "../../../providers/auth-service/auth-service";

/**
 * Generated class for the AdoracionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-adoracion',
  templateUrl: 'adoracion.html',
})
export class AdoracionPage {

/*
  adoracion=[
    {id:2,color:"#f193a3",nombre:"1. El Corazón de un adorador",modulo:[{
        titulo:"ADORACIÓN",
        icono:"adoracion.png",
        nombre:"El Corazón de un adorador",
        idModulo:"2",
        datos:[
          {id:1,color:"#f193a3",nombre:"Escritura Clave",idtema:"2"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave",idtema:"3"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso",idtema:"4"},
          {id:4,color:"#f193a3",nombre:"Aplicación",idtema:"5"},
        ]}]},
    {id:3,color:"#f193a3",nombre:"2. Acción de Gracias y Alabanza",modulo:[{
        titulo:"ADORACIÓN",
        icono:"adoracion.png",
        nombre:"Acción de Gracias y Alabanza",
        idModulo:"3",
        datos:[
          {id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:4,color:"#f193a3",nombre:"3. El propósito y poder de la adoración",modulo:[{
        titulo:"ADORACIÓN",
        icono:"adoracion.png",
        nombre:"El propósito y poder de la adoración",
        idModulo:"4",
        datos:[
          {id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:5,color:"#f193a3",nombre:"4. La importancia de la Oración",modulo:[{
        titulo:"ADORACIÓN",
        icono:"adoracion.png",
        nombre:"La importancia de la Oración",
        idModulo:"5",
        datos:[
          {id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:6,color:"#f193a3",nombre:"5. Dar – un acto de adoración",modulo:[{
        titulo:"ADORACIÓN",
        icono:"adoracion.png",
        nombre:"Dar – un acto de adoración",
        idModulo:"6",
        datos:[
          {id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]}
  ];
*/

  progreso: any;
  adoracion:any;
  temp: any;
  nivel:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController,public authServiceProvider: AuthServiceProvider) {

    this.temp=this.authServiceProvider.consultarTemasRealizados();
    this.nivel=this.authServiceProvider.consultarDatos();

    console.log("temp");
    console.log(this.temp);
    this.adoracion=this.temp.adoracion;


    console.log("adoracion");
    console.log(this.adoracion);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdoracionPage');
  }

  openNavDetailsPage(datos){
    console.log("datos adoracion");
    console.log(datos);
    if (this.nivel.nivel>=datos.id){

      let moduloModal = this.modalCtrl.create(ModulosPage, {modulo: datos});
      moduloModal.onDidDismiss(data => {
        console.log("adoracion");
        for (let i in this.adoracion){
          if (this.adoracion[i].id==data.id){
            console.log("actualizar");
            console.log(data.modulo);

            if (data.modulo[0].datos[0].color=="#e94b67" && data.modulo[0].datos[1].color=="#e94b67" && data.modulo[0].datos[1].color=="#e94b67" && data.modulo[0].datos[3].color=="#e94b67" && this.nivel.nivel==datos.id){
              //nivel a 2// si nivel es >2 no cambiar
              data.color="#e94b67";
              this.nivel.nivel=data.id+1
              console.log("completado")
            }
            this.temp.adoracion[i]=data;

          }
        }

        this.authServiceProvider.guardarProgreso(this.temp,this.nivel.nivel);
      });
      moduloModal.present();
      //this.navCtrl.push(ModulosPage,{modulo: datos});
    }

  }
}
