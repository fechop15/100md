import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {ModulosPage} from "../../modulos/modulos";
import {AuthServiceProvider} from "../../../providers/auth-service/auth-service";

/**
 * Generated class for the MisionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mision',
  templateUrl: 'mision.html',
})
export class MisionPage {

/*  mision = [
    {id: 1, color: "#f193a3", nombre: "1. Nuestro involucramiento en las misiones",modulo:[{
        titulo: "MISIÓN",
        icono: "mision.png",
        nombre: "Nuestro involucramiento en las misiones",
        idModulo:"49",
        datos: [
          {id: 1, color: "#f193a3", nombre: "Escritura Clave"},
          {id: 2, color: "#f193a3", nombre: "Verdad Clave"},
          {id: 3, color: "#f193a3", nombre: "Mi compromiso"},
          {id: 4, color: "#f193a3", nombre: "Aplicación"},
        ]
      }]},
    {id: 2, color: "#f193a3", nombre: "2. Entendiendo la Gran Comisión",modulo:[{
        titulo: "MISIÓN",
        icono: "mision.png",
        nombre: "Entendiendo la Gran Comisión",
        idModulo:"50",
        datos: [
          {id: 1, color: "#f193a3", nombre: "Escritura Clave"},
          {id: 2, color: "#f193a3", nombre: "Verdad Clave"},
          {id: 3, color: "#f193a3", nombre: "Mi compromiso"},
          {id: 4, color: "#f193a3", nombre: "Aplicación"},
        ]
      }]},
    {id: 3, color: "#f193a3", nombre: "3. Cómo ministrar en las misiones transculturales y transdenominacionales",modulo:[{
        titulo: "MISIÓN",
        icono: "mision.png",
        nombre: "Cómo ministrar en las misiones transculturales y transdenominacionales",
        idModulo:"51",
        datos: [
          {id: 1, color: "#f193a3", nombre: "Escritura Clave"},
          {id: 2, color: "#f193a3", nombre: "Verdad Clave"},
          {id: 3, color: "#f193a3", nombre: "Mi compromiso"},
          {id: 4, color: "#f193a3", nombre: "Aplicación"},
        ]
      }]},
    {id: 4, color: "#f193a3", nombre: "4. Movilizando a la iglesia para las misiones",modulo:[{
        titulo: "MISIÓN",
        icono: "mision.png",
        nombre: "Movilizando a la iglesia para las misiones",
        idModulo:"52",
        datos: [
          {id: 1, color: "#f193a3", nombre: "Escritura Clave"},
          {id: 2, color: "#f193a3", nombre: "Verdad Clave"},
          {id: 3, color: "#f193a3", nombre: "Mi compromiso"},
          {id: 4, color: "#f193a3", nombre: "Aplicación"},
        ]
      }]}
  ];*/

  progreso: any;
  mision:any;
  temp: any;
  nivel:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController,public authServiceProvider: AuthServiceProvider) {
    this.temp=this.authServiceProvider.consultarTemasRealizados();
    this.nivel=this.authServiceProvider.consultarDatos();

    console.log("temp");
    console.log(this.temp);
      this.mision=this.temp.mision;
     console.log("mision");
    console.log(this.mision);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MisionPage');
  }

  openNavDetailsPage(datos) {
    console.log(datos);
    if (this.nivel.nivel>=datos.id){

      let moduloModal = this.modalCtrl.create(ModulosPage, {modulo: datos});
    moduloModal.onDidDismiss(data => {
      console.log(data);
      console.log("mision");
      for (let i in this.mision){
        if (this.mision[i].id==data.id){
          console.log("actualizar");
          if (data.modulo[0].datos[0].color=="#e94b67" && data.modulo[0].datos[1].color=="#e94b67" && data.modulo[0].datos[1].color=="#e94b67" && data.modulo[0].datos[3].color=="#e94b67" && this.nivel.nivel==datos.id){
            //nivel a 2// si nivel es >2 no cambiar
            data.color="#e94b67";
            this.nivel.nivel=data.id+1
            console.log("completado")
          }
          this.temp.mision[i]=data;
        }
      }
      this.authServiceProvider.guardarProgreso(this.temp,3);

    });
    moduloModal.present();
    // this.navCtrl.push(ModulosPage, {modulo: datos});
  }
  }
}
