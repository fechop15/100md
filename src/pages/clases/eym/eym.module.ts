import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EymPage } from './eym';

@NgModule({
  declarations: [
    EymPage,
  ],
  imports: [
    IonicPageModule.forChild(EymPage),
  ],
})
export class EymPageModule {}
