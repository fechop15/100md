import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {ModulosPage} from "../../modulos/modulos";
import {AuthServiceProvider} from "../../../providers/auth-service/auth-service";

/**
 * Generated class for the EymPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-eym',
  templateUrl: 'eym.html',
})
export class EymPage {

/*  eym=[
    {id:1,color:"#f193a3",nombre:"1. La tarea del Evangelismo",modulo:[{
        titulo:"E & M",
        icono:"eym.png",
        nombre:"La tarea del Evangelismo",
        idModulo:"32",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:2,color:"#f193a3",nombre:"2. Una forma práctica de guiar a una persona a Cristo",modulo:[{
        titulo:"E & M",
        icono:"eym.png",
        nombre:"Una forma práctica de guiar a una persona a Cristo",
        idModulo:"33",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:3,color:"#f193a3",nombre:"3. La persona y el ministerio del Espíritu Santo",modulo:[{
        titulo:"E & M",
        icono:"eym.png",
        nombre:"La persona y el ministerio del Espíritu Santo",
        idModulo:"34",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:4,color:"#f193a3",nombre:"4. Unción del Espíritu Santo",modulo:[{
        titulo:"E & M",
        icono:"eym.png",
        nombre:"Unción del Espíritu Santo",
        idModulo:"35",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:5,color:"#f193a3",nombre:"5. Entendiendo la unción",modulo:[{
        titulo:"E & M",
        icono:"eym.png",
        nombre:"Entendiendo la unción",
        idModulo:"36",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:6,color:"#f193a3",nombre:"6. Cómo ser llenos con el Espíritu Santo",modulo:[{
        titulo:"E & M",
        icono:"eym.png",
        nombre:"Cómo ser llenos con el Espíritu Santo",
        idModulo:"37",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:7,color:"#f193a3",nombre:"7. Cómo ser guiado por el Espíritu Santo",modulo:[{
        titulo:"E & M",
        icono:"eym.png",
        nombre:"Cómo ser guiado por el Espíritu Santo",
        idModulo:"38",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:8,color:"#f193a3",nombre:"8. Moviéndonos en los dones espirituales",modulo:[{
        titulo:"E & M",
        icono:"eym.png",
        nombre:"Moviéndonos en los dones espirituales",
        idModulo:"39",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:9,color:"#f193a3",nombre:"9. El misterio de hablar en lenguas",modulo:[{
        titulo:"E & M",
        icono:"eym.png",
        nombre:"El misterio de hablar en lenguas",
        idModulo:"40",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:10,color:"#f193a3",nombre:"10. Cómo ministrar sanidad",modulo:[{
        titulo:"E & M",
        icono:"eym.png",
        nombre:"Cómo ministrar sanidad",
        idModulo:"41",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:11,color:"#f193a3",nombre:"11. Cómo ministrar liberación",modulo:[{
        titulo:"E & M",
        icono:"eym.png",
        nombre:"Cómo ministrar liberación",
        idModulo:"42",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:12,color:"#f193a3",nombre:"12. Viviendo un estilo de vida de perdón",modulo:[{
        titulo:"E & M",
        icono:"eym.png",
        nombre:"Viviendo un estilo de vida de perdón",
        idModulo:"43",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:13,color:"#f193a3",nombre:"13. Consejería y sanidad interior",modulo:[{
        titulo:"E & M",
        icono:"eym.png",
        nombre:"Consejería y sanidad interior",
        idModulo:"44",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:14,color:"#f193a3",nombre:"14. Entendiendo la guerra espiritual",modulo:[{
        titulo:"E & M",
        icono:"eym.png",
        nombre:"Entendiendo la guerra espiritual",
        idModulo:"45",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:15,color:"#f193a3",nombre:"15. Cómo empezar un grupo celular",modulo:[{
        titulo:"E & M",
        icono:"eym.png",
        nombre:"Cómo empezar un grupo celular",
        idModulo:"46",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:16,color:"#f193a3",nombre:"16. Como dirigir y desarrollar un grupo celular",modulo:[{
        titulo:"E & M",
        icono:"eym.png",
        nombre:"Como dirigir y desarrollar un grupo celular",
        idModulo:"47",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]},
    {id:17,color:"#f193a3",nombre:"17. El corazón de un pastor",modulo:[{
        titulo:"E & M",
        icono:"eym.png",
        nombre:"El corazón de un pastor",
        idModulo:"48",
        datos:[{id:1,color:"#f193a3",nombre:"Escritura Clave"},
          {id:2,color:"#f193a3",nombre:"Verdad Clave"},
          {id:3,color:"#f193a3",nombre:"Mi compromiso"},
          {id:4,color:"#f193a3",nombre:"Aplicación"},
        ]}]}
  ];*/

  progreso: any;
  eym:any;
  temp: any;
  nivel:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController,public authServiceProvider: AuthServiceProvider) {
    this.temp=this.authServiceProvider.consultarTemasRealizados();
    this.nivel=this.authServiceProvider.consultarDatos();

    console.log("temp");
    console.log(this.temp);
    this.eym=this.temp.eym;

    console.log("eym");
    console.log(this.eym);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EymPage');
  }
  openNavDetailsPage(datos){
    if (this.nivel.nivel>=datos.id){

      let moduloModal = this.modalCtrl.create(ModulosPage, {modulo: datos});
    moduloModal.onDidDismiss(data => {
      console.log(data);
      console.log("eym");
      for (let i in this.eym){
        if (this.eym[i].id==data.id){
          console.log("actualizar");
          if (data.modulo[0].datos[0].color=="#e94b67" && data.modulo[0].datos[1].color=="#e94b67" && data.modulo[0].datos[1].color=="#e94b67" && data.modulo[0].datos[3].color=="#e94b67" && this.nivel.nivel==datos.id){
            //nivel a 2// si nivel es >2 no cambiar
            data.color="#e94b67";
            this.nivel.nivel=data.id+1
            console.log("completado")
          }
          this.temp.eym[i]=data;
        }
      }

      this.authServiceProvider.guardarProgreso(this.temp,3);

    });
    moduloModal.present();
      //this.navCtrl.push(ModulosPage,{modulo: datos});
  }
  }
}
