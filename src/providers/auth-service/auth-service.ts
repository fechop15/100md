import {Http, Headers} from '@angular/http';
import { Injectable } from '@angular/core';
import {ToastController} from "ionic-angular";

/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
let apiUrl = 'http://www.plan100mildiscipulos.com/app100/api/';

@Injectable()
export class AuthServiceProvider {
  responseData: any;
  json: any;

  respuestas = [

  "Separa",
  "Reconocer",
  "Necesidad",
  "Ama",
  "Morir",
  "Salva",
  "Pecado",
  "Único",
  "Decisión",
  "Corazón",
  "Salvación",
  "Arrepentimiento",
  "Perdona",
  "Verdaderamente",
  "Puerta",
  "Toca",
  "Jesús",
  "Darte",
  "Convertirte",
  "Hijo/a",
  "Tuya",
  "Confiesa",
  "Boca",
  "Corazón",
  "Negarte",
  "Perteneces",
  "Orar",
  "Corazón",
  "Tu",
    "Tradición",
    "Finanzas",
    "Propósito",
    "Reafirmarlos",
    "Gracia",
    "Línea de plomo",
    "Formulados",
    "Dirigida",
    "Definidas",
    "Adoración",
    "Adorando",
    "Servicio",
    "Ocupamos",
    "Adoración",
    "Ordenados",
    "Deber",
    "Amor",
    "Evangelismo",
    "Privilegio",
    "Familia",
    "Donde sea",
    "Relación de calidad",
    "Paz",
    "Demuestra",
    "Duradera",
    "Ministerio",
    "Clave",
    "Base",
    "Valorar",
    "Ministrarnos",
    "Discipulado",
    "Bautizad",
    "Elementos",
    "Asociación",
    "Miembros",
    "Edificar",
    "Cristo",
    "Enseñar",
    "Maduras",
    "Discipulado",
    "Misión",
    "Mundo",
    "Llamados",
    "Orar",
    "Invertir",
    "Enviar",
    "Traemos",
    "Edificamos",
    "Entrenamos",
    "Enviamos",
    "Miembros",
    "Madurez",
    "Ministerio",
    "Misión",
    "Personas",
    "Apropiándome",
    "Seguidores",
    "Perdidos",
    "Madurez",
    "Ambición",
    "Enfoca",
    "Total",
    "Cualquier",
    "Admira",
    "Conoce",
    "Sin",
    "Bien",
    "Ganar",
    "Restauración",
    "Perspectiva",
    "Libera",
    "Produce",
    "Crea",
    "Permanentes",
    "Bueno",
    "Eterna",
    "Perdura",
    "Acción de gracias",
    "Trae",
    "Perspectiva",
    "Mente",
    "Fe",
    "Armas",
    "Sobreabundante",
    "Alabar",
    "Clave",
    "Silencia",
    "Sobrenatural",
    "Dulces",
    "Libera",
    "Prepara",
    "Impacta",
    "Continuamente",
    "Adoración",
    "Intima",
    "Santidad",
    "Palabra",
    "Sacrificio",
    "Abierto",
    "Arrepentido",
    "Confesar",
    "Confesión",
    "Alabanza",
    "Acción de gracias",
    "Adoración",
    "Alabanza",
    "Apaga",
    "Humildad",
    "Enemigo",
    "Queja",
    "Bendiciones",
    "Presencia",
    "Renovados",
    "Milagros",
    "Debajo",
    "Sanar",
    "Salvar",
    "Liberar",
    "Proteger",
    "Dividen",
    "Abusos",
    "Abusos",
    "Una",
    "Compromiso",
    "Prosperar",
    "Disciplina",
    "La falta de perdón",
    "Perdón",
    "Perdonarte",
    "Pertenece",
    "Diezmo",
    "Incremento",
    "Casa",
    "Pago",
    "Sacerdote",
    "Alentó",
    "Demostrar",
    "Honramos",
    "Bendición",
    "Balance",
    "Bendice",
    "Maldecimos",
    "No",
    "Público",
    "Respeto",
    "Seriamente",
    "Planeado",
    "Espontáneo",
    "Dirija",
    "Convicción",
    "Alegre",
    "Gozosamente",
    "Mala gana",
    "Prospera",
    "Refrescado",
    "Sacramento",
    "No creyentes",
    "Perezoso",
    "Trabajan",
    "Predicando",
    "Apoyar",
    "Viudas",
    "Pobres",
    "Patrón",
    "Imagen",
    "Semejanza",
    "Primero",
    "Dar",
    "Demostración",
    "Proporción",
    "Recibido",
    "Motiva",
    "Adoración",
    "Motivación",
    "Más grande",
    "Cristiano",
    "Discípulos",
    "Aceptado",
    "Multitud",
    "Compromiso",
    "Proceso",
    "Discipulado",
    "Señorío",
    "Demandas",
    "Maravilloso",
    "Cristo",
    "Lleno",
    "Controlado",
    "Pesado",
    "Placer",
    "Decisiones",
    "Cuidadosamente",
    "Convencido",
    "Discipulado",
    "Requiere",
    "Convicciones",
    "Comprometido",
    "Fortaleza",
    "Más fuerte",
    "Rendir",
    "Lección",
    "Influencia",
    "Crecimiento",
    "Creencias",
    "Divino",
    "Relación",
    "Revelación",
    "Personal",
    "Conocimiento",
    "Otros",
    "Por dentro",
    "Autoconocimiento",
    "Reconozcas"
  ];

  constructor(public http : Http, public toastCtrl: ToastController) {
    console.log('Hello AuthServiceProvider Provider');
  }


  postData(credentials, type) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();

      this.http.post(apiUrl + type, JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {
          resolve(JSON.parse('{"error":{"text":"Error de Conexion al Servidor"}}'));
          //reject(err);
        });
    });

  }

  actualizarLogin() {
    console.log("actualizando login..");
    let data = JSON.parse(localStorage.getItem('userData'));
    let userData={ "username": data.userData.username, "token": data.userData.token,"idusers":data.userData.idusers };
    this.postData(userData, "LoginUpdate").then((result) => {
      this.responseData = result;
      console.log(this.responseData);
      if (this.responseData.userData) {
        console.log(this.responseData.userData);
        localStorage.setItem("userData", JSON.stringify(this.responseData));
        return this.responseData.userData;
      } else if (this.responseData.error) {
        this.presentToast(this.responseData.error.text);
      }
    });
  }

  consultarTemasRealizados() {
    // data = JSON.parse(localStorage.getItem('json'));
    const data = JSON.parse(localStorage.getItem('json'));
    //let userData={ "idusers": data.userData.idusers, "token": data.userData.token };
    console.log("consultando temas..");
    //console.log(data);
    return JSON.parse(data);
  }

  consultarDatos() {
    const data = JSON.parse(localStorage.getItem('userData'));
    console.log("consultando datos..");
    console.log(data.userData);
    return data.userData;
  }

  guardarProgreso(clase,nivel) {
    //console.log(clase);
    console.log("proceso guardado");

    if (JSON.stringify(JSON.stringify(clase))){
      localStorage.setItem("json", JSON.stringify(JSON.stringify(clase)));
      //console.log(JSON.stringify(JSON.stringify(clase)));

    } else{
      localStorage.setItem("json", JSON.stringify(clase));
      //console.log(JSON.stringify(clase));
    }
    const json = JSON.parse(JSON.parse(localStorage.getItem('json')));
    const data = JSON.parse(localStorage.getItem('userData'));
    //console.log(json);
    let userData;
    if(nivel<=data.userData.nivel){
      userData={ "idusers": data.userData.idusers, "token": data.userData.token,"json": JSON.stringify(json) };
      console.log("json update");
    }else{
      userData={ "idusers": data.userData.idusers, "token": data.userData.token,"nivel":nivel,"json": JSON.stringify(json) };
      console.log("lvl update");
    }

    if (JSON.stringify(json) != data.userData.json){
      this.postData(userData, "nivelUpdate").then((result) => {
        this.responseData = result;
        //console.log(this.responseData);
        if (this.responseData.progresoData) {
          localStorage.setItem("progreso", JSON.stringify(this.consultarTemasRealizados()));
        } else if (this.responseData.error) {
          this.presentToast(this.responseData.error.text);
        }
      });
    }

  }

  realizarTema(idTema,datos) {
    const data = JSON.parse(localStorage.getItem('userData'));
    let userData={ "idusers": data.userData.idusers, "token": data.userData.token,"idtema":idTema,"datos": datos };
    console.log("Realizando tema");
    this.postData(userData, "progreso").then((result) => {
      this.responseData = result;
      console.log(this.responseData);
      if (this.responseData.progresoData) {
        this.consultarTemasRealizados();
        //localStorage.setItem("progreso", JSON.stringify(this.responseData));
      } else if (this.responseData.error) {
        this.presentToast(this.responseData.error.text);
      }
    });
  }

  actualizar(datos){
    console.log("Realizando actualizacion de datos");
    this.postData(datos, "actualizar").then((result) => {
      this.responseData = result;
      console.log(this.responseData);
      if (this.responseData.actualizacion) {
        this.presentToast(this.responseData.actualizacion.text);
      } else if (this.responseData.error) {
        this.presentToast(this.responseData.error.text);
      }
    });
  }

  presentToast(texto) {
    let toast = this.toastCtrl.create({
      message: texto,
      duration: 5000,
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }


  ramdomRespuestas(){
    return this.respuestas[Math.floor(Math.random() * (this.respuestas.length))];

  }

}
